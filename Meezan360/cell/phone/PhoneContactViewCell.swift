//
//  PhoneContactViewCell.swift
//  Hunters
//
//  Created by Akber Sayani on 14/12/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit

class PhoneContactViewCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var designationLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func populateData(_ data: PhoneContactModel) {
        self.nameLabel.text = data.name
        self.designationLabel.text = data.designation
    }

}
