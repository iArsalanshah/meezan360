//
//  SearchDetailsViewCell.swift
//  Meezan360
//
//  Created by Akber Sayani on 24/10/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit

class SearchDetailsViewCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var codeLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func populateData(_ data: String) {
        let dataArray = data.components(separatedBy: "&&")
        if dataArray.count >= 2 {
            self.codeLabel.isHidden = false
            self.nameLabel.text = String(dataArray[0])
            self.codeLabel.text = String(format: "Branch code: %@", String(dataArray[1]))
        } else {
            self.codeLabel.isHidden = true
            self.nameLabel.text = data
        }
    }
}
