//
//  TableColumnTVC.swift
//  Meezan360
//
//  Created by Syed Arsalan Shah on 12/10/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit

class PagerExtendedDetailsColumnTVC: UITableViewCell {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label3: UILabel!
    @IBOutlet weak var label4: UILabel!
    @IBOutlet weak var label5: UILabel!
    @IBOutlet weak var label6: UILabel!
    @IBOutlet weak var label7: UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    internal func setupData(_ data: PageValueModel) {
        self.title.setHtmlText(data.title)
        
        self.label1.setHtmlText(data.value)
        self.label2.setHtmlText(data.value1)
        self.label3.setHtmlText(data.value2)
        self.label4.setHtmlText(data.value3)
        self.label5.setHtmlText(data.value4)
        self.label6.setHtmlText(data.value5)
        self.label7.setHtmlText(data.value6)
    }
}
