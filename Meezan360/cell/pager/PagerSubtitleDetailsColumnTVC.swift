//
//  PagerSubtitleDetailsColumnTVC.swift
//  Hunters
//
//  Created by Akber Sayani on 24/11/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit

class PagerSubtitleDetailsColumnTVC: UITableViewCell {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var subLabel1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var subLabel2: UILabel!
    @IBOutlet weak var label3: UILabel!
    @IBOutlet weak var subLabel3: UILabel!
    @IBOutlet weak var label4: UILabel!
    @IBOutlet weak var subLabel4: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    internal func setupData(_ data: PagerSubValueModel) {
        self.title.setHtmlText(data.title)
        self.subTitle.setHtmlText(data.subTitle)

        self.label1.setHtmlText(data.value1)
        self.subLabel1.setHtmlText(data.subValue1)

        self.label2.setHtmlText(data.value2)
        self.subLabel2.setHtmlText(data.subValue2)
        
        self.label3.setHtmlText(data.value3)
        self.subLabel3.setHtmlText(data.subValue3)

        self.label4.setHtmlText(data.value4)
        self.subLabel4.setHtmlText(data.subValue4)
    }
}
