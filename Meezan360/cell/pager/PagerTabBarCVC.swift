//
//  PagerTabBarCVC.swift
//  Meezan360
//
//  Created by Syed Arsalan Shah on 05/11/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit

class PagerTabBarCVC: UICollectionViewCell {

    @IBOutlet weak var pagerCollectionView: UICollectionView!
    
    private var itemsPerRow: CGFloat = 1
    private let sectionInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
    private var dataSource = [PagerModel]()
    private var lastSelectedIndex = 0
    
    internal var pagerClickDelegate: PagerDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        pagerCollectionView.register(UINib(nibName: PagerTabCVC.storyboardId, bundle: nil), forCellWithReuseIdentifier: PagerTabCVC.storyboardId)

        pagerCollectionView.delegate = self
        pagerCollectionView.dataSource = self
        
        //bottom shadow effect
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.layer.shadowRadius = 6.0
        self.layer.shadowOpacity = 0.35
        self.layer.masksToBounds = false
    }

    internal func setupData(_ data: [PagerModel]?) {
        guard let data = data else { return }
        self.dataSource = data
        itemsPerRow = data.count > 4 ? 4 : CGFloat(data.count)

        self.pagerCollectionView.reloadData()
    }
}


extension PagerTabBarCVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        lastSelectedIndex = indexPath.row
        pagerClickDelegate?.onPagerClick(index: lastSelectedIndex)
        for (index, cell) in getAllCells().enumerated() {
            if let cell = cell as? PagerTabCVC, index != lastSelectedIndex {
                cell.setSelection(state: false)
                self.pagerCollectionView.reloadItems(at: [indexPath])
            }
        }
    }
    
    func getAllCells() -> [UICollectionViewCell] {
        var cells = [UICollectionViewCell]()
        // assuming tableView is your self.tableView defined somewhere
        for i in 0...self.pagerCollectionView.numberOfSections-1
        {
            for j in 0...self.pagerCollectionView.numberOfItems(inSection: i) - 1
            {
                if let cell = self.pagerCollectionView.cellForItem(at: NSIndexPath(row: j, section: i) as IndexPath) {
                    cells.append(cell)
                }

            }
        }
        return cells
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PagerTabCVC.storyboardId, for: indexPath) as? PagerTabCVC else { return UICollectionViewCell() }
        cell.titleLabel.text = dataSource[indexPath.row].title
        cell.setSelection(state: lastSelectedIndex == indexPath.row)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenSize: CGRect = UIScreen.main.bounds
        let viewWidth = screenSize.width
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = viewWidth - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        return CGSize(width: widthPerItem, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
}

