//
//  DetailMovementCVC.swift
//  Meezan360
//
//  Created by Syed Arsalan Shah on 10/10/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit

class PagerDetailsCVC: UICollectionViewCell {
    @IBOutlet weak var tableView: UITableView!
    
    private var dataSource: [PageValueModel] = []
    var isLoadExtendedColumn: Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(UINib(nibName: PagerDetailsColumnTVC.storyboardId, bundle: nil), forCellReuseIdentifier: PagerDetailsColumnTVC.storyboardId)
    }

    internal func setupData(_ data: [PageValueModel]) {
        self.dataSource = data
        self.tableView.reloadData()
    }
}

extension PagerDetailsCVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSource.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 25.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: PagerDetailsColumnTVC.storyboardId) as? PagerDetailsColumnTVC else { return UITableViewCell() }
        cell.setupData(self.dataSource[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let cell = cell as? PagerDetailsColumnTVC else { return }
        if let data = self.dataSource.first {
            if let text = data.value, !text.isEmpty {
                cell.label1.isHidden = false
            } else {
                cell.label1.isHidden = true
            }
            
            if let text = data.value1, !text.isEmpty {
                cell.label2.isHidden = false
            } else {
                cell.label2.isHidden = true
            }

            if let text = data.value2, !text.isEmpty {
                cell.label3.isHidden = false
            } else {
                cell.label3.isHidden = true
            }

            if let text = data.value3, !text.isEmpty {
                cell.label4.isHidden = false
            } else {
                cell.label4.isHidden = true
            }

            if let text = data.value4, !text.isEmpty {
                cell.label5.isHidden = false
            } else {
                cell.label5.isHidden = true
            }

            if let text = data.value5, !text.isEmpty {
                cell.label6.isHidden = false
            } else {
                cell.label6.isHidden = true
            }

            if let text = data.value6, !text.isEmpty {
                cell.label7.isHidden = false
            } else {
                cell.label7.isHidden = true
            }
        }
    }
}
