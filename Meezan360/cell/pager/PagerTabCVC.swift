//
//  PagerTabCVC.swift
//  Meezan360
//
//  Created by Syed Arsalan Shah on 05/11/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit

class PagerTabCVC: UICollectionViewCell {

    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bottomLineView: UIView!
    @IBOutlet weak var lineHeight: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    internal func setSelection(state: Bool) {
        if state == true {
            bottomLineView.backgroundColor = UIColor(named: "colorCustomPurple")
            bottomLineView.alpha = 1.0
            bottomLineView.cornerRadius = 2
            lineHeight.constant = 4
        } else {
            bottomLineView.backgroundColor = UIColor.lightGray
            bottomLineView.alpha = 0.5
            bottomLineView.cornerRadius = 0
            lineHeight.constant = 2
        }
    }
}
