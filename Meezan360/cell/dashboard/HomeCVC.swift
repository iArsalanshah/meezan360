//
//  HomeCVC.swift
//  ChartViewTestProject
//
//  Created by Syed Arsalan Shah on 10/5/19.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit

class HomeCVC: UICollectionViewCell {
    @IBOutlet weak var dataTypeLabel: UILabel!
    @IBOutlet weak var dataSizeLabel: UILabel!
    @IBOutlet weak var dataPercentageLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    internal func setupData(_ data: HeadValueModel) {
        dataTypeLabel.setHtmlText(data.key)
        dataSizeLabel.setHtmlText(data.value1)
        dataPercentageLabel.setHtmlText(data.value2)
    }
}
