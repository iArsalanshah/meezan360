//
//  ADCMenuChartLegendCVC.swift
//  Meezan360
//
//  Created by Akber Sayani on 06/11/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit

class ADCMenuChartLegendCVC: UICollectionViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    internal func setupData(_ data: CashKeyValueModel) {
        nameLabel.text = data.key
        valueLabel.text = data.value2
    }
    
    internal func setupData(_ data: HeadValueModel) {
        nameLabel.text = data.key
        valueLabel.text = data.value1
    }
}
