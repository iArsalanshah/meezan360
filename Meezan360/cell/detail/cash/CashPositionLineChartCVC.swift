//
//  CashPositionLineChartCVC.swift
//  Meezan360
//
//  Created by Syed Arsalan Shah on 25/10/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit
import Charts

class CashPositionLineChartCVC: UICollectionViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var chartView: LineChartView!
    
    override func awakeFromNib() {
        super.awakeFromNib()        
        // Initialization code
        setupChartView()
    }
    
    private func setupChartView() {
        chartView.delegate = self
        
        chartView.chartDescription?.enabled = false
        chartView.dragEnabled = true
        chartView.setScaleEnabled(true)
        chartView.pinchZoomEnabled = true
        chartView.rightAxis.enabled = false
        chartView.legend.enabled = false
    }
}

//MARK: - LINE CHART
extension CashPositionLineChartCVC: ChartViewDelegate {
    internal func setupData(_ data: [CashKeyValueModel]) {
        let xAxisValues = data.map {$0.key?.initials ?? "-"}
        let xAxis = chartView.xAxis
        xAxis.labelPosition = .bottom
        xAxis.labelFont = .systemFont(ofSize: 8)
        xAxis.labelCount = xAxisValues.count
        xAxis.granularityEnabled =  true
        xAxis.valueFormatter = IndexAxisValueFormatter(values: xAxisValues)
        
        let yAxisValues = data.map { Double($0.value1?.digits ?? "0") ?? 0.0 }
        let yAxis = chartView.leftAxis
        yAxis.drawGridLinesEnabled = true
        yAxis.granularityEnabled = true
        yAxis.spaceTop = 0.2
        yAxis.axisMinimum = 0.0
        yAxis.labelFont = UIFont.systemFont(ofSize: 8)


        self.chartView.marker = getMarker(chartView: chartView)
        chartView.animate(xAxisDuration: 2.0)
        
        setDataCount(xAxisValues, yAxisValues: yAxisValues)
    }
    
    internal func setDataCount(_ xAxisValues: [String], yAxisValues: [Double]) {
        let values = (0..<xAxisValues.count).map { (i) -> ChartDataEntry in
            let yValue = (i < yAxisValues.count) ? yAxisValues[i] : 0.0
            return ChartDataEntry(x: Double(i), y: yValue)
        }
        
        let set = LineChartDataSet(entries: values, label: "")
        set.drawIconsEnabled = false
        
        set.setColor(.black)
        set.setCircleColor(.black)
        set.lineWidth = 1
        set.circleRadius = 3
        set.valueFont = .systemFont(ofSize: 9)
        set.formLineWidth = 1
        set.formSize = 6
        set.drawHorizontalHighlightIndicatorEnabled = false//on graph click horizontal line
        set.drawVerticalHighlightIndicatorEnabled = false//on graph click vertical line
        //line chart below area to be filled or not
        set.drawFilledEnabled = false
        
        let data = LineChartData(dataSet: set)
        chartView.data = data
    }
    
    func getMarker(chartView: ChartViewBase) -> MarkerImage {
        let marker = BalloonMarker(color: UIColor(white: 180/255, alpha: 1),
                                   font: .systemFont(ofSize: 12),
                                   textColor: .white,
                                   insets: UIEdgeInsets(top: 8, left: 8, bottom: 20, right: 8))
        marker.chartView = chartView
        marker.minimumSize = CGSize(width: 80, height: 40)
        return marker
    }
}
