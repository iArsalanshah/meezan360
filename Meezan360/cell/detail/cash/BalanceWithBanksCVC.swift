//
//  BalanceWithBanksCVC.swift
//  Meezan360
//
//  Created by Syed Arsalan Shah on 26/10/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit
import Charts

class BalanceWithBanksCVC: UICollectionViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var chartView: BarChartView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupChartView()
    }
    
    private func setupChartView() {
        chartView.delegate = self
        chartView.chartDescription?.enabled = false
        chartView.drawBarShadowEnabled = false
        chartView.drawValueAboveBarEnabled = false
        chartView.rightAxis.enabled = false//disable right side coordinate vaues
        chartView.legend.enabled = false
    }
}

//MARK: - LINE CHART
extension BalanceWithBanksCVC: ChartViewDelegate {
    internal func setupData(_ data: [CashKeyValueModel]) {
        let xAxisValues = data.map {$0.key?.initials ?? "-"}
        let xAxis = chartView.xAxis
        xAxis.labelPosition = .bottom
        xAxis.labelFont = .systemFont(ofSize: 8)
        xAxis.labelCount = xAxisValues.count
        xAxis.valueFormatter = IndexAxisValueFormatter(values: xAxisValues)
        
        let yAxisValues = data.map { Double($0.value1?.digits ?? "0") ?? 0.0 }
        let yAxis = chartView.leftAxis
        yAxis.drawGridLinesEnabled = true
        yAxis.granularityEnabled = true
        yAxis.spaceTop = 0.2
        yAxis.axisMinimum = 0.0
        yAxis.labelFont = .systemFont(ofSize: 8)

        chartView.marker = self.getMarker(chartView: chartView)
        chartView.animate(xAxisDuration: 2.0)
        
        setDataCount(xAxisValues, yAxisValues: yAxisValues)
    }

    func setDataCount(_ xAxisValues: [String], yAxisValues: [Double]) {
        let values = (0..<xAxisValues.count).map { (i) -> BarChartDataEntry in
            let yValue = (i < yAxisValues.count) ? yAxisValues[i] : 0.0
            return BarChartDataEntry(x: Double(i), y: yValue)
        }
        
        let dataSet = BarChartDataSet(entries: values, label: nil)
        dataSet.colors = [ChartColorTemplates.colorFromString("#711A7E")]
        dataSet.drawValuesEnabled = false
        
        let chartData = BarChartData(dataSet: dataSet)
        chartData.setValueFont(UIFont(name: "HelveticaNeue-Light", size: 10)!)
        
        chartView.data = chartData
    }
    
    func getMarker(chartView: ChartViewBase) -> MarkerImage {
        let marker = XYMarkerView(color: UIColor(white: 180/250, alpha: 1),
                                  font: .systemFont(ofSize: 12),
                                  textColor: .white,
                                  insets: UIEdgeInsets(top: 8, left: 8, bottom: 20, right: 8),
                                  xAxisValueFormatter: chartView.xAxis.valueFormatter!)
        marker.chartView = chartView
        marker.minimumSize = CGSize(width: 80, height: 40)
        return marker
    }
}
