//
//  CASAMixCVC.swift
//  Meezan360
//
//  Created by Syed Arsalan Shah on 12/10/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit
import Charts

class CASAMixCVC: UICollectionViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var pieChartView: PieChartView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }    
}

//private var animationPlayed = false
extension CASAMixCVC {
    internal func setupPieChart(data: [DepositChartModel]) {
        self.titleLabel.text = data.first?.mainTitle ?? "-"
        
        var pieChartDataEntries = [PieChartDataEntry]()
        var pieChartColors = [UIColor]()
        
        for item in data {
            let value = item.val?.replacingOccurrences(of: "%", with: "")
            let dataEntry = PieChartDataEntry(value: Double(value ?? "0")!, label: item.title)
            if let hexCode = item.color {
                pieChartColors.append(UIColor(hexString: hexCode))
            } else {
                pieChartColors.append(UIColor.random)
            }
            
            pieChartDataEntries.append(dataEntry)
        }
        
        let dataSet = PieChartDataSet(entries: pieChartDataEntries, label: "Widget Types")
        dataSet.colors = pieChartColors
        dataSet.drawValuesEnabled = true
        dataSet.entryLabelFont = NSUIFont.systemFont(ofSize: 12.0, weight: .medium)
        dataSet.valueColors = [UIColor.black]
        dataSet.selectionShift = 8
        
        let data = PieChartData(dataSet: dataSet)
        pieChartView.data = data
        pieChartView.isMultipleTouchEnabled = false
        pieChartView.rotationEnabled = false//rotation gesture
        pieChartView.highlightPerTapEnabled = true
        pieChartView.drawHoleEnabled = false
        pieChartView.chartDescription?.enabled = false
        pieChartView.legend.enabled = false
        pieChartView.drawEntryLabelsEnabled = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.pieChartView.drawEntryLabelsEnabled = true
        }

        addShadow()//shadow

        pieChartView.animate(xAxisDuration: 1, yAxisDuration: 1, easingOption: .easeOutSine)
    }
    
    private func addShadow() {
        pieChartView.layer.shadowColor = UIColor.gray.cgColor
        pieChartView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        pieChartView.layer.shadowRadius = 12.0
        pieChartView.layer.shadowOpacity = 0.7
    }
}
