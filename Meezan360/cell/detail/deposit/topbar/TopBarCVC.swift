//
//  TopBarCVC.swift
//  Meezan360
//
//  Created by Syed Arsalan Shah on 10/10/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit

class TopBarCVC: UICollectionViewCell {
    @IBOutlet weak var collectionView: UICollectionView!
    
    private var itemsPerRow: CGFloat = 1
    private let sectionInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
    private var dataSource = [HeadValueModel]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        self.collectionView.register(UINib(nibName: HomeCVC.storyboardId, bundle: nil), forCellWithReuseIdentifier: HomeCVC.storyboardId)

        collectionView.delegate = self
        collectionView.dataSource = self
        
        if let layout = self.collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.estimatedItemSize = CGSize(width: 100, height: 60)
            layout.minimumLineSpacing = 20.0
            layout.minimumInteritemSpacing = 20.0
        }
        
        //bottom shadow effect
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.layer.shadowRadius = 6.0
        self.layer.shadowOpacity = 0.35
        self.layer.masksToBounds = false
    }
    
    internal func setupData(_ data: [HeadValueModel]) {
        self.dataSource = data
        itemsPerRow = CGFloat(self.dataSource.count)

        self.collectionView.reloadData()
    }
}


extension TopBarCVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeCVC.storyboardId, for: indexPath) as? HomeCVC else { return UICollectionViewCell() }
        cell.setupData(self.dataSource[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0)
    }
}
