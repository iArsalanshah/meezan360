//
//  DetailsButtonCVC.swift
//  Meezan360
//
//  Created by Syed Arsalan Shah on 12/10/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit

protocol DetailsButtonDelegate {
    func didTapOnButton(_ sender: UIView?)
}

class DetailsButtonCVC: UICollectionViewCell {
    @IBOutlet weak var dailyDepositView: UIView!
    @IBOutlet weak var currentAccountView: UIView!
    @IBOutlet weak var savingAccountView: UIView!
    @IBOutlet weak var termsDepositView: UIView!
    
    internal var delegate: DetailsButtonDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        let dailyDepositGesture = UITapGestureRecognizer(target: self, action: #selector(onclick(_:)))
        dailyDepositView.addGestureRecognizer(dailyDepositGesture)
        
        let currentAccountGesture = UITapGestureRecognizer(target: self, action: #selector(onclick(_:)))
        currentAccountView.addGestureRecognizer(currentAccountGesture)

        let savingAccountGesture = UITapGestureRecognizer(target: self, action: #selector(onclick(_:)))
        savingAccountView.addGestureRecognizer(savingAccountGesture)

        let termDepositGesture = UITapGestureRecognizer(target: self, action: #selector(onclick(_:)))
        termsDepositView.addGestureRecognizer(termDepositGesture)
    }
    
    @objc private func onclick(_ gesture: UITapGestureRecognizer) {
        if self.delegate != nil {
            self.delegate?.didTapOnButton(gesture.view)
        }
    }
}
