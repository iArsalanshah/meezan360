//
//  HeaderTVC.swift
//  Meezan360
//
//  Created by Syed Arsalan Shah on 12/10/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit

class HeaderTVC: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
