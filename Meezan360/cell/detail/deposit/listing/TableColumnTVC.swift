//
//  TableColumnTVC.swift
//  Meezan360
//
//  Created by Syed Arsalan Shah on 12/10/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit

class TableColumnTVC: UITableViewCell {
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label3: UILabel!
    @IBOutlet weak var label4: UILabel!
    @IBOutlet weak var label5: UILabel!
    @IBOutlet weak var label6: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    internal func setupHeaderData(withCellType type: DetailMovementCellType) {
        switch type {
        case .depositMenuList:
            self.label1.text = ""
            self.label2.text = "CA"
            self.label3.text = "SA"
            self.label4.text = "TD"
            self.label5.text = "Total"
            self.label5.isHidden = false
            self.label6.isHidden = false
            break
        case .cashMenuList:
            self.label1.text = ""
            self.label2.text = "Holding"
            self.label3.text = "Deposit"
            self.label4.text = "Ratio"
            self.label5.text = ""
            self.label5.isHidden = true
            self.label6.isHidden = true
            break
        case .otherMenuList:
            self.label1.text = ""
            self.label2.text = ""
            self.label3.text = ""
            self.label4.text = ""
            self.label5.text = ""
            self.label6.text = ""
            break
        }
    }
    
    internal func setupData(_ data: AnyObject) {
        if let data = data as? CurrencyModel {
            setupData(data)
        }
        
        if let data = data as? DepositListViewModel {
            setupData(data)
        }
        
        if let data = data as? OtherMenuItemDetailsModel {
            setupData(data)
        }
    }
    
    private func setupData(_ data: DepositListViewModel) {
        self.label1.setHtmlText(data.title)
        self.label2.setHtmlText(data.ca)
        self.label3.setHtmlText(data.sa)
        
        if let text = data.td {
            self.label4.isHidden = false
            self.label4.setHtmlText(text)
        } else {
            self.label4.isHidden = true
        }
        
        if let text = data.total {
            self.label5.isHidden = false
            self.label5.setHtmlText(text)
        } else {
            self.label5.isHidden = true
        }
        
        self.label6.isHidden = true
    }
    
    private func setupData(_ data: CurrencyModel) {
        self.label1.setHtmlText(data.title)
        self.label2.setHtmlText(data.holding)
        self.label3.setHtmlText(data.deposit)

        if let text = data.ratio {
            self.label4.isHidden = false
            self.label4.setHtmlText(text)
        } else {
            self.label4.isHidden = true
        }

        self.label5.isHidden = true
        self.label6.isHidden = true
    }
    
    private func setupData(_ data: OtherMenuItemDetailsModel) {
        self.label1.setHtmlText(data.title)
        
        self.label2.setHtmlText(data.value1)
        self.label3.setHtmlText(data.value2)
        self.label4.setHtmlText(data.value3)
        self.label5.setHtmlText(data.value4)
        self.label6.setHtmlText(data.value5)
    }
}
