//
//  DetailMovementCVC.swift
//  Meezan360
//
//  Created by Syed Arsalan Shah on 10/10/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit

enum DetailMovementCellType {
    case depositMenuList
    case cashMenuList
    case otherMenuList
}

class DetailMovementCVC: UICollectionViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    private var dataSource: [AnyObject] = []
    private var cellType: DetailMovementCellType = .depositMenuList
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(UINib(nibName: TableColumnTVC.storyboardId, bundle: nil), forCellReuseIdentifier: TableColumnTVC.storyboardId)
    }

    internal func setupData(_ data: [AnyObject], cellType: DetailMovementCellType = .depositMenuList) {
        self.cellType = cellType
        self.dataSource = data
        switch cellType {
        case .depositMenuList:
            if let data = data as? [DepositListViewModel] {
                self.titleLabel.text = data.first?.mainTitle ?? "-"
            } else {
                self.titleLabel.text = "-"
            }
            break
        case .cashMenuList:
            self.titleLabel.text = "Currency Position"
            break
        default:
            self.titleLabel.text = nil
            break
        }
        
        self.tableView.reloadData()
    }
}

extension DetailMovementCVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSource.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 25.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TableColumnTVC.storyboardId) as? TableColumnTVC else { return UITableViewCell() }
        cell.setupData(self.dataSource[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let cell = cell as? TableColumnTVC else { return }
        if let data = self.dataSource.first as? OtherMenuItemDetailsModel {
            if let text = data.value1, !text.isEmpty {
                cell.label2.isHidden = false
            } else {
                cell.label2.isHidden = true
            }

            if let text = data.value2, !text.isEmpty {
                cell.label3.isHidden = false
            } else {
                cell.label3.isHidden = true
            }

            if let text = data.value3, !text.isEmpty {
                cell.label4.isHidden = false
            } else {
                cell.label4.isHidden = true
            }

            if let text = data.value4, !text.isEmpty {
                cell.label5.isHidden = false
            } else {
                cell.label5.isHidden = true
            }
            
            if let text = data.value5, !text.isEmpty {
                cell.label6.isHidden = false
            } else {
                cell.label6.isHidden = true
            }
        }
    }
}
