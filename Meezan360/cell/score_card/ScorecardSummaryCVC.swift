//
//  ScorecardSummaryCVC.swift
//  Hunters
//
//  Created by Akber Sayani on 24/11/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit

class ScorecardSummaryCVC: UICollectionViewCell {
    @IBOutlet weak var keyLabel1: UILabel!
    @IBOutlet weak var keyLabel2: UILabel!
    @IBOutlet weak var keyLabel3: UILabel!
    @IBOutlet weak var valueLabel1: UILabel!
    @IBOutlet weak var valueLabel2: UILabel!
    @IBOutlet weak var valueLabel3: UILabel!
    
    func populateData(with keyValues: [KeyValueModel], row: Int) {
        for (index, item) in keyValues.enumerated() {
            switch index {
            case 0 + (row * 3):
                keyLabel1.text = item.key
                valueLabel1.text = item.value
                break
            case 1 + (row * 3):
                keyLabel2.text = item.key
                valueLabel2.text = item.value
                break
            case 2 + (row * 3):
                keyLabel3.text = item.key
                valueLabel3.text = item.value
                break
            default:
                break
            }
        }
    }
}
