//
//  ScoreCardHalfPieCVC.swift
//  Meezan360
//
//  Created by Syed Arsalan Shah on 19/10/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit
import HUChart

class ScoreCardHalfPieCVC: UICollectionViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var halfPieParentView: SemiCircularScale!
    @IBOutlet weak var pieLeftValueLabel: UILabel!
    @IBOutlet weak var pieRightValueLabel: UILabel!
    @IBOutlet weak var pieCenterValueLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    internal func setupData(_ data: ScorecardHeadModel) {
        self.halfPieParentView.circleRadius = self.frame.size.width / 1.5
        self.halfPieParentView.minReading = 0
        self.halfPieParentView.maxReading = 100
        
        self.titleLabel.text = data.title ?? "Score"
        self.pieCenterValueLabel.text = data.score ?? "-"
        
        let coloredValue = Double(data.percentage ?? "0") ?? 0
        self.halfPieParentView.reading = CGFloat(coloredValue)
    }

    //MARK:- PIE CHART
    internal func setupHalfPieChart(coloredValue: Int) {
        //change width of semi circle
        halfPieParentView.circleRadius = self.frame.size.width / 1.5
        //setup min max value
        halfPieParentView.minReading = 0
        halfPieParentView.maxReading = 100
        //setup colored value
        halfPieParentView.reading = CGFloat(coloredValue)
        
//        let halfPie = HUSemiCircleChart()
//        halfPieParentView.addSubview(halfPie)
//        halfPie.translatesAutoresizingMaskIntoConstraints = false
//        halfPie.leadingAnchor.constraint(equalTo: halfPieParentView.leadingAnchor, constant: 16).isActive = true
//        halfPie.trailingAnchor.constraint(equalTo: halfPieParentView.trailingAnchor, constant: -16).isActive = true
//        halfPie.topAnchor.constraint(equalTo: halfPieParentView.topAnchor, constant: 0).isActive = true
//        halfPie.bottomAnchor.constraint(equalTo: halfPieParentView.bottomAnchor, constant: 0).isActive = true
        
//        let data: NSMutableArray = NSMutableArray(array: [
//            HUChartEntry(name: "", value: 65)!,
//            HUChartEntry(name: "", value: 35)!
//        ])
//        let colors: NSMutableArray = NSMutableArray(array: [
//            UIColor(named: "colorTheme")!, UIColor.lightGray
//        ])
//
//        halfPie.data = data
//        halfPie.colors = colors
//        halfPie.title = "31.0"
//        halfPie.showPortionTextType = DONT_SHOW_PORTION
    }
}
