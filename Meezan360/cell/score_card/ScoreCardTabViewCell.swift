//
//  ScoreCardTabViewCell.swift
//  Meezan360
//
//  Created by Akber Sayani on 20/10/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit

class ScoreCardTabViewCell: UICollectionViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var cardView: UIView!

    func populateData(with item: ScoreModel) {
        titleLabel.text = item.name
        scoreLabel.text = item.score
    }
}
