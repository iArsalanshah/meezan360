//
//  Enums.swift
//
//

import Foundation
import UIKit

enum AppStoryboard : String {
    
    //Add all the storyboard names you wanted to use in your project
    case Main, Detail, Login, Search
    
    var instance : UIStoryboard {
        
        return UIStoryboard(name: self.rawValue, bundle: Bundle.main)
    }
    
    func viewController<T : UIViewController>(viewControllerClass : T.Type, function : String = #function, line : Int = #line, file : String = #file) -> T {
        
        let storyboardID = (viewControllerClass as UIViewController.Type).storyboardId
        
        guard let scene = instance.instantiateViewController(withIdentifier: storyboardID) as? T else {
            
            fatalError("ViewController with identifier \(storyboardID), not found in \(self.rawValue) Storyboard.\nFile : \(file) \nLine Number : \(line) \nFunction : \(function)")
        }
        
        return scene
    }
    
    func initialViewController() -> UIViewController? {
        
        return instance.instantiateInitialViewController()
    }
}

enum AccountType: String {
    case admin = "admin"
    case user = "user"
}

enum FontType: String {
    case light = "Poppins-Light"
    case regular = "Poppins-Regular"
    case medium = "Poppins-Medium"
    case semiBold = "Poppins-SemiBold"
    case bold = "Poppins-Bold"
}

enum StudentSignInType {
    case lateArrival
    case leavingEarly
    case returnToSchool
}

enum UserType {
    case admin
    case employee//also Staff
    case visitor
    case student
}
