
import UIKit
import Foundation

struct Global {

//    static var APP_MANAGER                   = AppStateManager.shared

}

enum WebRoute: String {
    func url() -> String{
        return APIConstants.BaseURL + self.rawValue
    }

    // MARK: - End points
    case loginRoute = "mobileservice.php"
    //case otherRoute = "360app.php"
    case otherRoute = "360appservice.php"
    
    // MARK: - Actions
    case loginAction = "login"
    case logout = "force_logout"
    case configAction = "360_getconfig"
    case changePassword = "reset_password"
    case phoneContacts = "phone_call"
    case setSearchFilterAction = "360_set_filters"
    case searchFilterAction = "lov_region_area_branches"

    // Dashboard options
    case dashboardAction = "360_dashboard"
    case financeAction = "360_finance"
    case complianceAction = "360_compliance"
    case wealthAction = "360_wealth"
    case profitablityAction = "360_profitability"
    case crossSellAction = "360_crossSell"
    case premiumAction = "360_premium"
    case controlsAction = "360_control"
    case adcAction = "360_adc"
    case ppcAction = "360_ppc"
    
    // Deposit options
    case depositAction = "360_deposit"
    case movementAction = "360_deposit_movement"
    case budgetAction = "360_deposit_budget"
    case topCustomerAction = "360_deposit_top150"
    case branchesAction = "360_deposit_onoffbranches"
    case dailyDepositPosition = "360_deposit_daily_position"
    case currentAccountTracker = "360_deposit_current_tracker"
    case savingAccountTracker = "360_deposit_saving_tracker"
    case termsDepositTracker = "360_deposit_term_tracker"
    
    // Scorecard options
    case scoreCardAction = "360_scorecard"
    case scoreCardDeposit = "360_scorecard_deposit"
    case scoreCardFinancing = "360_scorecard_financing"
    case scoreCardWealth = "360_scorecard_wealth"
    case scoreCardCrossSell = "360_scorecard_crosssell"
    case scoreCardADC = "360_scorecard_adc"
    case scoreCardControls = "360_scorecard_controls"
    case scoreCardCompliance = "360_scorecard_compliance"
    case scoreCardProfitablity = "360_scorecard_profitability"
    case scoreCardPremium = "360_scorecard_premiumbanking"
    case scoreCardCash = "360_scorecard_cash"

    // Cash options
    case cashAction = "360_cash"
    case currencyAction = "360_cash_currency"
    
    case complianceExtendedAction = "360_compliance_extend"
    case controlsExtendedAction = "360_control_extend"
}

struct APIConstants {
    static let BaseURL = "http://meezan360.uhfsolutions.com/mobileservice/"
    static let BaseLiveURL = "https://bdosales.meezanbank.com/"
}

struct Constants {
    
    static let appColor                      = UIColor(red: 53, green: 214, blue: 146, alpha: 1)
    static let appTitleColor                 = UIColor(red: 9, green: 9, blue: 72, alpha: 1)
    
    static let GMS_API_KEY                   = "AIzaSyBdDGZJRWrjyG309Rrm4BfD6K9fgdBntWw"
    static var GOOGLE_API_KEY                = "516496724041-ohf5jd1dl5n87htjp2don8n129k7gsfm.apps.googleusercontent.com"

    
    static let sideMenuDelegateName         = "controllerName"
    static let kFONT_WIDTH_FACTOR           = UIScreen.main.bounds.width / 414 //resize font according to screen size
    
    static let kWINDOW_FRAME                = UIScreen.main.bounds
    static let kSCREEN_SIZE                 = UIScreen.main.bounds.size
    static let kWINDOW_WIDTH                = UIScreen.main.bounds.size.width
    static let kWINDOW_HIEGHT               = UIScreen.main.bounds.size.height
    
    static let APP_DELEGATE                = UIApplication.shared.delegate as! AppDelegate
    static let UIWINDOW                    = UIApplication.shared.delegate!.window!
    
    static let USER_DEFAULTS               = UserDefaults.standard
    
    static let VALIDATION_PASSWORD_LENGTH              = 6
    
    static let VALIDATION_VALID_NAME                   = "Please provide a valid name."
    static let VALIDATION_VALID_EMAIL                  = "Please provide a valid Email Address."

    static let VALIDATION_PASSWORD_MIN                 = "Password should contain at least 6 characters."
    static let VALIDATION_PASSWORD_MATCH               = "New password and confirm password does not match."
    static let FORGET_PASSWORD                         = "A password has been sent to your email Address!"
    static let PASSWORD_UPDATED                        = "Your Password has been updated"
    
    static let VALIDATION_MAX_FIELD_LENGTH             = "Field must not be more than 15 characters."
    static let VALIDATION_MAX_DESCRIPTION_LENGTH       = "Description must not be more than 300 characters."
    static let VALIDATION_ALL_FIELDS                   = "Kindly fill all the fields."
    static let VALIDATION_VALID_URL                    = "Please provide a valid URL."
    static let VALIDATION_VALID_FILE_NAME              = "Please provide a file name."
    
    static let VALIDATION_IMAGE                        = "Please provide an image."
    static let VALIDATION_TERMS                        = "Kindly select the terms and conditions."
    
    static let PROFILE_UPDATED                         = "Your Profile has been updated."
    
    static let MESSAGE_LOGOUT                          = "Are you sure you want to logout?"
    
    static let FAILURE_MESSAGE                         = "Something went wrong. Please try again"
    
    
    struct AlertTypeTile {
        static let ERROR = "Error"
        static let SUCCESS = "Success"
        static let WARNING = "Warning"
        static let ALERT = "Alert"
    }
}

extension Notification.Name {
    static let SEARCH_FILTER_APPLIED = Notification.Name("SearchFilterAppliedNotification")
}
