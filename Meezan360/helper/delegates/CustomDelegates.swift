//
//  CustomDelegates.swift
//

import UIKit

protocol DialogDelegate: class {
    func onDismissDialog(text: String)
}

protocol OnClickDelegate: class {
    func onClick()
}

protocol PagerDelegate: class {
    func onPagerClick(index: Int)
}
