

import UIKit

@IBDesignable
class CardView: UIView {
    
    @IBInspectable var cornersRadius: CGFloat = 4
    
    override func layoutSubviews() {
        layer.cornerRadius = cornersRadius
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornersRadius)
        
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.3
        layer.shadowOffset = CGSize(width: 0, height: 3)
        layer.shadowRadius = 3
        layer.shadowPath = shadowPath.cgPath
    }
    
}
