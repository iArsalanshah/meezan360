
import UIKit

class RoundedButton: UIButton {
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let buttonHeight = self.frame.height
        
        self.layer.cornerRadius = CGFloat(buttonHeight / 2.0)
        self.clipsToBounds = true
        
    }
}

class RoundedImage : UIImageView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let buttonHeight = self.frame.height
        
        self.layer.cornerRadius = CGFloat(buttonHeight / 2.0)
        self.clipsToBounds = true
        
    }
}

class RoundedView : UIView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let buttonHeight = self.frame.height
        
        self.layer.cornerRadius = CGFloat(buttonHeight / 2.0)
        self.clipsToBounds = true
        
    }
}

