
import UIKit

// An attributed string extension to achieve colors on text.
extension NSMutableAttributedString {
    
    func setColor(color: UIColor, forText stringValue: String) {
        let range: NSRange = self.mutableString.range(of: stringValue, options: .caseInsensitive)
        self.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
    }
    
    func setCustomAttributedText(textOne: String, textTwo: String, titleFont: UIFont, detailFont: UIFont, titleColor: UIColor, detailColor:UIColor) -> NSMutableAttributedString {
        let yourAttributes = [NSAttributedString.Key.foregroundColor: titleColor, NSAttributedString.Key.font:titleFont]
        let yourOtherAttributes = [NSAttributedString.Key.foregroundColor: detailColor, NSAttributedString.Key.font:detailFont]
        
        let partOne = NSMutableAttributedString(string: textOne, attributes: yourAttributes)
        let partTwo = NSMutableAttributedString(string: textTwo, attributes: yourOtherAttributes)
        
        let combination = NSMutableAttributedString()
        
        combination.append(partOne)
        combination.append(partTwo)
        return combination
    }
    
}

