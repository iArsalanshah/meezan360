import UIKit

extension UILabel {
    func addCharacterSpacing(kernValue: Double = 1.15) {
        if let labelText = text, labelText.count > 0 {
            let attributedString = NSMutableAttributedString(string: labelText)
            attributedString.addAttribute(NSAttributedString.Key.kern, value: kernValue, range: NSRange(location: 0, length: attributedString.length - 1))
            attributedText = attributedString
        }
    }
    
    func setHtmlText(_ text: String?) {
        self.text = text?.html2String ?? "-"
        if let attributedText = text?.html2AttributedString, !attributedText.string.isEmpty {
            if let colorAttribute = attributedText.attribute(.foregroundColor, at: 0, effectiveRange: nil) as? UIColor {
                self.textColor = colorAttribute
            }
        }
    }
    
//    func decideTextDirection () {
//        if AppStateManager.shared.isArabic() {
//            self.textAlignment = .right
//        } else {
//            self.textAlignment = .left
//        }
//    }

}
