
import UIKit
import CommonCrypto

extension String {
    
    func getDateUTC(format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: self)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: dt!)
    }
    
    func getDateUTC(initalFormat: String, format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = initalFormat
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: self)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: dt!)
    }
    
    func localToUTC(initialFormat: String, returnFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = initialFormat
        dateFormatter.calendar = NSCalendar.current
        dateFormatter.timeZone = TimeZone.current
        
        let dt = dateFormatter.date(from: self)
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = returnFormat
        
        return dateFormatter.string(from: dt!)
    }
    
    func stringByRemovingEmoji() -> String {
        return String(self.filter { !$0.isEmoji() })
    }
    
    func isValidEmail() -> Bool {
        let emalRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let result = NSPredicate(format:"SELF MATCHES %@", emalRegex)
        
        return result.evaluate(with: self)
    }
    
    func isValidString() -> Bool {
        let trimmedString = self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        return !trimmedString.isEmpty
    }
    
    var isNumber: Bool {
        return !isEmpty && rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
    }
    
    var html2AttributedString: NSAttributedString? {
        return Data(utf8).html2AttributedString
    }
    
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
    
    func toDouble() -> Double {
        return (NumberFormatter().number(from: self)?.doubleValue) ?? 0.0
    }
    
    func toFloat() -> Float {
        return (NumberFormatter().number(from: self)?.floatValue) ?? 0.0
    }
    
    func toInt() -> Int {
        return (NumberFormatter().number(from: self)?.intValue) ?? 0
    }

    func widthOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.width
    }
    
    func heightOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.height
    }
    
    func sizeOfString(usingFont font: UIFont) -> CGSize {
        let fontAttributes = [NSAttributedString.Key.font: font]
        return self.size(withAttributes: fontAttributes)
    }

    var containsWhitespace : Bool {
        return(self.rangeOfCharacter(from: .whitespacesAndNewlines) != nil)
    }
    
    func getSpaceCount() -> Int {
        let count = self.components(separatedBy:" ").count
        if count > 0 {
            return count - 1
        } else {
            return 0
        }
    }
    
    //
    // Convert a base64 representation to a UIImage
    //
    func convertBase64ToImage() -> UIImage? {
        let imageData = Data(base64Encoded: self, options: Data.Base64DecodingOptions.ignoreUnknownCharacters)!
        return UIImage(data: imageData)
    }
    
    var md5: String {
        let data = Data(self.utf8)
        let hash = data.withUnsafeBytes { (bytes: UnsafeRawBufferPointer) -> [UInt8] in
            var hash = [UInt8](repeating: 0, count: Int(CC_MD5_DIGEST_LENGTH))
            CC_MD5(bytes.baseAddress, CC_LONG(data.count), &hash)
            return hash
        }
        return hash.map { String(format: "%02x", $0) }.joined()
    }
    
    var initials: String {
        return self.components(separatedBy: " ").filter { !$0.isEmpty }.reduce("") { ($0 == "" ? "" : "\($0.first!)") + "\($1.first!)" }
    }
}

extension Int {
    
    func withCommas() -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        return numberFormatter.string(from: NSNumber(value:self))!
    }
}

extension StringProtocol {
    
    var firstUppercased: String {
        guard let first = first else { return "" }
        return String(first).uppercased() + dropFirst()
    }
}

extension Character {
    
    fileprivate func isEmoji() -> Bool {
        return Character(UnicodeScalar(UInt32(0x1d000))!) <= self && self <= Character(UnicodeScalar(UInt32(0x1f77f))!)
            || Character(UnicodeScalar(UInt32(0x2100))!) <= self && self <= Character(UnicodeScalar(UInt32(0x26ff))!)
    }
}


extension Data {
    
    func toString() -> String {
        return String(data: self, encoding: .utf8)!
    }
    
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
    
    func toDictionary() -> [String: Any] {
        return (try? JSONSerialization.jsonObject(with: self)) as? [String: Any] ?? [:]
    }
    
}

extension Array {
    func toJson() -> String? {
        if JSONSerialization.isValidJSONObject(self) {
            if let jsonData = try? JSONSerialization.data(withJSONObject: self, options: .prettyPrinted) {
                return String(data: jsonData, encoding: String.Encoding.utf8)
            }
        }
        
        return nil
    }
}

extension Dictionary {
    var json: String? {
        if let jsonData = try? JSONSerialization.data(withJSONObject: self, options: .prettyPrinted) {
            return String(bytes: jsonData, encoding: String.Encoding.utf8)
        }
        
        return nil
    }
    
    func dict2json() -> String? {
        return json
    }
}

extension String {
    var digits: String {
        return components(separatedBy: CharacterSet.decimalDigits.inverted)
            .joined()
    }
    
    func base64Encoded() -> String? {
        if let data = self.data(using: .utf8) {
            return data.base64EncodedString()
        }
        return nil
    }

    func base64Decoded() -> String? {
        if let data = Data(base64Encoded: self, options: .ignoreUnknownCharacters) {
            return String(data: data, encoding: .utf8)
        }
        return nil
    }
    
    func toDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
}

extension Optional where Wrapped == String {
    
    var isEmpty: Bool {
        return self?.isEmpty ?? true
    }
    
    var value: String {
        guard let val = self else { return "" }
        return val
    }
}

extension Optional where Wrapped == Int {
    
    var isEmpty: Bool {
        if let _ = self { return true }
        else { return false }
    }
    
    var value: Int {
        guard let val = self else { return 0 }
        return val
    }
    
}

extension Optional where Wrapped == Float {
    
    var isEmpty: Bool {
        if let _ = self { return true }
        else { return false }
    }
    
    var value: Float {
        guard let val = self else { return 0.0 }
        return val
    }
    
}

extension Optional where Wrapped == Double {
    
    var isEmpty: Bool {
        if let _ = self { return true }
        else { return false }
    }
    
    var value: Double {
        guard let val = self else { return 0.0 }
        return val
    }
}
