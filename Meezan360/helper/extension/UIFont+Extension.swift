

import UIKit
import Foundation

extension UIFont {
    
    func sizeOfString(text: NSString, constrainedToWidth width: Double) -> CGSize {
        return text.boundingRect(with: CGSize(width: width, height: Double.greatestFiniteMagnitude),
                                 options: .usesLineFragmentOrigin,
                                 attributes: [NSAttributedString.Key.font: self],
                                 context: nil).size
    }
    
    func sizeOfString (string: String, constrainedToWidth width: Double) -> CGSize {
        return NSString(string: string).boundingRect(with: CGSize(width: width, height: Double.greatestFiniteMagnitude),
                                                     options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                                     attributes: [NSAttributedString.Key.font: self],
                                                     context: nil).size
    }
}
