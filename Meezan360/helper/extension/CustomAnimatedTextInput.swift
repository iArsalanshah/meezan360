//
//  CustomAnimatedTextInput.swift
//  Meezan360
//
//  Created by Akber Sayani on 19/10/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit
import SwiftValidator
import AnimatedTextInput

extension AnimatedTextInput: Validatable {
    open var validationText: String {
        return text ?? ""
    }
}

struct AppTextInputStyle: AnimatedTextInputStyle {
    let lineHeight: CGFloat = 1
    let yPlaceholderPositionOffset: CGFloat = 0
    let textAttributes: [String : Any]? = [:]
    let lineActiveColor = UIColor.white
    let lineInactiveColor = UIColor.white
    let placeholderInactiveColor = UIColor.white
    let activeColor = UIColor.white
    let inactiveColor = UIColor.white
    let errorColor = UIColor.red
    let textInputFont = UIFont(name: "SofiaProMedium", size: 18.0) ?? UIFont.systemFont(ofSize: 18.0)
    let textInputFontColor = UIColor.white
    let placeholderMinFontSize: CGFloat = 12.0
    let counterLabelFont: UIFont? = UIFont.systemFont(ofSize: 10.0)
    let leftMargin: CGFloat = 20.0
    let topMargin: CGFloat = 8.0
    let rightMargin: CGFloat = 0.0
    let bottomMargin: CGFloat = 0.0
    let yHintPositionOffset: CGFloat = 0.0
}

struct LightTextInputStyle: AnimatedTextInputStyle {
    let lineHeight: CGFloat = 0
    let yPlaceholderPositionOffset: CGFloat = 0
    let textAttributes: [String : Any]? = [:]
    let lineActiveColor = UIColor.black
    let lineInactiveColor = UIColor.black
    let placeholderInactiveColor = UIColor.gray
    let activeColor = UIColor.black
    let inactiveColor = UIColor.gray
    let errorColor = UIColor.red
    let textInputFont = UIFont(name: "SofiaProMedium", size: 18.0) ?? UIFont.systemFont(ofSize: 18.0)
    let textInputFontColor = UIColor.black
    let placeholderMinFontSize: CGFloat = 12.0
    let counterLabelFont: UIFont? = UIFont.systemFont(ofSize: 10.0)
    let leftMargin: CGFloat = 20.0
    let topMargin: CGFloat = 8.0
    let rightMargin: CGFloat = 0.0
    let bottomMargin: CGFloat = 0.0
    let yHintPositionOffset: CGFloat = 0.0
}
