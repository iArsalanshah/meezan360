//
//  Date+Extension.swift
//
//

import Foundation

extension Date {
    
    // Convert local time to UTC (or GMT)
    func toUTC() -> Date {
        let timezone = TimeZone.current
        let seconds = -TimeInterval(timezone.secondsFromGMT(for: self))
        return Date(timeInterval: seconds, since: self)
    }
    
    // Convert UTC (or GMT) to local time
    func toGMT() -> Date {
        let timezone = TimeZone.current
        let seconds = TimeInterval(timezone.secondsFromGMT(for: self))
        return Date(timeInterval: seconds, since: self)
    }
    
    func toString(withFormat format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        let myString = formatter.string(from: self)
        let yourDate = formatter.date(from: myString)
//        if AppStateManager.shared.isArabic() {
//            formatter.locale = Locale(identifier: "ar")
//        }
        formatter.dateFormat = format
        
        return formatter.string(from: yourDate!)
    }
    
    func getUTCFormateDate(withFormat format: String = "yyyy-MM-dd HH:mm:ss") -> String {
        let dateFormatter = DateFormatter()
        let timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.timeZone = timeZone
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
//
//    func isInSameWeek(date: Date) -> Bool {
//        return Calendar.current.isDate(self, equalTo: date, toGranularity: .weekOfYear)
//    }
//    func isInSameMonth(date: Date) -> Bool {
//        return Calendar.current.isDate(self, equalTo: date, toGranularity: .month)
//    }
//    func isInSameYear(date: Date) -> Bool {
//        return Calendar.current.isDate(self, equalTo: date, toGranularity: .year)
//    }
//    func isInSameDay(date: Date) -> Bool {
//        return Calendar.current.isDate(self, equalTo: date, toGranularity: .day)
//    }
//
//    var isInThisWeek: Bool {
//        return isInSameWeek(date: Date())
//    }
//
//    var isInToday: Bool {
//        return Calendar.current.isDateInToday(self)
//    }
//
//    var isInTheFuture: Bool {
//        return Date() < self
//    }
//
//    var isInThePast: Bool {
//        return self < Date()
//    }
//
//    /// Returns a Date with the specified days added to the one it is called with
//    func add(years: Int = 0, months: Int = 0, days: Int = 0, hours: Int = 0, minutes: Int = 0, seconds: Int = 0) -> Date {
//        var targetDay: Date
//        targetDay = Calendar.current.date(byAdding: .year, value: years, to: self)!
//        targetDay = Calendar.current.date(byAdding: .month, value: months, to: targetDay)!
//        targetDay = Calendar.current.date(byAdding: .day, value: days, to: targetDay)!
//        targetDay = Calendar.current.date(byAdding: .hour, value: hours, to: targetDay)!
//        targetDay = Calendar.current.date(byAdding: .minute, value: minutes, to: targetDay)!
//        targetDay = Calendar.current.date(byAdding: .second, value: seconds, to: targetDay)!
//        return targetDay
//    }
//
//    /// Returns a Date with the specified days subtracted from the one it is called with
//    func subtract(years: Int = 0, months: Int = 0, days: Int = 0, hours: Int = 0, minutes: Int = 0, seconds: Int = 0) -> Date {
//        let inverseYears = -1 * years
//        let inverseMonths = -1 * months
//        let inverseDays = -1 * days
//        let inverseHours = -1 * hours
//        let inverseMinutes = -1 * minutes
//        let inverseSeconds = -1 * seconds
//        return add(years: inverseYears, months: inverseMonths, days: inverseDays, hours: inverseHours, minutes: inverseMinutes, seconds: inverseSeconds)
//    }

}
