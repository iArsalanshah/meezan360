//
//  AlertManager.swift
//  Sphere
//
//  Created by Ali Abdul Jabbar on 05/09/2019.
//  Copyright © 2019 Ali Abdul Jabbar. All rights reserved.
//

import Foundation
import SwiftMessages

class AlertManager {	
	static func showAlertWith(title: String = Constants.AlertTypeTile.ALERT, message: String, on:UIViewController? = nil,forceOnWindow:Bool = false) {
		var on = on
		if on == nil { on = UIApplication.topVC() }
		var config = SwiftMessages.Config()
		config.presentationStyle = .top
		config.presentationContext = (on == nil) ? .automatic : .viewController(on!)
		let layout:MessageView.Layout = (on == nil) ? .cardView : ( (on?.navigationController != nil) ? .tabView : .cardView)
		let view = MessageView.viewFromNib(layout: layout)
		view.configureTheme(.error)
		
		let titleTxt = title
		let messageTxt = message
		
		view.configureContent(title: titleTxt, body: messageTxt)
		view.configureTheme(.error)
		//        view.configureTheme(backgroundColor: UIColor.hexStringToUIColor(hex: "A63232"), foregroundColor: UIColor.white, iconImage: nil, iconText: nil)
		view.button?.isHidden = true
		SwiftMessages.show(config: config, view: view)
	}
	
	static func showErrorWith(title: String = Constants.AlertTypeTile.ERROR, message: String, on:UIViewController? = nil,forceOnWindow:Bool = false) {
		var on = on
		if on == nil { on = UIApplication.topVC() }
		var config = SwiftMessages.Config()
		config.presentationStyle = .top
		config.presentationContext = (on == nil) ? .automatic : (forceOnWindow ? .automatic : .viewController(on!))
		let view = MessageView.viewFromNib(layout: (on == nil) ? .cardView : ( forceOnWindow ? .cardView : .tabView))
		view.configureTheme(.error)
		let titleTxt = title
		let messageTxt = message
		
		view.configureContent(title: titleTxt, body: messageTxt)
		view.button?.isHidden = true
		SwiftMessages.show(config: config, view: view)
	}
	
	static func showErrorBar(title: String = "", message: String, on:UIViewController? = nil,forceOnWindow:Bool = false){
		var on = on
		if on == nil { on = UIApplication.topVC() }
		var config = SwiftMessages.Config()
		config.presentationStyle = .top
		config.presentationContext = (on == nil) ? .automatic : (forceOnWindow ? .automatic : .viewController(on!))
		let view = MessageView.viewFromNib(layout: .statusLine)
		view.configureTheme(.error)
		let titleTxt = title
		let messageTxt = message
		
		view.configureContent(title: titleTxt, body: messageTxt)
		//error.button?.setTitle("Stop", for: .normal)
		view.button?.isHidden = true
		SwiftMessages.show(config: config, view: view)
	}
	
	static func showSuccessBar(title: String = "", message: String, on:UIViewController? = nil,forceOnWindow:Bool = false){
		var on = on
		if on == nil { on = UIApplication.topVC() }
		var config = SwiftMessages.Config()
		config.presentationStyle = .top
		config.presentationContext = (on == nil) ? .automatic : (forceOnWindow ? .automatic : .viewController(on!))
		let view = MessageView.viewFromNib(layout: .statusLine)
		view.configureTheme(.success)
		let titleTxt = title
		let messageTxt = message
		
		view.configureContent(title: titleTxt, body: messageTxt)
		//error.button?.setTitle("Stop", for: .normal)
		view.button?.isHidden = true
		SwiftMessages.show(config: config, view: view)
	}
	
	static func showWarningBar(title: String = "", message: String, on:UIViewController? = nil,forceOnWindow:Bool = false){
		var on = on
		if on == nil { on = UIApplication.topVC() }
		var config = SwiftMessages.Config()
		config.presentationStyle = .top
		config.presentationContext = (on == nil) ? .automatic : (forceOnWindow ? .automatic : .viewController(on!))
		let view = MessageView.viewFromNib(layout: .statusLine)
		view.configureTheme(.warning)
		let titleTxt = title
		let messageTxt = message
		
		view.configureContent(title: titleTxt, body: messageTxt)
		//error.button?.setTitle("Stop", for: .normal)
		view.button?.isHidden = true
		SwiftMessages.show(config: config, view: view)
	}
	
	static func showSuccessMessage(title: String = "", message: String, on:UIViewController? = nil, forceOnWindow:Bool = false){
		var on = on
		if on == nil { on = UIApplication.topVC() }
		var config = SwiftMessages.Config()
		config.presentationStyle = .top
		config.presentationContext = (on == nil) ? .automatic : (forceOnWindow ? .automatic : .viewController(on!))
		let view = MessageView.viewFromNib(layout: (on == nil) ? .cardView : (forceOnWindow ? .cardView : .tabView))
		view.configureTheme(.success)
		let titleTxt = title
		let messageTxt = message
		
		view.configureContent(title: titleTxt, body: messageTxt)
		view.button?.isHidden = true
		SwiftMessages.show(config: config, view: view)
	}
}
