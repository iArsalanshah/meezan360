//
//  ActivityIndicatorManager.swift
//  Meezan360
//
//  Created by Akber Sayani on 19/10/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class ActivityIndicatorManager: NSObject {
    static func showLoader() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        let size = CGSize(width: 50, height: 50)
        let bgColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        let color = UIColor(named: "colorCustomPurple") ?? UIColor.systemPurple
        let activityData = ActivityData(size: size, message: "", messageFont: UIFont.systemFont(ofSize: 12), type: .circleStrokeSpin, color: color, padding: 0, displayTimeThreshold: 0, minimumDisplayTime: 1, backgroundColor: bgColor, textColor: .black)
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData, nil)
    }
    
    static func hideLoader() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
    }
}
