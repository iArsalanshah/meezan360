

import UIKit

class HorizontalCollectionViewFlowLayout: UICollectionViewFlowLayout {
    
    private let sectionInsets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
    private let sellSize = CGSize(width: 140, height: 120)
    
//    init(sectionInsets: UIEdgeInsets, itemSize: CGSize) {
//        super.init()
//        self.sectionInsets = sectionInsets
//        self.sellSize = itemSize
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
    
    override func awakeFromNib() {
        self.itemSize = sellSize
        self.minimumInteritemSpacing = sectionInsets.left
        self.minimumLineSpacing = sectionInsets.left
        self.scrollDirection = .horizontal
        self.sectionInset = sectionInsets
    }
    
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {

        // Page width used for estimating and calculating paging.
        let pageWidth = self.itemSize.width + self.minimumInteritemSpacing

        // Make an estimation of the current page position.
        let approximatePage = self.collectionView!.contentOffset.x/pageWidth

        // Determine the current page based on velocity.
        let currentPage = (velocity.x < 0.0) ? floor(approximatePage) : ceil(approximatePage)

        // Create custom flickVelocity.
        let flickVelocity = velocity.x * 0.3

        // Check how many pages the user flicked, if <= 1 then flickedPages should return 0.
        let flickedPages = (abs(round(flickVelocity)) <= 1) ? 0 : round(flickVelocity)

        // Calculate newHorizontalOffset.
        let newHorizontalOffset = ((currentPage + flickedPages) * pageWidth) - self.collectionView!.contentInset.left

        return CGPoint(x: newHorizontalOffset, y: proposedContentOffset.y)
    }

    
//    override func targetContentOffset(
//        forProposedContentOffset proposedContentOffset: CGPoint,
//        withScrollingVelocity velocity: CGPoint
//        ) -> CGPoint {
//        var _proposedContentOffset = CGPoint(
//            x: proposedContentOffset.x, y: proposedContentOffset.y
//        )
//        var offSetAdjustment: CGFloat = CGFloat.greatestFiniteMagnitude
//        let horizontalCenter: CGFloat = CGFloat(
//            proposedContentOffset.x + (self.collectionView!.bounds.size.width / 2.0)
//        )
//
//        let targetRect = CGRect(
//            x: proposedContentOffset.x,
//            y: 0.0,
//            width: self.collectionView!.bounds.size.width,
//            height: self.collectionView!.bounds.size.height
//        )
//
//        let array: [UICollectionViewLayoutAttributes] =
//            self.layoutAttributesForElements(in: targetRect)!
//                as [UICollectionViewLayoutAttributes]
//        for layoutAttributes: UICollectionViewLayoutAttributes in array {
//            if layoutAttributes.representedElementCategory == UICollectionElementCategory.cell {
//                let itemHorizontalCenter: CGFloat = layoutAttributes.center.x
//                if abs(itemHorizontalCenter - horizontalCenter) < abs(offSetAdjustment) {
//                    offSetAdjustment = itemHorizontalCenter - horizontalCenter
//                }
//            }
//        }
//
//        var nextOffset: CGFloat = proposedContentOffset.x + offSetAdjustment
//
//        repeat {
//            _proposedContentOffset.x = nextOffset
//            let deltaX = proposedContentOffset.x - self.collectionView!.contentOffset.x
//            let velX = velocity.x
//
//            if
//                deltaX == 0.0 || velX == 0 || (velX > 0.0 && deltaX > 0.0) ||
//                    (velX < 0.0 && deltaX < 0.0)
//            {
//                break
//            }
//
//            if velocity.x > 0.0 {
//                nextOffset = nextOffset + self.snapStep()
//            } else if velocity.x < 0.0 {
//                nextOffset = nextOffset - self.snapStep()
//            }
//        } while self.isValidOffset(offset: nextOffset)
//
//        _proposedContentOffset.y = 0.0
//
//        return _proposedContentOffset
//    }
//
//    func isValidOffset(offset: CGFloat) -> Bool {
//        return (offset >= CGFloat(self.minContentOffset()) &&
//            offset <= CGFloat(self.maxContentOffset()))
//    }
//
//    func minContentOffset() -> CGFloat {
//        return -CGFloat(self.collectionView!.contentInset.left)
//    }
//
//    func maxContentOffset() -> CGFloat {
//        return CGFloat(
//            self.minContentOffset() + self.collectionView!.contentSize.width - self.itemSize.width
//        )
//    }
//
//    func snapStep() -> CGFloat {
//        return self.itemSize.width + self.minimumLineSpacing
//    }

    
}
