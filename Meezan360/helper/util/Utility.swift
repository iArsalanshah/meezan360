
import Foundation
import UIKit
import AVFoundation
//import NVActivityIndicatorView
//import Toast_Swift

class Utility/*: NVActivityIndicatorViewable*/ {
    
    func roundAndFormatFloat(floatToReturn : Float, numDecimalPlaces: Int) -> String{
        
        let formattedNumber = String(format: "%.\(numDecimalPlaces)f", floatToReturn)
        return formattedNumber
        
    }

    static func verifyUrl (urlString: String?) -> Bool {
        //Check for nil
        if let urlString = urlString {
            // create NSURL instance
            if let url = NSURL(string: urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) {
                // check if your application can open the NSURL instance
                return UIApplication.shared.canOpenURL(url as URL)
            }
        }
        return false
    }
    
    static func printFonts() {
        for familyName in UIFont.familyNames {
            print("\n-- \(familyName) \n")
            for fontName in UIFont.fontNames(forFamilyName: familyName) {
                print(fontName)
            }
        }
    }

    func topViewController(base: UIViewController? = (Constants.APP_DELEGATE).window?.rootViewController) -> UIViewController? {
    
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
    
    static func showWillBeImplenentAlert() {
        showAlert(title: "Message", message: "This functionality will be implmented in next build")
    }
    
    static func showInputDialog(title:String? = nil,
                         subtitle:String? = nil,
                         actionTitle:String? = "Add",
                         cancelTitle:String? = "Cancel",
                         inputPlaceholder:String? = nil,
                         inputKeyboardType:UIKeyboardType = UIKeyboardType.default,
                         cancelHandler: ((UIAlertAction) -> Swift.Void)? = nil,
                         actionHandler: ((_ text: String?) -> Void)? = nil) {
        
        let alert = UIAlertController(title: title, message: subtitle, preferredStyle: .alert)
        alert.addTextField { (textField:UITextField) in
            textField.placeholder = inputPlaceholder
            textField.keyboardType = inputKeyboardType
        }
        alert.addAction(UIAlertAction(title: actionTitle, style: .destructive, handler: { (action:UIAlertAction) in
            guard let textField =  alert.textFields?.first else {
                actionHandler?(nil)
                return
            }
            actionHandler?(textField.text)
        }))
        alert.addAction(UIAlertAction(title: cancelTitle, style: .cancel, handler: cancelHandler))
        
        Utility().topViewController()!.present(alert, animated: true){}
    }
    
    static func showInputDialog(title:String? = nil,
                                subtitle:String? = nil,
                                actionTitle:String? = "Add",
                                inputPlaceholder:String? = nil,
                                inputKeyboardType:UIKeyboardType = UIKeyboardType.default,
                                textFieldDelegate: UITextFieldDelegate?,
                                actionHandler: ((_ text: String?) -> Void)? = nil) {
        
        let alert = UIAlertController(title: title, message: subtitle, preferredStyle: .alert)
        alert.addTextField { (textField:UITextField) in
            textField.placeholder = inputPlaceholder
            textField.keyboardType = inputKeyboardType
            textField.delegate = textFieldDelegate
        }
        alert.addAction(UIAlertAction(title: actionTitle, style: .destructive, handler: { (action:UIAlertAction) in
            guard let textField =  alert.textFields?.first else {
                actionHandler?(nil)
                return
            }
            actionHandler?(textField.text)
        }))
        
        Utility().topViewController()!.present(alert, animated: true){}
    }
    
    static func showInputDialog(title:String? = nil,
                                subtitle:String? = nil,
                                actionTitle:String? = "Add",
                                inputPlaceholder1:String? = nil,
                                inputKeyboardType1:UIKeyboardType = UIKeyboardType.default,
                                textFieldDelegate1: UITextFieldDelegate?,
                                inputPlaceholder2:String? = nil,
                                inputKeyboardType2:UIKeyboardType = UIKeyboardType.default,
                                textFieldDelegate2: UITextFieldDelegate?,
                                actionHandler: ((_ text1: String?, _ text2: String?) -> Void)? = nil) {
        
        let alert = UIAlertController(title: title, message: subtitle, preferredStyle: .alert)
        alert.addTextField { (textField:UITextField) in
            textField.tag = 1
            textField.placeholder = inputPlaceholder1
            textField.keyboardType = inputKeyboardType1
            textField.delegate = textFieldDelegate1
        }
        alert.addTextField { (textField:UITextField) in
            textField.tag = 2
            textField.placeholder = inputPlaceholder2
            textField.keyboardType = inputKeyboardType2
            textField.delegate = textFieldDelegate2
        }
        alert.addAction(UIAlertAction(title: actionTitle, style: .destructive, handler: { (action:UIAlertAction) in
            var text1 = ""
            var text2 = ""
            alert.textFields?.forEach({ (textField) in
                if textField.tag == 1 {
                    text1 = textField.text.value
                } else if textField.tag == 2 {
                    text2 = textField.text.value
                }
            })
            actionHandler?(text1,text2)
        }))
        
        Utility().topViewController()!.present(alert, animated: true){}
    }

    static func showAlert(title:String?, message:String?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default) { _ in })
        Utility().topViewController()!.present(alert, animated: true){}
    }
    
    static func showAlert(title:String?, message:String?, closure: @escaping (UIAlertAction) -> Void) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default) { onClick in
            closure(onClick)
        })
        Utility().topViewController()!.present(alert, animated: true){}
    }
    
    static func showAlertWithYesNo(title:String?, message:String?, closure: @escaping (UIAlertAction) -> Void) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "No", style: .default) { _ in })
        alert.addAction(UIAlertAction(title: "Yes", style: .default) { onClick in
            closure(onClick)
        })
        Utility().topViewController()!.present(alert, animated: true){}
    }
    
    static func showActionsheet(sender: UIView, viewController: UIViewController, title: String, message: String?, actions: [(String, UIAlertAction.Style)], completion: @escaping (_ index: Int) -> Void) {
        let alertViewController = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        for (index, (title, style)) in actions.enumerated() {
            let alertAction = UIAlertAction(title: title, style: style) { (_) in
                completion(index)
            }
            alertViewController.addAction(alertAction)
        }
        if let popoverController = alertViewController.popoverPresentationController {
            popoverController.sourceView = sender
            popoverController.sourceRect = CGRect(x: sender.bounds.midX, y: sender.bounds.midY, width: 0, height: 0)
        }
        viewController.present(alertViewController, animated: true, completion: nil)
    }
    
//    static func showToastMessage(_ message: String, duration: TimeInterval, position: ToastPosition) {
//        Utility().topViewController()?.view.makeToast(message, duration: duration, position: position)
//    }
    
//    static func showShortToastBottom(_ message: String) {
//        Utility().topViewController()?.view.makeToast(message, duration: 2.5, position: ToastPosition.bottom)
//    }
//
//    static func showShortToastCenter(_ message: String) {
//        Utility().topViewController()?.view.makeToast(message, duration: 2.5, position: ToastPosition.center)
//    }
    
    static func resizeImage(image: UIImage,  targetSize: CGFloat) -> UIImage {
        
        guard (image.size.width > 1024 || image.size.height > 1024) else {
            return image;
        }
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newRect: CGRect = CGRect.zero;
        
        if(image.size.width > image.size.height) {
            newRect.size = CGSize(width: targetSize, height: targetSize * (image.size.height / image.size.width))
        } else {
            newRect.size = CGSize(width: targetSize * (image.size.width / image.size.height), height: targetSize)
        }
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newRect.size, false, 1.0)
        image.draw(in: newRect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    static func thumbnailForVideoAtURL(url: URL) -> UIImage? {
    
        let asset = AVAsset(url: url)
        let assetImageGenerator = AVAssetImageGenerator(asset: asset)
        assetImageGenerator.appliesPreferredTrackTransform=true
        
        var time = asset.duration
        time.value = min(time.value, 2)
        
        do {
            let imageRef = try assetImageGenerator.copyCGImage(at: time, actualTime: nil)
            return UIImage(cgImage: imageRef)
        } catch {
            print("error")
            return nil
        }
    }

    static func delay(delay:Double, closure:@escaping ()->()) {
        DispatchQueue.main.asyncAfter(
            deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
    }

//    static func showLoader() {
//        UIApplication.shared.isNetworkActivityIndicatorVisible = true
//        let size = CGSize(width: 50, height: 50)
//        let bgColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
//        let spinnerColor = Constants.appColor//UIColor(named: "AppColor")
//        let activityData = ActivityData(size: size, message: "", messageFont: UIFont.systemFont(ofSize: 12), type: .circleStrokeSpin,
//                                        color: spinnerColor, padding: 0, displayTimeThreshold: 0, minimumDisplayTime: 1,
//                                        backgroundColor: bgColor, textColor: UIColor.black)
//
//        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData, nil)
//    }
    
//    static func hideLoader() {
//        UIApplication.shared.isNetworkActivityIndicatorVisible = false
//        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
//    }
    
    static func randomString(length: Int, isNumeric: Bool = false) -> String {
        let letters = isNumeric ? "0123456789" : "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        
        return String((0..<length).map{ _ in letters.randomElement()! })
    }

    static func jsonEncode(object: AnyObject?) -> Data? {
        if JSONSerialization.isValidJSONObject(object as? Any ?? "") {
            return try? JSONSerialization.data(withJSONObject: object, options: .prettyPrinted)
        }
        return nil
    }

}











