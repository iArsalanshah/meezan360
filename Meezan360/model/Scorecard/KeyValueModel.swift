//
//  KeyValueModel.swift
//  Hunters
//
//  Created by Akber Sayani on 24/11/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit
import ObjectMapper

class KeyValueModel: NSObject, Mappable {
    var key: String?
    var value: String?
    
    override init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        key <- map["key"]
        value <- map["val"]
    }
}
