//
//  ScoreModel.swift
//  Meezan360
//
//  Created by Akber Sayani on 20/10/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit
import ObjectMapper

class ScoreModel: NSObject, Mappable {
    var id: String?
    var name: String?
    var enable: String?
    var colorHexCode: String?
    var score: String?

    override init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        enable <- map["enable"]
        colorHexCode <- map["color_code"]
        score <- map["score"]
    }
}
