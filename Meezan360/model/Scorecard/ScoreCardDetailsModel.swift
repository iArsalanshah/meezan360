//
//  ScoreCardDetailsModel.swift
//  Hunters
//
//  Created by Akber Sayani on 08/11/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit
import ObjectMapper

class ScoreCardDetailsModel: NSObject, Mappable {
    var head: ScorecardHeadModel?
    var values: [PageValueModel]?

    override init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        head <- map["head"]
        values <- map["table"]
    }
}
