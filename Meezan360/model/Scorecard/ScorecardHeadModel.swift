//
//  ScorecardHeadModel.swift
//  Hunters
//
//  Created by Akber Sayani on 08/11/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit
import ObjectMapper

class ScorecardHeadModel: NSObject, Mappable {
    var name: String?
    var title: String?
    var score: String?
    var kpi: String?
    var percentage: String?
    
    override init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        title <- map["title"]
        score <- map["score"]
        kpi <- map["kpi"]
        percentage <- map["percentage"]
    }    
}
