//
//  BranchModel.swift
//  Meezan360
//
//  Created by Akber Sayani on 24/10/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit
import ObjectMapper

class SearchModel: NSObject, Mappable, NSCoding {
    var branchCode: String?
    var branchName: String?
    var areaName: String?
    var regionName: String?
    var searchDate: String?    

    override init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    convenience required init?(coder decoder: NSCoder) {
        self.init()
        
        self.branchCode = decoder.decodeObject(forKey: "branch_code") as? String
        self.branchName = decoder.decodeObject(forKey: "branch_name") as? String
        self.areaName = decoder.decodeObject(forKey: "area_name") as? String
        self.regionName = decoder.decodeObject(forKey: "region_name") as? String
        self.searchDate = decoder.decodeObject(forKey: "search_date") as? String
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(self.branchCode, forKey: "branch_code")
        coder.encode(self.branchName, forKey: "branch_name")
        coder.encode(self.areaName, forKey: "area_name")
        coder.encode(self.regionName, forKey: "region_name")
        coder.encode(self.searchDate, forKey: "search_date")
    }
    
    func mapping(map: Map) {
        branchCode <- map["branch_code"]
        branchName <- map["branch_name"]
        areaName <- map["area_name"]
        regionName <- map["region_name"]
    }
}
