//
//  PhoneContactModel.swift
//  Hunters
//
//  Created by Akber Sayani on 14/12/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit
import ObjectMapper

class PhoneContactModel: NSObject, Mappable {
    var name: String?
    var designation: String?
    var phoneNumber: String?

    override init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        designation <- map["designation"]
        phoneNumber <- map["phone_number"]
    }
}
