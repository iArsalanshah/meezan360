//
//  Model.swift
//  ChartViewTestProject
//
//  Created by Syed Arsalan Shah on 10/5/19.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import Foundation

struct HomeEntity {
    let type: String?
    let size: String?
    let percentage: String?
}

struct DetailsTableEntity {
    let title: String
    let columnTitle: [String]?
    let rowData: [String: [String]]
    
    var tableRowsCount: Int {
        return 1/*Header*/ + (columnTitle == nil ? 0 : 1) /*Column*/ + rowData.count
    }
    
    var sectionHeight: Double {
        let headerHeight = 44
        let columnHeight = columnTitle == nil ? 0 : 25
        let rowsHeight = rowData.count * 25
        return Double(headerHeight + columnHeight + rowsHeight)
    }
    
    func getRowData(reqRow: Int) -> [String] {
        var returnArray = [String]()
        for (index, data) in rowData.enumerated() {
            if index == reqRow {
                returnArray.append(data.key)
                returnArray+=data.value
            }
        }
        return returnArray
    }
    
}

struct DummyData {
    
    func homeTopBarData() -> [HomeEntity] {
        return [
            HomeEntity(type: "Current", size: "528M", percentage: "44%"),
            HomeEntity(type: "Savings", size: "355M", percentage: "27%"),
            HomeEntity(type: "Term Deposite", size: "458M", percentage: "38%"),
            HomeEntity(type: "Total", size: "1,312", percentage: nil)
        ]
    }
    
    func detailTopBarData() -> [HomeEntity] {
        return [
            HomeEntity(type: "Year 2018", size: "528M", percentage: nil),
            HomeEntity(type: "Year 2019", size: "355M", percentage: nil),
            HomeEntity(type: "YTD", size: "582M", percentage: nil),
            HomeEntity(type: "Growth", size: "98%", percentage: nil)
        ]
    }
    
    func sellDetailTopBarData() -> [HomeEntity] {
        return [
            HomeEntity(type: "Budget", size: "528M", percentage: nil),
            HomeEntity(type: "EOP", size: "355M", percentage: nil),
            HomeEntity(type: "Dec Target", size: "582M", percentage: nil),
        ]
    }
    
    func detailTableData() -> [DetailsTableEntity] {
        return [
            DetailsTableEntity(title: "ETB NTB MOVEMENT", columnTitle: ["CA", "SA", "TD", "Total"],
                               rowData: ["Month End Budget" : ["15.37M", "12.45", "15.37M", "12.45M"]
                                , "Year End Budget" : ["15.37M", "12.45", "15.37M", "12.45M"]
                                ,"Previous" : ["15.37M", "12.45", "15.37M", "12.45M"], "Today" : ["15.37M", "12.45", "15.37M", "12.45M"],
                                 "Variance" : ["15.37M", "12.45", "15.37M", "12.45M"], "Variance (Month)" : ["15.37M", "12.45", "15.37M", "12.45M"],
                                 "Variance (Year)" : ["15.37M", "12.45", "15.37M", "12.45M"]
                ]
            )
            ,DetailsTableEntity(title: "Top 150 Customer", columnTitle: nil,
                               rowData: ["Top 150" : ["15.37M", "12.45", "15.37M", "12.45M"], "Total" : ["15.37M", "12.45", "15.37M", "12.45M"],
                                         "Top to Total" : ["15.37M", "12.45", "15.37M", "12.45M"]]),
            DetailsTableEntity(title: "On Off Branches", columnTitle: nil,
                               rowData: ["On Branches" : ["45", "45", "45", "45"], "Off Branches" : ["15", "12", "15", "12"]])
        ]
    }
    
    func sellDetailTableData() -> [DetailsTableEntity] {
        return [
            DetailsTableEntity(title: "",
                               columnTitle: ["Budget", "EOP", "Dec Target"],
                               rowData: [
                                "HYDERABAD" : ["15.37M", "12.45", "15.37M"],
                                "BALOCHISTAN" : ["15.37M", "12.45", "15.37M"],
                                "KPK" : ["15.37M", "12.45", "12.45M"],
                                "FAISALABAD" : ["15.37M", "15.37M", "12.45M"],
                                "MULTAN" : ["15.37M", "12.45", "15.37M"],
                                "LAHORE" : ["15.37M", "15.37M", "12.45M"],
                                "KARACHI" : ["15.37M", "15.37M", "12.45M"]
                ]
            )
        ]
    }
    
    func currencyPositionTableData() -> [DetailsTableEntity] {
        return [
            DetailsTableEntity(title: "CURRENCY POSITION", columnTitle: ["Holding", "Deposit", "Ratio"],
                               rowData: [
                                "PKR" : ["15.37M", "12.45M", "58%"],
                                "EUR" : ["15.37M", "12.45M", "56%"],
                                "GBP" : ["15.37M", "12.45M", "62%"],
                                "USD" : ["15.37M", "12.45M", "65%"],
                                "" : ["0.37M", "0.45M", "70%"],
                                "Total" : ["15.37M", "12.45", "55%"]
                ]
            )
        ]
    }
}
