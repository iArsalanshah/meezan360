//
//  CashKeyValueModel.swift
//  Meezan360
//
//  Created by Akber Sayani on 27/10/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit
import ObjectMapper

class CashKeyValueModel: NSObject, Mappable {
    var key: String?
    var value1: String?
    var value2: String?
    var color: String?
    
    override init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        key <- map["title"]
        value1 <- map["val"]
        value2 <- map["val1"]
        color <- map["color"]
    }
}
