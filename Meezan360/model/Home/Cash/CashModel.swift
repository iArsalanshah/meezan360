//
//  CashModel.swift
//  Meezan360
//
//  Created by Akber Sayani on 27/10/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit
import ObjectMapper

class CashModel: NSObject, Mappable {
    var headValues: [HeadValueModel]?
    var currenty: [CurrencyModel]?
    var cashLimit: [CashKeyValueModel]?
    var cashPosition: [CashKeyValueModel]?
    var balance: [CashKeyValueModel]?
    var summary: DashboardSummaryModel?

    override init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        headValues <- map["head"]
        currenty <- map["currency"]
        cashLimit <- map["cashlimit"]
        balance <- map["balance"]
        cashPosition <- map["cashposition"]
        summary <- map["summary"]
    }
}
