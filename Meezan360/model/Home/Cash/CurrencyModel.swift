//
//  CurrencyModel.swift
//  Meezan360
//
//  Created by Akber Sayani on 27/10/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit
import ObjectMapper

class CurrencyModel: NSObject, Mappable {
    var title: String?
    var holding: String?
    var deposit: String?
    var ratio: String?

    override init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        title <- map["title"]
        holding <- map["holding"]
        deposit <- map["deposit"]
        ratio <- map["ratio"]
    }
}
