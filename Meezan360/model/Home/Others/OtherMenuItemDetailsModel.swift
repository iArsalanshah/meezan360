//
//  OtherMenuItemDetailsModel.swift
//  Meezan360
//
//  Created by Akber Sayani on 30/10/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit
import ObjectMapper

class OtherMenuItemDetailsModel: NSObject, Mappable {
    var title: String?
    var value1: String?
    var value2: String?
    var value3: String?
    var value4: String?
    var value5: String?

    override init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        title <- map["title"]
        
        // 360_compliance
        value1 <- map["tmir"]
        value2 <- map["ecdd"]
        
        // 360_crossSell
        value1 <- map["budget"]
        value2 <- map["eop"]
        value3 <- map["dectarget"]

        // 360_finance
        value1 <- map["smetrade"]
        value2 <- map["commtrade"]
        value3 <- map["smefinance"]
        value4 <- map["commfinance"]
        
        // 360_control
        value1 <- map["shariaaudit"]
        value2 <- map["internalaudit"]
        value3 <- map["tailaccount"]
        value4 <- map["servicequality"]
        value5 <- map["reassurance"]
        
        // 360_profitability
        value1 <- map["profitability"]
        value2 <- map["roa"]
        value3 <- map["cof"]

        // 360_premium
        value1 <- map["relationship"]
        value2 <- map["deposit"]
        value3 <- map["upgradations"]
    }
}
