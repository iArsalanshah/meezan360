    //
//  OtherMenuDetailsModel.swift
//  Meezan360
//
//  Created by Akber Sayani on 30/10/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit
import ObjectMapper

class OtherMenuItemModel: NSObject, Mappable {
    var headValues: [HeadValueModel]?
    var dataValues: [OtherMenuItemDetailsModel]?
    var summary: DashboardSummaryModel?
    
    override init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        headValues <- map["head"]
        summary <- map["summary"]        
        dataValues <- map["value"]
        dataValues <- map["crosssell"]
    }
}
