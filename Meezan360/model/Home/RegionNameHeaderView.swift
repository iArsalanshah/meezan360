//
//  RegionNameHeaderView.swift
//  Hunters
//
//  Created by Akber Sayani on 23/11/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit

class RegionNameHeaderView: UICollectionReusableView {
    @IBOutlet weak var regionNameLabel: UILabel!
}
