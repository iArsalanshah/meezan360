//
//  DashboardModel.swift
//  Meezan360
//
//  Created by Akber Sayani on 19/10/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit
import ObjectMapper

class DashboardModel: NSObject, Mappable {
    var id: String?
    var name: String?
    var syncDate: String?
    var identifier: String?
    var defaultValue: String?
    var colorHexCode: String?
    var summary: DashboardSummaryModel?
    var headValues: [HeadValueModel]?
    var graphValues: [GraphValueModel]?
    
    override init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        syncDate <- map["sync_date"]
        identifier <- map["identifier"]
        headValues <- map["head"]
        summary <- map["summary"]
        graphValues <- map["graph"]
        defaultValue <- map["default"]
        colorHexCode <- map["color_code"]
    }
}
