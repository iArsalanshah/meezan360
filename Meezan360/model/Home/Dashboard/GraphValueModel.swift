//
//  GraphValueModel.swift
//  Meezan360
//
//  Created by Akber Sayani on 19/10/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit
import ObjectMapper

class GraphValueModel: NSObject, Mappable {
    var name: String?
    var colorHexCode: String?
    var values: [String]?
    
    override init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        colorHexCode <- map["color"]
        values <- map["values"]
    }
}
