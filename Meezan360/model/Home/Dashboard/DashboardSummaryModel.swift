//
//  DashboardSummaryModel.swift
//  Hunters
//
//  Created by Akber Sayani on 23/11/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit
import ObjectMapper

class DashboardSummaryModel: NSObject,Mappable {
    var voiceText: String?
    var regionName: String?
    
    override init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        voiceText <- map["voicetext"]
        regionName <- map["region_name"]
    }    
}
