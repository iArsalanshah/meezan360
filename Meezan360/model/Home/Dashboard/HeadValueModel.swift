//
//  HeadValueModel.swift
//  Meezan360
//
//  Created by Akber Sayani on 19/10/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit
import ObjectMapper

class HeadValueModel: NSObject, Mappable {
    var key: String?
    var value1: String?
    var value2: String?
    
    override init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        key <- map["key"]
        value1 <- map["val1"]
        value2 <- map["val2"]
    }
}
