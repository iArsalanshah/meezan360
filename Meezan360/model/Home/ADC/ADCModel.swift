//
//  ADCModel.swift
//  Meezan360
//
//  Created by Akber Sayani on 02/11/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit
import ObjectMapper

class ADCModel: NSObject, Mappable {
    var headValues: [HeadValueModel]?
    var pie: [CashKeyValueModel]?
    var summary: DashboardSummaryModel?

    override init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        headValues <- map["head"]
        pie <- map["pie"]
        summary <- map["summary"]
    }
}
