//
//  ADCDebitCardModel.swift
//  Meezan360
//
//  Created by Akber Sayani on 02/11/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit
import ObjectMapper

class ADCDebitCardModel: NSObject, Mappable {
    var title: String?
    var actual: String?
    var budget: String?
    var variance: String?

    override init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        title <- map["title"]
        actual <- map["actual"]
        budget <- map["budget"]
        variance <- map["variance"]
    }
}
