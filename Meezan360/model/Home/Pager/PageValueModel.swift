//
//  PageValueModel.swift
//  Meezan360
//
//  Created by Akber Sayani on 07/11/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit
import ObjectMapper

class PageValueModel: NSObject, Mappable {
    var title: String?
    var value: String?
    var value1: String?
    var value2: String?
    var value3: String?
    var value4: String?
    var value5: String?
    var value6: String?

    override init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        title <- map["title"]
        value <- map["val"]
        
        // values for movement
        value1 <- map["today"]
        value2 <- map["lastday"]
        value3 <- map["variance"]

        // values for budget
        value1 <- map["monthendbudget"]
        value2 <- map["variancemonth"]
        value3 <- map["yearendbudget"]
        value4 <- map["varianceyear"]

        // values for top 150
        value1 <- map["customer_name"]
        value2 <- map["top150"]
        value3 <- map["previous"]
        value4 <- map["variance"]

        // values for branches
        value1 <- map["on"]
        value2 <- map["off"]
        
        // values for cash currency
        value1 <- map["holding"]
        value2 <- map["deposit"]
        value3 <- map["ratio"]
        
        // values for wealth management
        value1 <- map["budget"]
        value2 <- map["eop"]
        value3 <- map["dectarget"]
        
        // values for score card details
        value1 <- map["val1"]
        value2 <- map["val2"]
        value3 <- map["val3"]
        value4 <- map["val4"]
        value5 <- map["val5"]
        value6 <- map["val6"]
    }
}
