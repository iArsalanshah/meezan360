//
//  PageExtendedValueModel.swift
//  Hunters
//
//  Created by Akber Sayani on 21/12/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit
import ObjectMapper

class PageExtendedValueModel: NSObject, Mappable {
    var title: String?
    var value1: String?
    var value2: String?
    var value3: String?
    var value4: String?
    var value5: String?
    var value6: String?
    var value7: String?
    
    override init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        title <- map["title"]
        
        // values for controls and compliance
        value1 <- map["val"]
        value2 <- map["val1"]
        value3 <- map["val2"]
        value4 <- map["val3"]
        value5 <- map["val4"]
        value6 <- map["val5"]
        value7 <- map["val6"]
    }
}
