//
//  PagerSubValueModel.swift
//  Hunters
//
//  Created by Akber Sayani on 24/11/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit
import ObjectMapper

class PagerSubValueModel: NSObject, Mappable {
    var title: String?
    var subTitle: String?
    var value1: String?
    var subValue1: String?
    var value2: String?
    var subValue2: String?
    var value3: String?
    var subValue3: String?
    var value4: String?
    var subValue4: String?
    
    override init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        title <- map["title"]
        subTitle <- map["noofbranches"]
        
        value1 <- map["daycaactual"]
        subValue1 <- map["daycaactualpercentage"]
        value2 <- map["monthtargetcaactual"]
        subValue2 <- map["monthtargetcapercentage"]
        value3 <- map["yeartargetcaactual"]
        subValue3 <- map["yeartargetcapercentage"]
        value4 <- map["variancemonth"]
        subValue4 <- map["varianceyear"]
    }
}
