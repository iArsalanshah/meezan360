//
//  PagerModel.swift
//  Meezan360
//
//  Created by Akber Sayani on 07/11/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit
import ObjectMapper

class PagerModel: NSObject, Mappable {
    var title: String?
    var values: [PageValueModel]?
    var headValues: [HeadValueModel]?
    var summary: DashboardSummaryModel?

    override init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        title <- map["title"]
        values <- map["value"]
        headValues <- map["head"]
        summary <- map["summary"]
    }
}
