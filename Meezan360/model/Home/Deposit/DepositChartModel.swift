//
//  DepositChartModel.swift
//  Meezan360
//
//  Created by Akber Sayani on 27/10/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit
import ObjectMapper

class DepositChartModel: NSObject, Mappable {
    var mainTitle: String?
    var title: String?
    var val: String?
    var color: String?

    override init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        mainTitle <- map["maintitle"]
        title <- map["title"]
        val <- map["val"]
        color <- map["color"]
    }
}
