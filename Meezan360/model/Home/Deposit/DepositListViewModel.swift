//
//  ETBNTBMovement.swift
//  Meezan360
//
//  Created by Akber Sayani on 27/10/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit
import ObjectMapper

class DepositListViewModel: NSObject, Mappable {
    var level: String?
    var key: String?
    var mainTitle: String?
    var title: String?
    var ca: String?
    var sa: String?
    var td: String?
    var total: String?

    override init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        level <- map["level"]
        key <- map["key"]
        mainTitle <- map["maintitle"]
        title <- map["title"]
        ca <- map["ca"]
        ca <- map["val1"]
        sa <- map["sa"]
        sa <- map["val2"]
        td <- map["td"]
        td <- map["val3"]
        total <- map["total"]
    }
}
