//
//  DepositModel.swift
//  Meezan360
//
//  Created by Akber Sayani on 27/10/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit
import ObjectMapper

class DepositModel: NSObject, Mappable {
    var headValues: [HeadValueModel]?
    var branches: [DepositListViewModel]?
    var etbntbMovement: [DepositListViewModel]?
    var budget: [DepositListViewModel]?
    var topCustomer: [DepositListViewModel]?
    var casamix: [DepositChartModel]?
    var tierwisepie: [DepositChartModel]?
    var summary: DashboardSummaryModel?
    
    override init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        headValues <- map["head"]
        summary <- map["summary"]
        branches <- map["OnOffBranches"] //OnOffBranches
        etbntbMovement <- map["ETBNTBMovement"] // ETBNTBMovement
        budget <- map["Budget"] //Budget
        topCustomer <- map["Top150Customer"] // Top150Customer
        casamix <- map["casamix"] //casamix
        tierwisepie <- map["tierwisepie"] //tierwisepie
    }
}
