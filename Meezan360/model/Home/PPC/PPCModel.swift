//
//  PPCModel.swift
//  Meezan360
//
//  Created by Akber Sayani on 06/11/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit
import ObjectMapper

class PPCModel: NSObject, Mappable {
    var headValues: [HeadValueModel]?
    var pie: [PPCPieDetailsModel]?
    var footerValues: [HeadValueModel]?
    
    override init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        headValues <- map["head"]
        footerValues <- map["footer"]
        pie <- map["pie"]
    }
}

