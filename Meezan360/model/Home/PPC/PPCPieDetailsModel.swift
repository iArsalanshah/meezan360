//
//  PPCPieDetailsModel.swift
//  Hunters
//
//  Created by Akber Sayani on 15/12/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit
import ObjectMapper

class PPCPieDetailsModel: NSObject, Mappable {
    var title: String?
    var val: String?
    var color: String?
    var key1: String?
    var val1: String?
    var key2: String?
    var val2: String?
    var key3: String?
    var val3: String?
    var key4: String?
    var val4: String?

    override init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        title <- map["title"]
        val <- map["val"]
        color <- map["color"]
        key1 <- map["key1"]
        val1 <- map["val1"]
        key2 <- map["key2"]
        val2 <- map["val2"]
        key3 <- map["key3"]
        val3 <- map["val3"]
        key4 <- map["key4"]
        val4 <- map["val4"]
    }
}
