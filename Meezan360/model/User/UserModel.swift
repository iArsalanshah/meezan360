//
//  UserModel.swift
//  Meezan360
//
//  Created by Akber Sayani on 19/10/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit
import ObjectMapper

class UserModel: NSObject, Mappable, NSCoding {
    var id: String?
    var firstName: String?
    var type: String?
    var status: String?
    var emailAddress: String?
    var designation: String?
    
    override init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    convenience required init?(coder decoder: NSCoder) {
        self.init()
        
        self.id = decoder.decodeObject(forKey: "user_id") as? String
        self.firstName = decoder.decodeObject(forKey: "first_name") as? String
        self.type = decoder.decodeObject(forKey: "user_type") as? String
        self.status = decoder.decodeObject(forKey: "status") as? String
        self.emailAddress = decoder.decodeObject(forKey: "email_address") as? String
        self.designation = decoder.decodeObject(forKey: "designation") as? String
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(self.id, forKey: "user_id")
        coder.encode(self.firstName, forKey: "first_name")
        coder.encode(self.type, forKey: "user_type")
        coder.encode(self.status, forKey: "phone")
        coder.encode(self.emailAddress, forKey: "email_address")
        coder.encode(self.designation, forKey: "designation")
    }
    
    func mapping(map: Map) {
        id <- map["user_id"]
        firstName <- map["first_name"]
        type <- map["user_type"]
        status <- map["status"]
        emailAddress <- map["email_address"]
        designation <- map["designation"]
    }
}
