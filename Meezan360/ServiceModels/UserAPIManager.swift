//
//  UserAPIManager.swift
//  The Court Lawyer
//
//  Created by Ahmed Shahid on 5/3/18.
//  Copyright © 2018 Ahmed Shahid. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

class UsersAPIManager: APIManagerBase {
    
    func loginUser(params: Parameters, success: @escaping DefaultAPIAnyObject, failure: @escaping DefaultAPIFailureClosure) {
        let encodedParams: [String: Any] = [
            "payload": params.dict2json()?.base64Encoded() ?? ""
        ]
        if let route = GETURLfor(route: WebRoute.loginRoute.rawValue, params: encodedParams) {
            self.postRequestWith(route: route, parameters: params, success: { (response) in                
                // Save user auth token
                if let header = response["header"] {
                    if let token = header["token"] as? String {
                        AppStateManager.shared.saveAuthToken(token)
                    }
                }

                if let result = response["body"] {
                    guard let payload = result["payload"] as? String else {
                        let errorMessage = "Something went wrong. Please try again"
                        let userInfo = [NSLocalizedFailureReasonErrorKey: errorMessage]
                        failure(NSError(domain: "Error", code: 0, userInfo: userInfo))
                        return
                    }
                    
                    guard let json = payload.base64Decoded() else {
                        let errorMessage = "Something went wrong. Please try again"
                        let userInfo = [NSLocalizedFailureReasonErrorKey: errorMessage]
                        failure(NSError(domain: "Error", code: 0, userInfo: userInfo))
                        return
                    }
                    
                    guard let data = json.toDictionary(text: json) else {
                        let errorMessage = "Something went wrong. Please try again"
                        let userInfo = [NSLocalizedFailureReasonErrorKey: errorMessage]
                        failure(NSError(domain: "Error", code: 0, userInfo: userInfo))
                        return
                    }
                    
                    success(data as AnyObject)
                    
                } else {
                    // Failure message
                    let errorMessage = "Something went wrong. Please try again"
                    let userInfo = [NSLocalizedFailureReasonErrorKey: errorMessage]
                    failure(NSError(domain: "Error", code: 0, userInfo: userInfo))
                }
            }, failure: failure, withHeaders: true)
        }
    }
    
    func logoutUser(params: Parameters, success: @escaping DefaultBoolResultAPISuccesClosure, failure: @escaping DefaultAPIFailureClosure) {
        if let route = GETURLfor(route: WebRoute.loginRoute.rawValue, params: params) {
            self.postRequestWith(route: route, parameters: params, success: { (response) in
                success(true)
            }, failure: failure, withHeaders: true)
        }
    }
    
    func postArrayRequestWith(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        var _params = params
        _params["token"] = AppStateManager.shared.getAuthToken() ?? "0"
        if let route = GETURLfor(route: WebRoute.otherRoute.rawValue, params: _params) {
            self.postRequestWith(route: route, parameters: params, success: { (response) in
                if let header = response["header"] {
                    if let token = header["token"] as? String {
                        AppStateManager.shared.saveAuthToken(token)
                    }
                }

                if let result = response["body"] as? Array<AnyObject> {
                    success(result)
                } else {
                    // Failure message
                    let errorMessage = "Something went wrong. Please try again"
                    let userInfo = [NSLocalizedFailureReasonErrorKey: errorMessage]
                    failure(NSError(domain: "Error", code: 0, userInfo: userInfo))
                }
                
            }, failure: failure, withHeaders: true)
        }
    }
    
    func postDictionaryRequestWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        var _params = params
        _params["token"] = AppStateManager.shared.getAuthToken() ?? "0"
        if let route = GETURLfor(route: WebRoute.otherRoute.rawValue, params: _params) {
            self.postRequestWith(route: route, parameters: params, success: { (response) in
                if let header = response["header"] {
                    if let token = header["token"] as? String {
                        AppStateManager.shared.saveAuthToken(token)
                    }
                }

                if let result = response["body"] as? Dictionary<String, AnyObject> {
                    success(result)
                } else {
                    // Failure message
                    let errorMessage = "Something went wrong. Please try again"
                    let userInfo = [NSLocalizedFailureReasonErrorKey: errorMessage]
                    failure(NSError(domain: "Error", code: 0, userInfo: userInfo))
                }
            }, failure: failure, withHeaders: true)
        }
    }
    
    func postBooleanRequestWith(params: Parameters, success: @escaping DefaultBoolResultAPISuccesClosure, failure: @escaping DefaultAPIFailureClosure) {
        var _params = params
        _params["token"] = AppStateManager.shared.getAuthToken() ?? "0"
        if let route = GETURLfor(route: WebRoute.loginRoute.rawValue, params: _params) {
            self.postRequestWith(route: route, parameters: params, success: { (response) in
                if let header = response["header"] {
                    if let token = header["token"] as? String {
                        AppStateManager.shared.saveAuthToken(token)
                    }
                }
                success(true)
            }, failure: failure, withHeaders: true)
        }
    }
}
