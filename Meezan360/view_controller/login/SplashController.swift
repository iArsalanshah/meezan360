//
//  SplashController.swift
//  Meezan360
//
//  Created by Akber Sayani on 19/10/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class SplashController: BaseController {
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //Constants.APP_DELEGATE.changeRootController()
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
            self.getConfiguration()
            //self.moveToNext()
        }
    }
    
    private func moveToNext() {
        if AppStateManager.shared.isUserLoggedIn() {
            Constants.APP_DELEGATE.changeRootController()
        } else {
            self.showLoginVC()
        }
    }
    
    private func showLoginVC() {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "LoginController") as! LoginController
        self.heroReplaceViewController(with: controller)
    }
    
    private func getConfiguration() {
        ActivityIndicatorManager.showLoader()
        let params: [String: Any] = [
            "action": WebRoute.configAction.rawValue,
            "device_id": UIDevice.current.identifierForVendor?.uuidString ?? "",
        ]
        APIManager.sharedInstance.user.postDictionaryRequestWith(params: params, success: { (response) in
            ActivityIndicatorManager.hideLoader()
            if let status = response["status"] as? String, status == "live" {
                AppStateManager.shared.isLive = true
            }
            self.moveToNext()
        }) { (error) in
            ActivityIndicatorManager.hideLoader()
            self.moveToNext()
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
