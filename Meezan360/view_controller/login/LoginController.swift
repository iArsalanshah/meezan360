//
//  LoginController.swift
//  Meezan360
//
//  Created by Akber Sayani on 19/10/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit
import Peep
import Haptica
import Hero
import SwiftValidator
import AnimatedTextInput

class LoginController: BaseController {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var emailField: AnimatedTextInput!
    @IBOutlet weak var passwordField: AnimatedTextInput!
    
    var timer = Timer()
    var dataSource = ["walkthrough_01", "walkthrough_02", "walkthrough_03", "walkthrough_04"]
    
    let validator = Validator()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setupFields()
        self.setupPageControls()
        self.addInteractionDismissGesture(isHorizontal: true)
        self.hero.modalAnimationType = .selectBy(presenting: .push(direction: .left), dismissing: .pull(direction: .right))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        startTimer()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        stopTimer()
    }
    
    func setupFields() {
        self.emailField.placeHolderText = "Username"
        self.emailField.type = .email
        self.emailField.style = AppTextInputStyle()
        self.emailField.returnKeyType = .next
        self.validator.registerField(self.emailField, rules: [RequiredRule(message: "Email is required")])
        self.emailField.delegate = self
        
        self.passwordField.placeHolderText = "Password"
        self.passwordField.type = .password(toggleable: true)
        self.passwordField.returnKeyType = .done
        self.passwordField.style = AppTextInputStyle()
        self.validator.registerField(self.passwordField, rules: [RequiredRule(message: "Password is required")])
        self.passwordField.delegate = self
    }
    
    func setupPageControls() {
        self.pageControl.numberOfPages = self.dataSource.count
        self.pageControl.currentPage = 0
    }
    
    func startTimer() {
        self.timer = Timer.scheduledTimer(withTimeInterval: 3.0, repeats: true, block: { (timer) in
            let visibleRect = CGRect(origin: self.collectionView.contentOffset, size: self.collectionView.bounds.size)
            let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
            if let indexPath = self.collectionView.indexPathForItem(at: visiblePoint) {
                let index = (indexPath.row + 1) % self.dataSource.count
                self.collectionView.scrollToItem(at: IndexPath(row: index, section: indexPath.section), at: .right, animated: true)
                self.pageControl.currentPage = index
            }
        })
    }
    
    func stopTimer() {
        self.timer.invalidate()
    }

    // MARK: - IBAction Methods
    
    @IBAction func forgotPasswordButtonTapped(_ sender: UIButton) {
        Peep.play(sound: KeyPress.tap)
        Haptic.impact(.light).generate()
        self.performSegue(withIdentifier: "forgotPassword", sender: self)
    }
    
    @IBAction func loginButtonTapped(_ sender: UIButton) {
        Peep.play(sound: KeyPress.tap)
        self.validator.validate(self)
    }
    
    // MARK: - Web Service Methods
    
    private func loginAction() {
        ActivityIndicatorManager.showLoader()
        let params: [String: Any] = [
            "action": "login",
            "username": self.emailField.text ?? "",
            "password": self.passwordField.text?.md5 ?? ""
        ]
        APIManager.sharedInstance.user.loginUser(params: params, success: { (response) in
            ActivityIndicatorManager.hideLoader()
            if let data = response["users"] as? [String: Any] {
                if let user = UserModel(JSON: data) {
                    AppStateManager.shared.saveUserDetails(user)
                    Constants.APP_DELEGATE.changeRootController()
                } else {
                    AlertManager.showErrorWith(message: "User object not found")
                }
            } else {
                AlertManager.showErrorWith(message: "User object not found")
            }
        }) { (error) in
            ActivityIndicatorManager.hideLoader()
            AlertManager.showErrorWith(message: error.localizedDescription)
        }
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        segue.destination.modalPresentationStyle = .fullScreen
    }
}

extension LoginController : ValidationDelegate {
    func validationSuccessful() {
        self.view.endEditing(true)
        Haptic.impact(.light).generate()
        self.loginAction()
    }
    
    func validationFailed(_ errors: [(Validatable, ValidationError)]) {
        Peep.play(sound: KeyPress.tap)
        for (_, error) in validator.errors {
            (error.field as? AnimatedTextInput)?.show(error: error.errorMessage)
            (error.field as? AnimatedTextInput)?.shake()
        }
    }
}

extension LoginController : AnimatedTextInputDelegate {
    func animatedTextInputShouldReturn(animatedTextInput: AnimatedTextInput) -> Bool {
        if animatedTextInput == self.emailField {
            self.validator.validateField(self.emailField) { (error) in
                if (error != nil) {
                    self.emailField.show(error: error?.errorMessage ?? "", placeholderText: "Email")
                    Peep.play(sound: KeyPress.tap)
                    self.emailField.shake()
                }else {
                    self.emailField.clearError()
                    self.passwordField.becomeFirstResponder()
                }
            }
        }else if animatedTextInput == self.passwordField {
            self.validator.validateField(self.passwordField) { (error) in
                self.passwordField.resignFirstResponder()
                if (error != nil) {
                    self.passwordField.show(error: error?.errorMessage ?? "", placeholderText: "Password")
                    Peep.play(sound: KeyPress.tap)
                    self.passwordField.shake()
                }else {
                    self.passwordField.clearError()
                    self.validator.validate(self)
                }
            }
        }
        return false
    }
}

extension LoginController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WalkthroughImageCellIdentifier", for: indexPath) as! WalkthroughImageCVC
        cell.imageView.image = UIImage(named: self.dataSource[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = collectionView.frame.size
        return CGSize(width: size.width, height: size.height)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let visibleRect = CGRect(origin: collectionView.contentOffset, size: collectionView.bounds.size)
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        if let indexPath = collectionView.indexPathForItem(at: visiblePoint) {
            self.pageControl.currentPage = indexPath.row
        }
    }
}
