//
//  ForgotPasswordController.swift
//  Meezan360
//
//  Created by Akber Sayani on 19/10/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit
import SwiftValidator
import Peep
import Haptica
import AnimatedTextInput

class ForgotPasswordController: BaseController {
    @IBOutlet weak var emailField: AnimatedTextInput!

    let validator = Validator()

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setupFields()
        self.addInteractionDismissGesture(isHorizontal: true)
        self.hero.modalAnimationType = .selectBy(presenting: .push(direction: .left), dismissing: .pull(direction: .right))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.emailField.becomeFirstResponder()
    }
    
    // MARK: - Setup Methods
    
    func setupFields() {
        self.emailField.placeHolderText = "Username"
        self.emailField.text = ""
        self.emailField.type = .email
        self.emailField.style = AppTextInputStyle()
        self.emailField.returnKeyType = .done
        self.validator.registerField(self.emailField, rules: [RequiredRule(message: "Email is required"), EmailRule(message: "Invalid email")])
        self.emailField.delegate = self
    }
    
    // MARK: - IBActions
    
    @IBAction func submitButtonTapped(_ sender: UIButton) {
        Peep.play(sound: KeyPress.tap)
        self.validator.validate(self)
    }
    
    // MARK: - Web Service Methods
    
    private func forgotPasswordAction() {
        AlertManager.showSuccessMessage(message: "Reset password email is sent to your email address", forceOnWindow:true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ForgotPasswordController : ValidationDelegate {
    func validationSuccessful() {
        Haptic.impact(.light).generate()
        self.view.endEditing(true)
        self.emailField.clearError()
        self.forgotPasswordAction()
    }
    
    func validationFailed(_ errors: [(Validatable, ValidationError)]) {
        Peep.play(sound: KeyPress.tap)
        for (_, error) in validator.errors {
            (error.field as? AnimatedTextInput)?.show(error: error.errorMessage)
            (error.field as? AnimatedTextInput)?.shake()
        }
    }
}

extension ForgotPasswordController : AnimatedTextInputDelegate {
    func animatedTextInputShouldReturn(animatedTextInput: AnimatedTextInput) -> Bool {
        if animatedTextInput == self.emailField {
            self.validator.validate(self)
        }
        return false
    }
}
