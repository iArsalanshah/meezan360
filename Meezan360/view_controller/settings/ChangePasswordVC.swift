//
//  ChangePasswordVC.swift
//  Hunters
//
//  Created by Akber Sayani on 11/11/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit
import Peep
import Haptica
import SwiftValidator
import AnimatedTextInput

class ChangePasswordVC: BaseController {
    @IBOutlet weak var oldPasswordField: AnimatedTextInput!
    @IBOutlet weak var newPasswordField: AnimatedTextInput!
    @IBOutlet weak var confirmPasswordField: AnimatedTextInput!

    let validator = Validator()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setupFields()
        self.addInteractionDismissGesture(isHorizontal: true)
        self.hero.modalAnimationType = .selectBy(presenting: .push(direction: .left), dismissing: .pull(direction: .right))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.oldPasswordField.becomeFirstResponder()
    }
    
    func setupFields() {
        self.oldPasswordField.placeHolderText = "Old Password"
        self.oldPasswordField.type = .password(toggleable: true)
        self.oldPasswordField.returnKeyType = .next
        self.oldPasswordField.style = LightTextInputStyle()
        self.validator.registerField(self.oldPasswordField, rules: [RequiredRule(message: "Field is required")])
        self.oldPasswordField.delegate = self
        
        self.newPasswordField.placeHolderText = "New Password"
        self.newPasswordField.type = .password(toggleable: true)
        self.newPasswordField.returnKeyType = .next
        self.newPasswordField.style = LightTextInputStyle()
        self.validator.registerField(self.newPasswordField, rules: [RequiredRule(message: "Field is required"), PasswordRule(regex: "^(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[a-z]).{8,}$", message: "Invalid password")])
        self.newPasswordField.delegate = self

        self.confirmPasswordField.placeHolderText = "Confirm Password"
        self.confirmPasswordField.type = .password(toggleable: true)
        self.confirmPasswordField.returnKeyType = .done
        self.confirmPasswordField.style = LightTextInputStyle()
        self.validator.registerField(self.confirmPasswordField, rules: [RequiredRule(message: "Field is required"), PasswordRule(regex: "^(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[a-z]).{8,}$", message: "Invalid password")])
        self.confirmPasswordField.delegate = self
    }
    
    @IBAction func submitButtonTapped(_ sender: UIButton) {
        Peep.play(sound: KeyPress.tap)
        self.validator.validate(self)
    }
    
    // MARK: - Web Service Methods
    
    private func changePassword() {
        ActivityIndicatorManager.showLoader()
        let params: [String: Any] = [
            "action": WebRoute.changePassword.rawValue,
            "user_id": AppStateManager.shared.getUserId() ?? "",
            "new_password": self.newPasswordField.text?.md5 ?? "",
            "old_password": self.oldPasswordField.text?.md5 ?? ""
        ]
        APIManager.sharedInstance.user.postBooleanRequestWith(params: params, success: { (response) in
            ActivityIndicatorManager.hideLoader()
            AlertManager.showSuccessMessage(message: "Password has been reset")
            self.forceLogout()
        }) { (error) in
            ActivityIndicatorManager.hideLoader()
            if error.code == -1000 {
                self.showInvalidTokenAlert()
            } else if error.code == -2000 {
                self.showChangePasswordAlert()
            }  else {
                AlertManager.showErrorWith(message: error.localizedDescription)
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}

extension ChangePasswordVC : ValidationDelegate {
    func validationSuccessful() {
        self.view.endEditing(true)
        Haptic.impact(.light).generate()
        if self.newPasswordField.text == self.confirmPasswordField.text {
            self.changePassword()
        } else {
            AlertManager.showAlertWith(message: "Passwords do not match")
        }
    }
    
    func validationFailed(_ errors: [(Validatable, ValidationError)]) {
        Peep.play(sound: KeyPress.tap)
        for (_, error) in validator.errors {
            (error.field as? AnimatedTextInput)?.show(error: error.errorMessage)
            (error.field as? AnimatedTextInput)?.shake()
            if error.errorMessage == "Invalid password" {
                AlertManager.showAlertWith(message: "Must be 8 characters with 1 uppercase, 1 lowercase and 1 number.")
            }
        }
    }
}

extension ChangePasswordVC : AnimatedTextInputDelegate {
    func animatedTextInputShouldReturn(animatedTextInput: AnimatedTextInput) -> Bool {
        if animatedTextInput == self.oldPasswordField {
            self.validator.validateField(self.oldPasswordField) { (error) in
                if (error != nil) {
                    self.oldPasswordField.show(error: error?.errorMessage ?? "", placeholderText: "Old Password")
                    Peep.play(sound: KeyPress.tap)
                    self.oldPasswordField.shake()
                }else {
                    self.oldPasswordField.clearError()
                    self.newPasswordField.becomeFirstResponder()
                }
            }
        }else if animatedTextInput == self.newPasswordField {
            self.validator.validateField(self.newPasswordField) { (error) in
                self.newPasswordField.resignFirstResponder()
                if (error != nil) {
                    self.newPasswordField.show(error: error?.errorMessage ?? "", placeholderText: "New Password")
                    Peep.play(sound: KeyPress.tap)
                    self.newPasswordField.shake()
                }else {
                    self.newPasswordField.clearError()
                    self.confirmPasswordField.becomeFirstResponder()
                }
            }
        } else if animatedTextInput == self.confirmPasswordField {
            self.validator.validateField(self.confirmPasswordField) { (error) in
                self.confirmPasswordField.resignFirstResponder()
                if (error != nil) {
                    self.confirmPasswordField.show(error: error?.errorMessage ?? "", placeholderText: "Confirm Password")
                    Peep.play(sound: KeyPress.tap)
                    self.confirmPasswordField.shake()
                }else {
                    self.confirmPasswordField.clearError()
                    self.validator.validate(self)
                }
            }
        }
        return false
    }
}
