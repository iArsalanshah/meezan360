//
//  AboutAppVC.swift
//  Hunters
//
//  Created by Akber Sayani on 11/11/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit

class AboutAppVC: BaseController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.addInteractionDismissGesture(isHorizontal: true)
        self.hero.modalAnimationType = .selectBy(presenting: .push(direction: .left), dismissing: .pull(direction: .right))
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
