//
//  SettingsController.swift
//  ChartViewTestProject
//
//  Created by Amr Abd Elwahab on 02/10/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit
import Peep
import Haptica

class SettingsController: BaseController {
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var designationLabel: UILabel!
    @IBOutlet weak var enableSoundSwitch: UISwitch!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.populateData()
    }
    
    private func populateData() {
        if let userDetails = AppStateManager.shared.getUserDetails() {
            self.firstNameLabel.text = userDetails.firstName
            self.designationLabel.text = userDetails.designation
        }
        
        self.enableSoundSwitch.isOn = AppStateManager.shared.getUserPreferenceSoundEnable()
    }
    
    @IBAction func logoutButtonTapped(_ sender: UIButton) {
        Peep.play(sound: KeyPress.tap)
        Haptic.impact(.light).generate()
        self.forceLogout()
    }
    
    @IBAction func changePasswordButtonTapped(_ sender: UIButton) {
        Peep.play(sound: KeyPress.tap)
        Haptic.impact(.light).generate()
        self.performSegue(withIdentifier: "showChangePassword", sender: self)
    }

    @IBAction func aboutAppButtonTapped(_ sender: UIButton) {
        Peep.play(sound: KeyPress.tap)
        Haptic.impact(.light).generate()
        self.performSegue(withIdentifier: "showAboutApp", sender: self)
    }
    
    @IBAction func enableSoundSwitchValueChanged(_ sender: Any) {
        AppStateManager.shared.saveUserPreferenceSoundEnable(self.enableSoundSwitch.isOn)
    }
}
