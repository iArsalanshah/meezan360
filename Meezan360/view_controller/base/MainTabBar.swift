//
//  BaseTabBar.swift
//  ChartViewTestProject
//
//  Created by Amr Abd Elwahab on 02/10/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit
import RAMAnimatedTabBarController

class BaseTabBar: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        addShadow()
    }
    
    private func addShadow() {
        self.tabBar.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.tabBar.layer.shadowRadius = 3;
        self.tabBar.layer.shadowColor = UIColor.gray.cgColor
        self.tabBar.layer.shadowOpacity = 0.5;
    }
}
