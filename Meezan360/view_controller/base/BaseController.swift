//
//  BaseController.swift
//  ChartViewTestProject
//
//  Created by Amr Abd Elwahab on 03/10/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit
import Hero
import Peep
import Haptica
import IQKeyboardManagerSwift
import AVFoundation

class BaseController: UIViewController {
    
    var panGR: UIScreenEdgePanGestureRecognizer!
    
    let synth = AVSpeechSynthesizer()
    var speechUtterance = AVSpeechUtterance(string: "")

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if let navController = self.navigationController, navController.viewControllers.count > 1 {
            self.addBackButton()
        }
    }
    
    private func addBackButton() {
        let backImage = UIImage(named: "arrowLeft")
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 30.0, height: 30.0))
        button.setImage(backImage, for: .normal)
        button.addTarget(self, action: #selector(self.backButtonTapped(_:)), for: .touchUpInside)
        let item = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = item
    }

    func addInteractionDismissGesture(isHorizontal : Bool = false){
        if isHorizontal {
            panGR = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(handlePanHorizontal(gestureRecognizer:)))
            view.addGestureRecognizer(panGR)
        }else{
            panGR = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(handlePan(gestureRecognizer:)))
            view.addGestureRecognizer(panGR)
        }
    }
    
    public func playVoiceText(_ text: String) {
        guard AppStateManager.shared.getUserPreferenceSoundEnable() else {
            return
        }
        
        speechUtterance = AVSpeechUtterance(string: text)
        speechUtterance.rate = 0.5
        synth.speak(speechUtterance)
    }
    
    public func addSearchButton() {
        let searchImage = UIImage(named: "search-icon")
        let searchButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30.0, height: 30.0))
        searchButton.setImage(searchImage, for: .normal)
        searchButton.addTarget(self, action: #selector(self.searchButtonTapped(_:)), for: .touchUpInside)
        let searchItem = UIBarButtonItem(customView: searchButton)

        let phoneImage = UIImage(named: "phone-icon")
        let phoneButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30.0, height: 30.0))
        phoneButton.setImage(phoneImage, for: .normal)
        phoneButton.tintColor = UIColor(hexString: "#92959C")
        phoneButton.addTarget(self, action: #selector(self.phoneButtonTapped(_:)), for: .touchUpInside)
        let phoneItem = UIBarButtonItem(customView: phoneButton)
        
        self.navigationItem.rightBarButtonItems = [searchItem, phoneItem]
    }
    
    public func showInvalidTokenAlert() {
        let alertController = UIAlertController(title: "Message", message: "Due to security reasons you have been forced logout, please login again", preferredStyle: .alert)
        
        let actionOk = UIAlertAction(title: "Ok", style: .default) { (action) in
            self.forceLogout()
        }
        
        alertController.addAction(actionOk)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    public func showChangePasswordAlert() {
        if let controller = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "ChangePasswordVC") as? ChangePasswordVC {
            controller.modalTransitionStyle = .crossDissolve
            self.tabBarController?.present(controller, animated: false, completion: nil)
        }
    }

    // MARK: IBAction Methods
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        Peep.play(sound: KeyPress.tap)
        Haptic.impact(.light).generate()
        back()
    }
    
    @IBAction func searchButtonTapped(_ sender: UIButton) {
        Peep.play(sound: KeyPress.tap)
        Haptic.impact(.light).generate()
        if let controller = AppStoryboard.Search.instance.instantiateViewController(withIdentifier: "SearchFiltersController") as? SearchFiltersController {
            controller.delegate = self
            controller.modalPresentationStyle = .overCurrentContext
            controller.modalTransitionStyle = .crossDissolve
            self.tabBarController?.present(controller, animated: false, completion: nil)
        }
    }
    
    @IBAction func phoneButtonTapped(_ sender: UIButton) {
        Peep.play(sound: KeyPress.tap)
        Haptic.impact(.light).generate()
        if let controller = AppStoryboard.Search.instance.instantiateViewController(withIdentifier: "PhoneContactsViewController") as? PhoneContactsViewController {
            controller.modalPresentationStyle = .overCurrentContext
            controller.modalTransitionStyle = .crossDissolve
            self.tabBarController?.present(controller, animated: false, completion: nil)
        }
    }
    
    func back() {
        DispatchQueue.main.async{
            IQKeyboardManager.shared.resignFirstResponder()
            if let nav = self.navigationController {
                if nav.viewControllers.count == 1{
                    nav.dismiss(animated: true, completion: nil)
                }else{
                    nav.popViewController(animated: true)
                }
            }else{
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    @objc func searchFilterApplied(_ notification: NSNotification) {
        //self.getDashboardDetails()
        print("Search filter applied: \(self.className)")
    }
    
    @objc func handlePanHorizontal(gestureRecognizer:UIPanGestureRecognizer) {
        DispatchQueue.main.async{
            let translation = gestureRecognizer.translation(in: nil)
            let progress = translation.x / self.view.bounds.width
            
            switch gestureRecognizer.state {
            case .began:
                // begin the transition as normal
                if let nav = self.navigationController {
                    if nav.viewControllers.count == 1{
                        nav.dismiss(animated: true, completion: nil)
                    }else{
                        nav.popViewController(animated: true)
                    }
                }else{
                    self.dismiss(animated: true, completion: nil)
                }
            case .changed:
                Hero.shared.update(progress)
            default:
                if progress > 0.3 {
                    Hero.shared.finish()
                } else {
                    Hero.shared.cancel()
                }
            }
        }
    }
    
    @objc func handlePan(gestureRecognizer:UIPanGestureRecognizer) {
        DispatchQueue.main.async{
            let translation = gestureRecognizer.translation(in: nil)
            let progress = translation.y / self.view.bounds.height
            
            switch gestureRecognizer.state {
            case .began:
                // begin the transition as normal
                if let nav = self.navigationController {
                    if nav.viewControllers.count == 1{
                        nav.dismiss(animated: true, completion: nil)
                    }else{
                        nav.popViewController(animated: true)
                    }
                }else{
                    self.dismiss(animated: true, completion: nil)
                }
            case .changed:
                Hero.shared.update(progress)
            default:
                if progress > 0.3 {
                    Hero.shared.finish()
                } else {
                    Hero.shared.cancel()
                }
            }
        }
    }
    
    // MARK: Web Service Methods
    
    public func forceLogout() {
        ActivityIndicatorManager.showLoader()
        let params: [String: Any] = [
            "action": WebRoute.logout.rawValue,
            "user_id": AppStateManager.shared.getUserId() ?? ""
        ]
        APIManager.sharedInstance.user.logoutUser(params: params, success: { (response) in
            ActivityIndicatorManager.hideLoader()
            AppStateManager.shared.removeUserDetails()
            AppStateManager.shared.removeSearchFilterDetails()
            Constants.APP_DELEGATE.changeRootController()
        }) { (error) in
            ActivityIndicatorManager.hideLoader()
            AlertManager.showErrorWith(message: error.localizedDescription)
        }
    }
}

extension BaseController: SearchFiltersDelegate {
    @objc func didApplySearchFilters() {
        print("Search filter applied")
    }
}
