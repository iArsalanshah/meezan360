//
//  PhoneContactsViewController.swift
//  Hunters
//
//  Created by Akber Sayani on 14/12/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit
import ObjectMapper

class PhoneContactsViewController: BaseController {
    class func instantiateFromStoryboard() -> PhoneContactsViewController {
        return AppStoryboard.Search.instance.instantiateViewController(withIdentifier: String(describing: self)) as! PhoneContactsViewController
    }

    @IBOutlet weak var tableView: UITableView! {
        didSet {
            self.tableView.tableFooterView = UIView()
        }
    }
    
    var dataSource = Array<PhoneContactModel>()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.getPhoneContacts()
    }
    
    private func showPhoneDialer(_ phone: String) {
        if let phoneURL = URL(string: "tel://\(phone)") {
            if UIApplication.shared.canOpenURL(phoneURL) {
                UIApplication.shared.open(phoneURL, options: [:], completionHandler: nil)
            } else {
                AlertManager.showErrorWith(message: "Call option not available")
            }
        }
    }

    
    @IBAction func backgroundViewTapped(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Web Service Methods
    
    private func getPhoneContacts() {
        ActivityIndicatorManager.showLoader()
        let params = [
            "action": WebRoute.phoneContacts.rawValue,
            "userid": AppStateManager.shared.getUserId() ?? ""
        ]
        APIManager.sharedInstance.user.postArrayRequestWith(params: params, success: { (responseArray) in
            ActivityIndicatorManager.hideLoader()
            self.dataSource = Mapper<PhoneContactModel>().mapArray(JSONArray: responseArray as! [[String: Any]])
            self.tableView.reloadData()
        }) { (error) in
            ActivityIndicatorManager.hideLoader()
            if error.code == -1000 {
                self.showInvalidTokenAlert()
            } else if error.code == -2000 {
                self.showChangePasswordAlert()
            }  else {
                AlertManager.showErrorWith(message: error.localizedDescription)
            }
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension PhoneContactsViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PhoneContactCellIdentifier", for: indexPath) as! PhoneContactViewCell
        cell.populateData(self.dataSource[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        // Do nothing
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let phone = self.dataSource[indexPath.row].phoneNumber {
            self.showPhoneDialer(phone)
        }
    }
}
