//
//  TabBar2Controller.swift
//  ChartViewTestProject
//
//  Created by Amr Abd Elwahab on 02/10/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit
import ObjectMapper

class ScoreCardController: BaseController {
    @IBOutlet weak var mainCollectionView: UICollectionView!
    
    var navigationTitle: String?
    var actionName: String?
    
    private var dataSource: ScoreCardDetailsModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupNavigation()
        setupUI()
        
        self.getDetails()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    private func setupNavigation() {
        navigationItem.title = navigationTitle
        navigationController?.view.backgroundColor = UIColor.white
        navigationController?.navigationBar.prefersLargeTitles = true
        
        self.addSearchButton()
    }
    
    private func setupUI() {
        mainCollectionView.delegate = self
        mainCollectionView.dataSource = self
        
        // Register collection view nibs
        mainCollectionView.register(UINib(nibName: ScoreCardHalfPieCVC.storyboardId, bundle: nil), forCellWithReuseIdentifier: ScoreCardHalfPieCVC.storyboardId)
        mainCollectionView.register(UINib(nibName: PagerDetailsCVC.storyboardId, bundle: nil), forCellWithReuseIdentifier: PagerDetailsCVC.storyboardId)
    }
    
    override func didApplySearchFilters() {
        self.getDetails()
    }

    
    // MARK: - Web Service Methods

    private func getWebServiceAction() -> String? {
        if let title = self.navigationTitle {
            switch title {
            case "Deposit":
                return WebRoute.scoreCardDeposit.rawValue
            case "Financing":
                return WebRoute.scoreCardFinancing.rawValue
            case "Wealth":
                return WebRoute.scoreCardWealth.rawValue
            case "Cross Sell":
                return WebRoute.scoreCardCrossSell.rawValue
            case "Controls":
                return WebRoute.scoreCardControls.rawValue
            case "Profitability":
                return WebRoute.scoreCardProfitablity.rawValue
            case "Cash":
                return WebRoute.scoreCardCash.rawValue
            case "Premium":
                return WebRoute.scoreCardPremium.rawValue
            case "ADC":
                return WebRoute.scoreCardADC.rawValue
            case "Compliance":
                return WebRoute.scoreCardCompliance.rawValue
            default:
                return nil
            }
        }
        
        return nil
    }

    private func getDetails() {
        ActivityIndicatorManager.showLoader()
        let params = [
            "action": self.getWebServiceAction() ?? "",
            "userid": AppStateManager.shared.getUserId() ?? ""
        ]
        APIManager.sharedInstance.user.postDictionaryRequestWith(params: params, success: { (response) in
            ActivityIndicatorManager.hideLoader()
            if let model = ScoreCardDetailsModel(JSON: response) {
                self.dataSource = model
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    self.mainCollectionView.reloadData()
                }
            }
        }) { (error) in
            ActivityIndicatorManager.hideLoader()
            if error.code == -1000 {
                self.showInvalidTokenAlert()
            } else if error.code == -2000 {
                self.showChangePasswordAlert()
            }  else {
                AlertManager.showErrorWith(message: error.localizedDescription)
            }
        }
    }

}

private let tableDataSource = DummyData().sellDetailTableData()

extension ScoreCardController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (self.dataSource != nil) ? 2 : 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row == 0 {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ScoreCardHalfPieCVC.storyboardId, for: indexPath) as? ScoreCardHalfPieCVC else { return UICollectionViewCell() }
            if let data = self.dataSource?.head {
                cell.setupData(data)
            }
            return cell
        } else {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PagerDetailsCVC.storyboardId, for: indexPath) as? PagerDetailsCVC else { return UICollectionViewCell() }
            cell.backgroundColor = .clear
            if let values = self.dataSource?.values {
                cell.setupData(values)
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.section == 0 {
            let height: CGFloat = 8 + 21 + 8 + 175 + 15 + 8
            return CGSize(width: self.mainCollectionView.frame.width, height: height)
        } else {
            return CGSize(width: self.mainCollectionView.frame.width, height: CGFloat(self.getListViewItemHeight()))
        }
    }
    
    func getListViewItemHeight() -> Double {
        let header = 20.0
        let footer = 20.0
        var rows = 0.0
        if let values = self.dataSource?.values {
            rows = rows + (Double(values.count) * 25.0)
        }
        
        return header + footer + rows
    }
}
