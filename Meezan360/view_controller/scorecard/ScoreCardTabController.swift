//
//  ScoreCardTabController.swift
//  Meezan360
//
//  Created by Akber Sayani on 20/10/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit
import Peep
import Haptica
import ObjectMapper
import collection_view_layouts

enum LayoutType: Int {
    case instagram
    case flipboard
    case facebook
    case flickr
    
    private static let _count: LayoutType.RawValue = {
         // find the maximum enum value
         var maxValue: Int = 0
         while let _ = LayoutType(rawValue: maxValue) {
             maxValue += 1
         }
         return maxValue
     }()

     static func randomLayout() -> LayoutType {
         // pick and return a new value
         let rand = arc4random_uniform(UInt32(_count))
         return LayoutType(rawValue: Int(rand))!
     }
}

class ScoreCardTabController: BaseController {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var summaryCollectionView: UICollectionView!
    
    private var layout: BaseLayout!
    private var cellSizes = [[CGSize]]()
    private var dataSource = [ScoreModel]()
    private var arrSummary = [KeyValueModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        prepareCellSizes(withType: .flipboard)
        showLayout(withType: .flipboard)

        self.getScoreCardDetails()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true        
    }
    
    private func prepareCellSizes(withType type: LayoutType) {
        //let range = 50...150
        cellSizes.removeAll()

        self.dataSource.forEach { items in
            let sizes = items.name?.map { item -> CGSize in
                return CGSize(width: 0.1, height: 0.1)
            }
            
            if let sizes = sizes {
                cellSizes.append(sizes)
            }
        }
    }
    
    private func showLayout(withType type: LayoutType) {
        switch type {
        case .instagram:
            layout = InstagramLayout()
        case .flipboard:
            layout = FlipboardLayout()
        case .facebook:
            layout = FacebookLayout()
        case .flickr:
            layout = FlickrLayout()
        }

        layout.delegate = self

        // All layouts support this configs
        layout.contentPadding = ItemsPadding(horizontal: 15, vertical: 15)
        layout.cellsPadding = ItemsPadding(horizontal: 10, vertical: 10)

        collectionView.collectionViewLayout = layout
        collectionView.setContentOffset(CGPoint.zero, animated: false)
        collectionView.reloadData()
    }
    
    override func didApplySearchFilters() {
        self.getScoreCardDetails()
    }
    
    // MARK: - Web Service Methods
    
    private func getScoreCardDetails() {
        ActivityIndicatorManager.showLoader()
        let params: [String: Any] = [
            "action": WebRoute.scoreCardAction.rawValue,
            "userid": AppStateManager.shared.getUserId() ?? "0"
        ]
        APIManager.sharedInstance.user.postDictionaryRequestWith(params: params, success: { (response) in
            ActivityIndicatorManager.hideLoader()
            if let scorecard = response["scorecard"] as? [AnyObject] {
                self.dataSource = Mapper<ScoreModel>().mapArray(JSONArray: scorecard as! [[String: Any]])
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    self.collectionView.reloadData()
                }
            }
            
            if let summary = response["summary"] as? [AnyObject] {
                self.arrSummary = Mapper<KeyValueModel>().mapArray(JSONArray: summary as! [[String: Any]])
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    // Do something
                    self.summaryCollectionView.reloadData()
                }
            }
            
        }) { (error) in
            ActivityIndicatorManager.hideLoader()
            if error.code == -1000 {
                self.showInvalidTokenAlert()
            } else if error.code == -2000 {
                self.showChangePasswordAlert()
            }  else {
                AlertManager.showErrorWith(message: error.localizedDescription)
            }
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showScoreCardDetails" {
            if let item = sender as? ScoreModel {
                let controller = segue.destination as? ScoreCardController
                controller?.navigationTitle = item.name
            }
        }
    }
}

extension ScoreCardTabController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == summaryCollectionView {
            return Int(ceil(Double(self.arrSummary.count) / 3.0))
        } else {
            return self.dataSource.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == summaryCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ScorecardSummaryCVC", for: indexPath) as! ScorecardSummaryCVC
            cell.populateData(with: self.arrSummary, row: indexPath.row)
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ScoreCardTabCellIdentifier", for: indexPath) as! ScoreCardTabViewCell
            cell.populateData(with: self.dataSource[indexPath.row])
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let cell = cell as? ScoreCardTabViewCell {
            if let hexCode = self.dataSource[indexPath.row].colorHexCode {
                cell.cardView.backgroundColor = UIColor(hexString: hexCode).withAlphaComponent(0.7)
            } else {
                cell.cardView.backgroundColor = UIColor.random.withAlphaComponent(0.7)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == summaryCollectionView {
            return
        }
        
        Peep.play(sound: KeyPress.tap)
        Haptic.impact(.light).generate()
        self.performSegue(withIdentifier: "showScoreCardDetails", sender: self.dataSource[indexPath.row])
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == summaryCollectionView {
            return CGSize(width: 140, height: 140)
        } else {
            return CGSize.zero
        }
    }
}

// MARK: - LayoutDelegate

extension ScoreCardTabController: LayoutDelegate {
    func cellSize(indexPath: IndexPath) -> CGSize {
        return cellSizes[indexPath.section][indexPath.row]
    }

    func headerHeight(indexPath: IndexPath) -> CGFloat {
        return 0.0
    }

    func footerHeight(indexPath: IndexPath) -> CGFloat {
        return 0.0
    }
}
