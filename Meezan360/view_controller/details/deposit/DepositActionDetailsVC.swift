//
//  DepositActionDetailsVC.swift
//  Hunters
//
//  Created by Akber Sayani on 24/11/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit
import ObjectMapper

class DepositActionDetailsVC: BaseController {
    @IBOutlet weak var tableView: UITableView!

    private var dataSource: [PagerSubValueModel] = []
    
    var actionName: String?
    var navigationTitle: String?    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupNavigation()
        setupTableView()
        
        self.getDetails()
    }
    
    private func setupNavigation() {
        navigationItem.title = navigationTitle
        navigationController?.view.backgroundColor = UIColor.white
        navigationController?.navigationBar.prefersLargeTitles = true

        self.addSearchButton()
    }
    
    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(UINib(nibName: PagerSubtitleDetailsColumnTVC.storyboardId, bundle: nil), forCellReuseIdentifier: PagerSubtitleDetailsColumnTVC.storyboardId)
    }
    
    // MARK: - Helper Methods
    
    private func getWebServiceAction() -> String? {
        if let actionName = self.actionName {
            switch actionName {
            case "currentAccount":
                return WebRoute.currentAccountTracker.rawValue
            case "termDeposit":
                return WebRoute.termsDepositTracker.rawValue
            case "savingAccount":
                return WebRoute.savingAccountTracker.rawValue
            default:
                return nil
            }
        }
        
        return nil
    }
    
    // MARK: - Web Service Methods
    
    private func getDetails() {
        ActivityIndicatorManager.showLoader()
        let params = [
            "action": self.getWebServiceAction() ?? "",
            "userid": AppStateManager.shared.getUserId() ?? ""
        ]
        APIManager.sharedInstance.user.postArrayRequestWith(params: params, success: { (responseArray) in
            ActivityIndicatorManager.hideLoader()
            self.dataSource = Mapper<PagerSubValueModel>().mapArray(JSONArray: responseArray as! [[String: Any]])
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.tableView.reloadData()
            }
        }) { (error) in
            ActivityIndicatorManager.hideLoader()
            if error.code == -1000 {
                self.showInvalidTokenAlert()
            } else if error.code == -2000 {
                self.showChangePasswordAlert()
            }  else {
                AlertManager.showErrorWith(message: error.localizedDescription)
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension DepositActionDetailsVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSource.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 48.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: PagerSubtitleDetailsColumnTVC.storyboardId) as? PagerSubtitleDetailsColumnTVC else { return UITableViewCell() }
        cell.setupData(self.dataSource[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        if let cell = cell as? PagerSubtitleDetailsColumnTVC {
//            if indexPath.row == 0 {
//                let purpleColor = UIColor(named: "colorCustomPurple")
//                cell.title.textColor = purpleColor
//                cell.subTitle.textColor = purpleColor
//
//                cell.label1.textColor = purpleColor
//                cell.label2.textColor = purpleColor
//                cell.label3.textColor = purpleColor
//                cell.label4.textColor = purpleColor
//
//                cell.subLabel1.textColor = purpleColor
//                cell.subLabel2.textColor = purpleColor
//                cell.subLabel3.textColor = purpleColor
//                cell.subLabel4.textColor = purpleColor
//            } else {
//                let blackColor = UIColor.black
//                cell.title.textColor = blackColor
//                cell.subTitle.textColor = blackColor
//
//                cell.label1.textColor = blackColor
//                cell.label2.textColor = blackColor
//                cell.label3.textColor = blackColor
//                cell.label4.textColor = blackColor
//
//                cell.subLabel1.textColor = blackColor
//                cell.subLabel2.textColor = blackColor
//                cell.subLabel3.textColor = blackColor
//                cell.subLabel4.textColor = blackColor
//            }
//        }
    }
}

