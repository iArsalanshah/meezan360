//
//  HomePieChartDetailsVC.swift
//  ManagementProject
//
//  Created by Syed Arsalan Shah on 10/7/19.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit
import ObjectMapper

struct DataSourceModel {
    var name: String
    var value: Any?
}

class DepositMenuDetailsVC: BaseController {
    @IBOutlet weak var mainCollectionView: UICollectionView!
    @IBOutlet weak var regionNameLabel: UILabel!
    
    var dataSource: [DataSourceModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupNavigation()
        setupCollectionView()
        
        self.getDepositDetails()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    private func setupNavigation() {
        navigationItem.title = "Deposit"
        navigationController?.view.backgroundColor = UIColor.white
        navigationController?.navigationBar.prefersLargeTitles = true

        self.addSearchButton()
    }
    
    private func setupCollectionView() {
        mainCollectionView.register(UINib(nibName: TopBarCVC.storyboardId, bundle: nil), forCellWithReuseIdentifier: TopBarCVC.storyboardId)
        mainCollectionView.register(UINib(nibName: DetailMovementCVC.storyboardId, bundle: nil), forCellWithReuseIdentifier: DetailMovementCVC.storyboardId)
        mainCollectionView.register(UINib(nibName: CASAMixCVC.storyboardId, bundle: nil), forCellWithReuseIdentifier: CASAMixCVC.storyboardId)
        mainCollectionView.register(UINib(nibName: TeirWiseChartCVC.storyboardId, bundle: nil), forCellWithReuseIdentifier: TeirWiseChartCVC.storyboardId)
        mainCollectionView.register(UINib(nibName: DetailsButtonCVC.storyboardId, bundle: nil), forCellWithReuseIdentifier: DetailsButtonCVC.storyboardId)
        
        mainCollectionView.delegate = self
        mainCollectionView.dataSource = self
    }
    
    override func didApplySearchFilters() {
        self.getDepositDetails()
    }

    // MARK: - Web Service Methods
    
    private func getDepositDetails() {
        ActivityIndicatorManager.showLoader()
        let params = [
            "action": WebRoute.depositAction.rawValue,
            "userid": AppStateManager.shared.getUserId() ?? ""
        ]
        APIManager.sharedInstance.user.postArrayRequestWith(params: params, success: { (responseArray) in
            ActivityIndicatorManager.hideLoader()
            let array = Mapper<DepositModel>().mapArray(JSONArray: responseArray as! [[String: Any]])
            if let deposit = array.first {
                self.dataSource.removeAll()
                let mirrored_object = Mirror(reflecting: deposit)
                for (_, attr) in mirrored_object.children.enumerated() {
                    if let propertyName = attr.label, propertyName != "summary" {
                        self.dataSource.append(DataSourceModel(name: propertyName, value: attr.value))
                    }
                }
                // Add bottom view cell item in datasource
                self.dataSource.append(DataSourceModel(name: "bottomView", value: nil))
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    self.regionNameLabel.text = array.first?.summary?.regionName
                    if let voiceText = array.first?.summary?.voiceText {
                        self.playVoiceText(voiceText)
                    }
                    self.mainCollectionView.reloadData()
                }
            }
        }) { (error) in
            ActivityIndicatorManager.hideLoader()
            if error.code == -1000 {
                self.showInvalidTokenAlert()
            } else if error.code == -2000 {
                self.showChangePasswordAlert()
            }  else {
                AlertManager.showErrorWith(message: error.localizedDescription)
            }
        }
    }
    
    // MARK: - Navigation Methods
    
    private func showDetails(_ data: DataSourceModel) {
        let controller = PagerVC(nibName: PagerVC.storyboardId, bundle: nil)
        controller.actionName = data.name
        if let item = data.value as? [DepositListViewModel] {
            controller.navigationTitle = item.first?.mainTitle
        } else {
            controller.navigationTitle = data.name
        }
        
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    private func showDailyDepositPosition() {
        let controller = PagerVC(nibName: PagerVC.storyboardId, bundle: nil)
        controller.actionName = "dailyPosition"
        controller.navigationTitle = "Daily Deposit Position"
        
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    private func showCurrentAccountTracker() {
        if let controller = AppStoryboard.Detail.instance.instantiateViewController(withIdentifier: DepositActionDetailsVC.storyboardId) as? DepositActionDetailsVC {
            controller.actionName = "currentAccount"
            controller.navigationTitle = "Current Account Tracker"
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    private func showSavingAccountTracker() {
        if let controller = AppStoryboard.Detail.instance.instantiateViewController(withIdentifier: DepositActionDetailsVC.storyboardId) as? DepositActionDetailsVC {
            controller.actionName = "savingAccount"
            controller.navigationTitle = "Saving Account Tracker"
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }

    private func showTermDepositTracker() {
        if let controller = AppStoryboard.Detail.instance.instantiateViewController(withIdentifier: DepositActionDetailsVC.storyboardId) as? DepositActionDetailsVC {
            controller.actionName = "termDeposit"
            controller.navigationTitle = "Terms Deposit Tracker"
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}

//MARK:- CollectionView

extension DepositMenuDetailsVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return self.dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let data = self.dataSource[indexPath.section]
        switch data.name {
        case "headValues":
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TopBarCVC.storyboardId, for: indexPath) as? TopBarCVC else { return UICollectionViewCell() }
            cell.backgroundColor = .clear
            cell.contentView.backgroundColor = .clear
            cell.setupData(data.value as! [HeadValueModel])
            return cell
        case "etbntbMovement", "budget", "topCustomer", "branches":
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DetailMovementCVC.storyboardId, for: indexPath) as? DetailMovementCVC else { return UICollectionViewCell() }
            cell.backgroundColor = .clear
            cell.contentView.backgroundColor = .clear
            cell.setupData(data.value as! [DepositListViewModel])
            return cell
        case "casamix":
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CASAMixCVC.storyboardId, for: indexPath) as? CASAMixCVC else { return UICollectionViewCell() }
            cell.backgroundColor = .clear
            cell.contentView.backgroundColor = .clear
            cell.setupPieChart(data: data.value as! [DepositChartModel])
            return cell
        case "tierwisepie":
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TeirWiseChartCVC.storyboardId, for: indexPath) as? TeirWiseChartCVC else { return UICollectionViewCell() }
            cell.backgroundColor = .clear
            cell.contentView.backgroundColor = .clear
            cell.setupPieChart(data: data.value as! [DepositChartModel])
            return cell
        case "bottomView":
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DetailsButtonCVC.storyboardId, for: indexPath) as? DetailsButtonCVC else { return UICollectionViewCell() }
            cell.backgroundColor = .clear
            cell.contentView.backgroundColor = .clear
            cell.delegate = self
            return cell
        default:
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        let data = self.dataSource[indexPath.section]
        switch data.name {
        case "etbntbMovement", "budget", "topCustomer", "branches":
            self.showDetails(data)
            break
        default:
            break
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let data = self.dataSource[indexPath.section]
        switch data.name {
        case "headValues":
            return CGSize(width: collectionView.frame.width, height: 60)
        case "etbntbMovement", "budget", "topCustomer", "branches":
            return CGSize(width: collectionView.frame.width, height: CGFloat(getListViewItemHeightWithIndex(indexPath.section)))
        case "casamix":
            return CGSize(width: collectionView.frame.width, height: 250)
        case "tierwisepie":
            return CGSize(width: collectionView.frame.width, height: 350)
        case "bottomView":
            return CGSize(width: collectionView.frame.width, height: 400)
        default:
            return CGSize(width: 0, height: 0)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
    }
    
    // MARK: - Helper Methods
    
    func getListViewItemHeightWithIndex(_ index: Int) -> Double {
        let data = self.dataSource[index]

        let header = 44.0
        let footer = 20.0
        var rows = 0.0
        if let values = data.value as? [DepositListViewModel] {
            rows = rows + (Double(values.count) * 25.0)
        }
        
        return header + footer + rows
    }
}

//MARK: - Custom Delegate
extension DepositMenuDetailsVC: DetailsButtonDelegate {
    func didTapOnButton(_ sender: UIView?) {
        guard let view = sender else { return }
        switch view.tag {
        case 1000:
            self.showDailyDepositPosition()
            break
        case 1001:
            self.showCurrentAccountTracker()
            break
        case 1002:
            self.showSavingAccountTracker()
            break
        case 1003:
            self.showTermDepositTracker()
            break
        default:
            break
        }
    }
}
