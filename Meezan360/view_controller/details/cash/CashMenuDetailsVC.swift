//
//  CashOptimizationVC.swift
//  Meezan360
//
//  Created by Syed Arsalan Shah on 25/10/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit
import ObjectMapper

enum CashDetailsSections: Int, CaseIterable {
    case topBar
    case cashPosition
    case cashLimit
    case bankBalance
    case currencyPosition
}

class CashMenuDetailsVC: BaseController {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var regionNameLabel: UILabel!
    
    private let sectionInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
    private var itemsPerRow = 4
    private var dataSource: CashModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupNavigation()
        setupCollectionView()
        
        self.getCashDetails()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    private func setupNavigation() {
        navigationItem.title = "Cash Optimization"
        navigationController?.view.backgroundColor = UIColor.white
        navigationController?.navigationBar.prefersLargeTitles = true
        
        self.addSearchButton()
    }
    
    private func setupCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self
        
        // Register collection view cell nibs
        collectionView.register(UINib(nibName: TopBarCVC.storyboardId, bundle: nil), forCellWithReuseIdentifier: TopBarCVC.storyboardId)
        collectionView.register(UINib(nibName: CashPositionLineChartCVC.storyboardId, bundle: nil), forCellWithReuseIdentifier: CashPositionLineChartCVC.storyboardId)
        collectionView.register(UINib(nibName: CashLimitCVC.storyboardId, bundle: nil), forCellWithReuseIdentifier: CashLimitCVC.storyboardId)
        collectionView.register(UINib(nibName: BalanceWithBanksCVC.storyboardId, bundle: nil), forCellWithReuseIdentifier: BalanceWithBanksCVC.storyboardId)
        collectionView.register(UINib(nibName: DetailMovementCVC.storyboardId, bundle: nil), forCellWithReuseIdentifier: DetailMovementCVC.storyboardId)
    }
    
    override func didApplySearchFilters() {
        self.getCashDetails()
    }
    
    // MARK: - Web Service Methods
    
    private func getCashDetails() {
        ActivityIndicatorManager.showLoader()
        let params = [
            "action": WebRoute.cashAction.rawValue,
            "userid": AppStateManager.shared.getUserId() ?? ""
        ]
        APIManager.sharedInstance.user.postArrayRequestWith(params: params, success: { (responseArray) in
            ActivityIndicatorManager.hideLoader()
            let array = Mapper<CashModel>().mapArray(JSONArray: responseArray as! [[String: Any]])
            self.dataSource = array.first
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.regionNameLabel.text = self.dataSource?.summary?.regionName
                if let voiceText = self.dataSource?.summary?.voiceText {
                    self.playVoiceText(voiceText)
                }
                self.collectionView.reloadData()
            }

        }) { (error) in
            ActivityIndicatorManager.hideLoader()
            if error.code == -1000 {
                self.showInvalidTokenAlert()
            } else if error.code == -2000 {
                self.showChangePasswordAlert()
            }  else {
                AlertManager.showErrorWith(message: error.localizedDescription)
            }
        }
    }
    
    // MARK: - Navigation Methods
    
    private func showDetails(_ actionName: String, title: String?) {
        let controller = PagerVC(nibName: PagerVC.storyboardId, bundle: nil)
        controller.actionName = actionName
        controller.navigationTitle = title ?? actionName
        
        self.navigationController?.pushViewController(controller, animated: true)
    }
}

//MARK:- CollectionView
private var tableDataSource = DummyData().currencyPositionTableData()

extension CashMenuDetailsVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return (self.dataSource != nil) ? CashDetailsSections.allCases.count : 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.section {
        case CashDetailsSections.topBar.rawValue:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TopBarCVC.storyboardId, for: indexPath) as? TopBarCVC else { return UICollectionViewCell() }
            cell.backgroundColor = .clear
            if let data = self.dataSource?.headValues {
                cell.setupData(data)
            }
            return cell
        case CashDetailsSections.cashPosition.rawValue:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CashPositionLineChartCVC.storyboardId, for: indexPath) as? CashPositionLineChartCVC else { return UICollectionViewCell() }
            cell.backgroundColor = .clear
            if let data = self.dataSource?.cashPosition {
                cell.setupData(data)
            }
            return cell
        case CashDetailsSections.cashLimit.rawValue:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CashLimitCVC.storyboardId, for: indexPath) as? CashLimitCVC else { return UICollectionViewCell() }
            cell.backgroundColor = .clear
            if let data = self.dataSource?.cashLimit {
                cell.setupData(data)
            }
            return cell
        case CashDetailsSections.bankBalance.rawValue:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BalanceWithBanksCVC.storyboardId, for: indexPath) as? BalanceWithBanksCVC else { return UICollectionViewCell() }
            cell.backgroundColor = .clear
            if let data = self.dataSource?.balance {
                cell.setupData(data)
            }
            return cell
        case CashDetailsSections.currencyPosition.rawValue:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DetailMovementCVC.storyboardId, for: indexPath) as? DetailMovementCVC else { return UICollectionViewCell() }
            cell.backgroundColor = .clear
            if let data = self.dataSource?.currenty {
                cell.setupData(data, cellType: .cashMenuList)
            }
            return cell
        default:
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        switch indexPath.section {
        case CashDetailsSections.currencyPosition.rawValue:
            self.showDetails("currencyPosition", title: "Currency Position")
            break
        default:
            break
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch indexPath.section {
        case CashDetailsSections.topBar.rawValue:
            return CGSize(width: collectionView.frame.width, height: 60)
        case CashDetailsSections.cashPosition.rawValue,
             CashDetailsSections.cashLimit.rawValue,
             CashDetailsSections.bankBalance.rawValue:
            return CGSize(width: collectionView.frame.width, height: 250)
        case CashDetailsSections.currencyPosition.rawValue:
            return CGSize(width: collectionView.frame.width, height: CGFloat(self.getListViewItemHeight()))
        default:
            return CGSize(width: 0, height: 0)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.bottom
    }
    
    // MARK: - Helper Methods
    
    func getListViewItemHeight() -> Double {
        var height = 40.0 + 20.0
        if let data = self.dataSource?.currenty {
            height = height + (Double(data.count) * 25.0)
        }
        
        return height
    }
}
