//
//  ADCMenuDetailsVC.swift
//  Meezan360
//
//  Created by Akber Sayani on 30/10/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit
import Charts
import ObjectMapper

class ADCMenuDetailsVC: BaseController, ChartViewDelegate {
    @IBOutlet weak var regionNameLabel: UILabel!

    @IBOutlet weak var textLabel1: UILabel!
    @IBOutlet weak var textLabel2: UILabel!
    @IBOutlet weak var textLabel3: UILabel!
    @IBOutlet weak var textLabel4: UILabel!
    @IBOutlet weak var textLabel5: UILabel!
    @IBOutlet weak var textLabel6: UILabel!
    
    @IBOutlet weak var valueLabel1: UILabel!
    @IBOutlet weak var valueLabel2: UILabel!
    @IBOutlet weak var valueLabel3: UILabel!
    @IBOutlet weak var valueLabel4: UILabel!
    @IBOutlet weak var valueLabel5: UILabel!
    @IBOutlet weak var valueLabel6: UILabel!
    
    @IBOutlet weak var pieChartView: PieChartView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    private let CELLWIDTH = 75
    private let CELLSPACING = 10

    var dataSource: ADCModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupNavigation()
        setupPieChart()
        setupCollectionView()
        
        self.getADCDetails()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    // MARK: - Setup Methods
    
    private func setupNavigation() {
        navigationItem.title = "ADC"
        navigationController?.view.backgroundColor = UIColor.white
        navigationController?.navigationBar.prefersLargeTitles = true
        
        self.addSearchButton()
    }
    
    private func setupPieChart() {
        pieChartView.delegate = self
        pieChartView.isMultipleTouchEnabled = false
        pieChartView.rotationEnabled = false
        pieChartView.drawHoleEnabled = false
        pieChartView.chartDescription?.enabled = false
        pieChartView.legend.enabled = false

        self.addShadow(to: pieChartView)
    }
    
    private func setupCollectionView() {
        // Initialization code
        collectionView.delegate = self
        collectionView.dataSource = self
        
        // Register collection view nibs
        self.collectionView.register(UINib(nibName: ADCMenuChartLegendCVC.storyboardId, bundle: nil), forCellWithReuseIdentifier: ADCMenuChartLegendCVC.storyboardId)
    }
    
    override func didApplySearchFilters() {
        self.getADCDetails()
    }
    
    // MARK: - Helper Methods
    
    private func addShadow(to pie: PieChartView) {
        pie.layer.shadowColor = UIColor.gray.cgColor
        pie.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        pie.layer.shadowRadius = 12.0
        pie.layer.shadowOpacity = 0.7
    }
    
    private func populateData() {
        if let headValues = self.dataSource?.headValues {
            for (index, value) in headValues.enumerated() {
                switch index {
                case 0:
                    self.textLabel1.text = value.value1
                    self.valueLabel1.setHtmlText(value.value2)
                    break
                case 1:
                    self.textLabel2.text = value.value1
                    self.valueLabel2.setHtmlText(value.value2)
                    break
                case 2:
                    self.textLabel3.text = value.value1
                    self.valueLabel3.setHtmlText(value.value2)
                    break
                case 3:
                    self.textLabel4.text = value.value1
                    self.valueLabel4.setHtmlText(value.value2)
                    break
                case 4:
                    self.textLabel5.text = value.value1
                    self.valueLabel5.setHtmlText(value.value2)
                    break
                case 5:
                    self.textLabel6.text = value.value1
                    self.valueLabel6.setHtmlText(value.value2)
                    break
                default:
                    break
                }
            }
        }
    }
    
    private func reloadChartDataSet() {
        if let pieChartValues = self.dataSource?.pie {
            var dataEntries = [ChartDataEntry]()
            var colors = [UIColor]()
            
            for item in pieChartValues {
                let data = PieChartDataEntry(value: Double(item.value1?.digits ?? "0")!, label: item.key)
                dataEntries.append(data)
                
                // Add pie chart color
                if let hexCode = item.color { colors.append(UIColor(hexString: hexCode)) }
                else { colors.append(UIColor.random) }
            }
            
            let dataSet = PieChartDataSet(entries: dataEntries, label: "")
            //pieChartView background colors
            dataSet.colors = colors
            //dataSet.colors = ChartColorTemplates.material()
            dataSet.drawValuesEnabled = false
            dataSet.entryLabelFont = NSUIFont.systemFont(ofSize: 12.0)
            
            //pieChartView text colors
            dataSet.valueColors = [UIColor.black]
            dataSet.selectionShift = 8
                        
            let data = PieChartData(dataSet: dataSet)
            pieChartView.data = data
            
            //Hide labels while animation
            pieChartView.drawEntryLabelsEnabled = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                self.pieChartView.drawEntryLabelsEnabled = true
                self.collectionView.reloadData()
            }
            
            //This must stay at end of function
            pieChartView.animate(xAxisDuration: 2, yAxisDuration: 2, easingOption: .easeOutSine)
        }
    }
    
    // MARK: - Web Service Methods
    
    private func getADCDetails() {
        ActivityIndicatorManager.showLoader()
        let params = [
            "action": WebRoute.adcAction.rawValue,
            "userid": AppStateManager.shared.getUserId() ?? ""
        ]
        APIManager.sharedInstance.user.postArrayRequestWith(params: params, success: { (responseArray) in
            ActivityIndicatorManager.hideLoader()
            let array = Mapper<ADCModel>().mapArray(JSONArray: responseArray as! [[String: Any]])
            self.dataSource = array.first
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.regionNameLabel.text = self.dataSource?.summary?.regionName
                if let voiceText = self.dataSource?.summary?.voiceText {
                    self.playVoiceText(voiceText)
                }
                self.populateData()
                self.reloadChartDataSet()
            }
        }) { (error) in
            ActivityIndicatorManager.hideLoader()
            if error.code == -1000 {
                self.showInvalidTokenAlert()
            } else if error.code == -2000 {
                self.showChangePasswordAlert()
            } else {
                AlertManager.showErrorWith(message: error.localizedDescription)
            }
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
}

extension ADCMenuDetailsVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource?.pie?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ADCMenuChartLegendCVC.storyboardId, for: indexPath) as? ADCMenuChartLegendCVC else { return UICollectionViewCell() }
        cell.backgroundColor = .clear
        if let data = self.dataSource?.pie {
            cell.setupData(data[indexPath.row])
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: CELLWIDTH, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let totalCellWidth = CELLWIDTH * collectionView.numberOfItems(inSection: 0)
        let totalSpacingWidth = CELLSPACING * (collectionView.numberOfItems(inSection: 0) - 1)

        let leftInset = (collectionView.layer.frame.size.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
        let rightInset = leftInset

        return UIEdgeInsets(top: 0, left: leftInset, bottom: 0, right: rightInset)
    }
}
