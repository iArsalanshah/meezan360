//
//  SellDetailsVC.swift
//  Meezan360
//
//  Created by Syed Arsalan Shah on 12/10/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit
import ObjectMapper

enum OtherMenuDetailsSections: Int, CaseIterable {
    case topBar
    case dataList
}

class OtherMenuDetailsVC: BaseController {
    @IBOutlet weak var mainCollectionView: UICollectionView!
    @IBOutlet weak var regionNameLabel: UILabel!
    
    var selectedItemName: String?
    var navigationTitle: String?
    
    var dataSource: OtherMenuItemModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupNavigation()
        setupCollectionView()
        
        // Call web service
        self.getDataFromService()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    private func setupNavigation() {
        navigationItem.title = navigationTitle
        navigationController?.view.backgroundColor = UIColor.white
        navigationController?.navigationBar.prefersLargeTitles = true
        
        self.addSearchButton()
    }
    
    private func setupCollectionView() {
        mainCollectionView.delegate = self
        mainCollectionView.dataSource = self
        
        // Register collection view nibs
        mainCollectionView.register(UINib(nibName: TopBarCVC.storyboardId, bundle: nil), forCellWithReuseIdentifier: TopBarCVC.storyboardId)
        mainCollectionView.register(UINib(nibName: DetailMovementCVC.storyboardId, bundle: nil), forCellWithReuseIdentifier: DetailMovementCVC.storyboardId)
    }
    
    override func didApplySearchFilters() {
        self.getDataFromService()
    }
    
    // MARK: - Web Service Methods
    
    private func getDataFromService() {
        switch selectedItemName {
        case "financing":
            self.getApiDetailsWithAction(WebRoute.financeAction.rawValue)
            break
        case "crosssell":
            self.getApiDetailsWithAction(WebRoute.crossSellAction.rawValue)
            break
        case "controls":
            self.getApiDetailsWithAction(WebRoute.controlsAction.rawValue)
            break
        case "profitability":
            self.getApiDetailsWithAction(WebRoute.profitablityAction.rawValue)
            break
        case "premimum":
            self.getApiDetailsWithAction(WebRoute.premiumAction.rawValue)
            break
        case "compliance":
            self.getApiDetailsWithAction(WebRoute.complianceAction.rawValue)
            break
        default:
            break
        }
    }
    
    private func getApiDetailsWithAction(_ action: String) {
        ActivityIndicatorManager.showLoader()
        let params = [
            "action": action,
            "userid": AppStateManager.shared.getUserId() ?? ""
        ]
        APIManager.sharedInstance.user.postArrayRequestWith(params: params, success: { (responseArray) in
            ActivityIndicatorManager.hideLoader()
            let array = Mapper<OtherMenuItemModel>().mapArray(JSONArray: responseArray as! [[String: Any]])
            self.dataSource = array.first
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.regionNameLabel.text = self.dataSource?.summary?.regionName
                if let voiceText = self.dataSource?.summary?.voiceText {
                    self.playVoiceText(voiceText)
                }
                self.mainCollectionView.reloadData()
            }
        }) { (error) in
            ActivityIndicatorManager.hideLoader()
            if error.code == -1000 {
                self.showInvalidTokenAlert()
            } else if error.code == -2000 {
                self.showChangePasswordAlert()
            }  else {
                AlertManager.showErrorWith(message: error.localizedDescription)
            }
        }
    }
}

//MARK:- CollectionView
private let sectionInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
private var dataSourceCount = 2
private let IndexTopBar = 0
private let IndexTableView = 1
private var tableDataSource = DummyData().sellDetailTableData()

extension OtherMenuDetailsVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return (self.dataSource != nil) ? OtherMenuDetailsSections.allCases.count : 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return ((self.dataSource?.headValues?.count ?? 0) > 0) ? 1 : 0
        }
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.section {
        case OtherMenuDetailsSections.topBar.rawValue:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TopBarCVC.storyboardId, for: indexPath) as? TopBarCVC else { return UICollectionViewCell() }
            cell.backgroundColor = .clear
            if let data = self.dataSource?.headValues {
                cell.setupData(data)
            }
            return cell
        case OtherMenuDetailsSections.dataList.rawValue:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DetailMovementCVC.storyboardId, for: indexPath) as? DetailMovementCVC else { return UICollectionViewCell() }
            cell.backgroundColor = .clear
            if let data = self.dataSource?.dataValues {
                cell.setupData(data, cellType: .otherMenuList)
            }
            return cell
        default:
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch indexPath.section {
        case IndexTopBar:
            return CGSize(width: collectionView.frame.width, height: 60.0)
        case IndexTableView:
            return CGSize(width: collectionView.frame.width, height: CGFloat(self.getListViewItemHeightWithIndex()))
        default:
            return CGSize(width: 0, height: 0)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.bottom
    }
    
    // MARK: - Helper Methods
    
    func getListViewItemHeightWithIndex() -> Double {
        let header = 44.0
        let footer = 20.0
        let rows = Double(self.dataSource?.dataValues?.count ?? 0) * 25.0
        return header + footer + rows
    }
}
