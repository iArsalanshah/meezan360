//
//  PagerContentViewController.swift
//  Hunters
//
//  Created by Akber Sayani on 09/11/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit

class PagerContentViewController: BaseController {
    @IBOutlet weak var collectionView: UICollectionView!
    var headValues: [HeadValueModel]?    
    var dataSource: [PageValueModel]?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupCollectionView()
    }
    
    private func setupCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self
        
        // Register collection view nibs
        self.collectionView.register(UINib(nibName: TopBarCVC.storyboardId, bundle: nil), forCellWithReuseIdentifier: TopBarCVC.storyboardId)
        self.collectionView.register(UINib(nibName: PagerDetailsCVC.storyboardId, bundle: nil), forCellWithReuseIdentifier: PagerDetailsCVC.storyboardId)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - CollectionView

extension PagerContentViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            let count = self.headValues?.count ?? 0
            return (count > 0) ? 1 : 0
        }
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.section {
        case 0:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TopBarCVC.storyboardId, for: indexPath) as? TopBarCVC else { return UICollectionViewCell() }
            cell.backgroundColor = .clear
            if let data = self.headValues {
                cell.setupData(data)
            }
            return cell
        case 1:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PagerDetailsCVC.storyboardId, for: indexPath) as? PagerDetailsCVC else { return UICollectionViewCell() }
            cell.backgroundColor = .clear
            if let data = self.dataSource {
                cell.setupData(data)
            }
            return cell
        default:
            return UICollectionViewCell()
        }

    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch indexPath.section {
        case 0:
            return CGSize(width: self.view.frame.width, height: 60.0)
        case 1:
            return CGSize(width: collectionView.frame.width, height: CGFloat(self.getListViewItemHeight()))
        default:
            return CGSize(width: 0, height: 0)
        }
    }

    func getListViewItemHeight() -> Double {
        let header = 20.0 + 16.0
        let footer = 20.0
        let rows = Double(self.dataSource?.count ?? 0) * 25.0

        return header + footer + rows
    }
}
