//
//  PagerVC.swift
//  Meezan360
//
//  Created by Syed Arsalan Shah on 05/11/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit
import ObjectMapper
import LZViewPager

class PagerVC: BaseController {
    @IBOutlet weak var regionNameLabel: UILabel!
    @IBOutlet weak var viewPager: LZViewPager!
    private var subControllers:[UIViewController] = []
    
    private var dataSource = [PagerModel]()
    private var selectedIndex = 0
    
    var actionName: String?
    var navigationTitle: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigation()
        setupViewPager()
        
        self.getDetails()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    private func setupNavigation() {
        navigationItem.title = self.navigationTitle
        navigationController?.view.backgroundColor = UIColor.white
        navigationController?.navigationBar.prefersLargeTitles = true
        
        self.addSearchButton()
    }
    
    private func setupViewPager() {
        viewPager.dataSource = self
        viewPager.delegate = self
        viewPager.hostController = self
    }
    
    private func reloadViewPager() {
        guard self.dataSource.count > 0 else { return }
        
        for item in self.dataSource {
            let controller = PagerContentViewController(nibName: PagerContentViewController.storyboardId, bundle: nil)
            controller.title = item.title
            controller.dataSource = item.values
            controller.headValues = item.headValues
            self.subControllers.append(controller)
        }
        
        self.viewPager.reload()
    }
    
    override func didApplySearchFilters() {
        self.getDetails()
    }
            
    // MARK: - Web Service Methods
    
    private func getWebServiceAction() -> String? {
        if let actionName = self.actionName {
            switch actionName {
            case "etbntbMovement":
                return WebRoute.movementAction.rawValue
            case "budget":
                return WebRoute.budgetAction.rawValue
            case "topCustomer":
                return WebRoute.topCustomerAction.rawValue
            case "branches":
                return WebRoute.branchesAction.rawValue
            case "currencyPosition":
                return WebRoute.currencyAction.rawValue
            case "wealth":
                return WebRoute.wealthAction.rawValue
            case "compliance":
                return WebRoute.complianceExtendedAction.rawValue
            case "controls":
                return WebRoute.controlsExtendedAction.rawValue
            case "dailyPosition":
                return WebRoute.dailyDepositPosition.rawValue
            default:
                return nil
            }
        }
        
        return nil
    }
        
    private func getDetails() {
        ActivityIndicatorManager.showLoader()
        let params = [
            "action": self.getWebServiceAction() ?? "",
            "userid": AppStateManager.shared.getUserId() ?? ""
        ]
        APIManager.sharedInstance.user.postArrayRequestWith(params: params, success: { (responseArray) in
            ActivityIndicatorManager.hideLoader()
            self.dataSource = Mapper<PagerModel>().mapArray(JSONArray: responseArray as! [[String: Any]])
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.regionNameLabel.text = self.dataSource.first?.summary?.regionName
                if let voiceText = self.dataSource.first?.summary?.voiceText {
                    self.playVoiceText(voiceText)
                }
                self.reloadViewPager()
            }
        }) { (error) in
            ActivityIndicatorManager.hideLoader()
            if error.code == -1000 {
                self.showInvalidTokenAlert()
            } else if error.code == -2000 {
                self.showChangePasswordAlert()
            }  else {
                AlertManager.showErrorWith(message: error.localizedDescription)
            }
        }
    }
}

// MARK: - LZPagerView

extension PagerVC: LZViewPagerDelegate, LZViewPagerDataSource {
    func numberOfItems() -> Int {
        return self.subControllers.count
    }
    
    func controller(at index: Int) -> UIViewController {
        return subControllers[index]
    }
    
    func button(at index: Int) -> UIButton {
        //Customize your button styles here
        let button = UIButton()
        button.setTitleColor(UIColor.black, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 15.0)
        return button
    }
    
    func widthForButton(at index: Int) -> CGFloat {
        let defaultWidth = self.view.bounds.size.width/CGFloat(self.subControllers.count)
        let controller = self.subControllers[index] as! PagerContentViewController
        if let width = controller.title?.widthOfString(usingFont: UIFont.systemFont(ofSize: 15.0)) {
            return max(defaultWidth, (width + 10.0))
        }
        
        return defaultWidth
    }
}
