//
//  HomeTabController.swift
//  ChartViewTestProject
//
//  Created by Amr Abd Elwahab on 19/09/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit
import BetterSegmentedControl
import Charts
import RAMAnimatedTabBarController
import ObjectMapper
import ESPullToRefresh
import AVFoundation

class HomeTabController: BaseController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var regionNameLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pieChartSelectionImageView: UIImageView!
    @IBOutlet weak var lineChartTitleLabel: UILabel!
    @IBOutlet weak var pie: PieChartView!
    @IBOutlet weak var lineChart: LineChartView!
    @IBOutlet weak var topView: UIView!
    
    private let SPIN_ANIMATION_DURATION: Double = 1.0
    private var lastSelectedIndex: Double = 0
    private let months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
    
    var dataSource = [DashboardModel]()
    var selectedItem: DashboardModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        //setupSegment()
        setupPullToRefresh()
        setupCollectionView()
        setupPieChart()
        setupLineChart()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.getDashboardDetails()
        }        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    private func setupPullToRefresh() {
        self.scrollView.es.addPullToRefresh {
            [unowned self] in
            /// Do anything you want...
            self.getDashboardDetails(isShowLoading: false)
        }
    }
    
    private func updateTitleLabel(_ data: DashboardModel?) {
        self.titleLabel.text = data?.name
        self.titleLabel.textColor = UIColor(hexString: data?.colorHexCode ?? "#000000")
    }
    
    override func didApplySearchFilters() {
        self.getDashboardDetails()
    }
    
    // MARK: - Navigation Methods
    
    private func showDetails() {
        let data = self.dataSource[Int(lastSelectedIndex)]
        if let identifier = data.identifier {
            switch identifier {
            case "deposit":
                self.showDepositMenuDetailsVC()
                break
            case "cash":
                self.showCashMenuDetailsVC()
                break
            case "adc":
                self.showADCMenuDetailsVC()
                break
            case "wealth", "controls", "compliance":
                self.showPagerMenuDetailVC(data.name, action: identifier)
                break
            default:
                self.showOtherMenuDetailsVC(withSelectedItemName: identifier, navigationTitle: data.name)
                break
            }
        }
    }
    
    private func showDepositMenuDetailsVC() {
        if let controller = AppStoryboard.Detail.instance.instantiateViewController(withIdentifier: DepositMenuDetailsVC.storyboardId) as? DepositMenuDetailsVC {
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    private func showCashMenuDetailsVC() {
        if let controller = AppStoryboard.Detail.instance.instantiateViewController(withIdentifier: CashMenuDetailsVC.storyboardId) as? CashMenuDetailsVC {
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    private func showADCMenuDetailsVC() {
        if let controller = AppStoryboard.Detail.instance.instantiateViewController(withIdentifier: ADCMenuDetailsVC.storyboardId) as? ADCMenuDetailsVC {
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    private func showPagerMenuDetailVC(_ title: String?, action: String) {
        let controller = PagerVC(nibName: PagerVC.storyboardId, bundle: nil)
        controller.actionName = action
        controller.navigationTitle = title
        
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    private func showOtherMenuDetailsVC(withSelectedItemName name: String, navigationTitle: String?) {
        if let controller = AppStoryboard.Detail.instance.instantiateViewController(withIdentifier: OtherMenuDetailsVC.storyboardId) as? OtherMenuDetailsVC {
            controller.selectedItemName = name
            controller.navigationTitle = navigationTitle
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    // MARK: - Web Service Methods
    
    private func getDashboardDetails(isShowLoading: Bool = true) {
        if isShowLoading { ActivityIndicatorManager.showLoader() }
        let params = [
            "action": WebRoute.dashboardAction.rawValue,
            "userid": AppStateManager.shared.getUserId() ?? "0"
        ]
        APIManager.sharedInstance.user.postArrayRequestWith(params: params, success: { (responseArray) in
            ActivityIndicatorManager.hideLoader()
            self.scrollView.es.stopPullToRefresh()
            self.pie.isHidden = false
            self.dataSource = Mapper<DashboardModel>().mapArray(JSONArray: responseArray as! [[String : Any]])
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.setPieChartDataSet()
                self.regionNameLabel.text = self.dataSource.first?.summary?.regionName
                if let voiceText = self.dataSource.first?.summary?.voiceText {
                    self.playVoiceText(voiceText)
                }
            }
        }) { (error) in
            ActivityIndicatorManager.hideLoader()
            self.scrollView.es.stopPullToRefresh()
            if error.code == -1000 {
                self.showInvalidTokenAlert()
            } else if error.code == -2000 {
                self.showChangePasswordAlert()
            }  else {
                AlertManager.showErrorWith(message: error.localizedDescription)
            }
        }
    }
}

//MARK:- PIE CHART

extension HomeTabController: ChartViewDelegate {
    private func setupPieChart() {
        pie.delegate = self
        
        pie.isMultipleTouchEnabled = false
        pie.rotationEnabled = true //rotation gesture
        pie.highlightPerTapEnabled = true
        pie.drawHoleEnabled = false
        pie.rotationAngle = 0 //to match data set index with pie slice position
        pie.chartDescription?.enabled = false
        pie.legend.enabled = false
    }
    
    private func setPieChartDataSet() {
        guard self.dataSource.count > 0 else {
            return
        }
        
        var defaultIndex = 0
        var pieChartDataEntries = [PieChartDataEntry]()
        var pieChartColors = [UIColor]()
        for (index, item) in self.dataSource.enumerated() {
            let label = (item.name ?? "-") + "\n" + (item.syncDate ?? "")
            let dataEntry = PieChartDataEntry(value: 100.0/Double(self.dataSource.count), label: label)
            if let hexCode = item.colorHexCode { pieChartColors.append(UIColor(hexString: hexCode)) }
            else { pieChartColors.append(UIColor.random) }
            
            if let defaultValue = item.defaultValue, defaultValue.lowercased() == "yes" {
                defaultIndex = index
            }
            
            pieChartDataEntries.append(dataEntry)
        }
        
        let dataSet = PieChartDataSet(entries: pieChartDataEntries, label: "Widget Types")
        dataSet.colors = pieChartColors
        dataSet.valueColors = [UIColor.white]
        dataSet.selectionShift = 8
        dataSet.drawValuesEnabled = false
        dataSet.entryLabelFont = NSUIFont.systemFont(ofSize: 12.0)
        
        let chartData = PieChartData(dataSet: dataSet)
        pie.data = chartData
        
        // need to fix this constant value
        //self.pie.rotationAngle = CGFloat((360.0/Double(self.dataSource.count)) * Double(defaultIndex))
        self.pie.rotationAngle = self.getAngle(with: Double(defaultIndex))
        
        //Hide labels while animation
        pie.drawEntryLabelsEnabled = false
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
            self.pieChartSelectionImageView.isHidden = false
            self.pie.drawEntryLabelsEnabled = true
            
            //default selected
            self.lastSelectedIndex = Double(defaultIndex)
            self.pie.highlightValue(x: self.lastSelectedIndex, dataSetIndex: 0, callDelegate: false)
            
            //reload head value dataset
            self.selectedItem = self.dataSource[defaultIndex]
            self.collectionView.reloadData()
            
            //reload line chart dataset
            self.topView.isHidden = false
            self.lineChartTitleLabel.isHidden = false
            self.lineChart.isHidden = false
            self.setLineChartDataSet()
            self.updateTitleLabel(self.dataSource[defaultIndex])
        }
        
        //This must stay at end of function
        pie.animate(xAxisDuration: 2.0, yAxisDuration: 2.0, easingOption: .easeOutExpo)
    }
    
    private func getAngle(with index: Double) -> CGFloat {
        switch index {
        case 0:
            return CGFloat(7 * 36)
        case 1:
            return CGFloat(6 * 36)
        case 2:
            return CGFloat(5 * 36)
        case 3:
            return CGFloat(4 * 36)
        case 4:
            return CGFloat(3 * 36)
        case 5:
            return CGFloat(2 * 36)
        case 6:
            return CGFloat(1 * 36)
        case 7:
            return CGFloat(0 * 36)
        case 8:
            return CGFloat(9 * 36)
        case 9:
            return CGFloat(8 * 36)
            
        default:
            return CGFloat(0)
        }
    }
    
    private func getIndex(with angle: CGFloat) -> Int {
        switch angle {
        case 0..<18 :
            return 7
        case 18..<54:
            return 6
        case 54..<90:
            return 5
        case 90..<126:
            return 4
        case 126..<162:
            return 3
        case 162..<198:
            return 2
        case 198..<234:
            return 1
        case 234..<270:
            return 0
        case 270..<306:
            return 9
        case 306..<342:
            return 8
        case 342..<360:
            return 7
        default:
            return 7
        }
    }
    
    func chartValueHighlightBySelectedIndex(_ index: Double, fromAngle: CGFloat) {
        let toAngle = self.getAngle(with: index)
        self.pie.spin(duration: SPIN_ANIMATION_DURATION, fromAngle: fromAngle, toAngle: toAngle, easingOption: .easeInOutSine)
        
        // Save current index value
        self.lastSelectedIndex = index
        
        // Update title label
        self.updateTitleLabel(self.dataSource[Int(index)])
        
        // Reload head value dataset
        self.selectedItem = self.dataSource[Int(index)]
        self.collectionView.reloadData()
        
        // reload line chart dataset
        self.setLineChartDataSet()
    }
    
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        if let data = chartView.data, data.isKind(of: PieChartData.self) {
            let currentIndex = highlight.x
            if currentIndex == lastSelectedIndex {
                //Show details
                self.showDetails()
            } else {
                // Update pie chart with highlight value
                self.chartValueHighlightBySelectedIndex(currentIndex, fromAngle: self.getAngle(with: lastSelectedIndex))
            }
        } else {
            //line graph
            let xValue = highlight.x
            let yValue = highlight.y
            print("y = \(yValue), x = \(months[Int(xValue)])")
        }
    }
    
    func chartValueNothingSelected(_ chartView: ChartViewBase) {
        //to reselect last selected slice
        if let data = chartView.data, data.isKind(of: PieChartData.self) {
            pie.highlightValue(x: lastSelectedIndex, dataSetIndex: 0, callDelegate: false)
        }
    }
    
    /// Callbacks rotation gesture events
    func chartViewRotationDidStop(_ chartView: ChartViewBase) {
        let currentIndex = Double(self.getIndex(with: self.pie.rotationAngle))
        
        //self.pie.hight
        self.pie.highlightValue(x: currentIndex, dataSetIndex: 0, callDelegate: false)
        self.chartValueHighlightBySelectedIndex(currentIndex, fromAngle: self.pie.rotationAngle)
    }
}

//MARK:- Collection View

extension HomeTabController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    private func setupCollectionView() {
        self.collectionView.backgroundColor = .clear
        self.collectionView.register(UINib(nibName: HomeCVC.storyboardId, bundle: nil), forCellWithReuseIdentifier: HomeCVC.storyboardId)
        
        if let layout = self.collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.minimumLineSpacing = 20.0
            layout.minimumInteritemSpacing = 20.0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.selectedItem?.headValues?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeCVC.storyboardId, for: indexPath) as? HomeCVC else { return UICollectionViewCell() }
        cell.backgroundColor = .clear
        if let data = self.selectedItem?.headValues?[indexPath.row] {
            cell.setupData(data)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0)        
    }
}

//MARK:- LINE CHART

extension HomeTabController {
    private func setupLineChart() {
        lineChart.delegate = self
        lineChart.chartDescription?.enabled = false
        lineChart.dragEnabled = false
        lineChart.setScaleEnabled(false)
        lineChart.pinchZoomEnabled = false
        // x-axis limit line
        lineChart.rightAxis.enabled = false
        let xAxis = lineChart.xAxis
        xAxis.drawGridLinesEnabled = false
        xAxis.labelPosition = .bottom
        xAxis.labelFont = UIFont.systemFont(ofSize: 8)
        xAxis.valueFormatter = IndexAxisValueFormatter(values: months)
        
        xAxis.granularity = 1//no. of steps break for x-Axis
        //        lineChart.xAxis.avoidFirstLastClippingEnabled = true
        
        //y-Axis Settings
        let yAxis = lineChart.leftAxis
        yAxis.enabled = true
        yAxis.labelFont = UIFont.systemFont(ofSize: 10)
        yAxis.drawGridLinesEnabled = false
        //legend settigns
        let l = lineChart.legend
        l.horizontalAlignment = .left
        l.verticalAlignment = .top
        l.orientation = .horizontal
        l.drawInside = true
        l.font = UIFont.systemFont(ofSize: 8)
        lineChart.animate(xAxisDuration: 2, yAxisDuration: 2)
        lineChart.marker = getMarker(chartView: lineChart)
        
        //setDataCount(months.count, range: UInt32(45))
    }
    
    private func setLineChartDataSet() {
        guard let graphValues = self.selectedItem?.graphValues else { return }
        
        let dataSets = (0..<graphValues.count).map { i -> LineChartDataSet in
            var chartDataEntries = [ChartDataEntry]()
            if let values = graphValues[i].values {
                for (index, value) in values.enumerated() {
                    let dataEntry = ChartDataEntry(x: Double(index), y: Double(value)!)
                    chartDataEntries.append(dataEntry)
                }
            }
            
            let dataSet = LineChartDataSet(entries: chartDataEntries, label: graphValues[i].name)
            dataSet.lineWidth = 1.8
            dataSet.circleRadius = 4
            dataSet.mode = .cubicBezier
            dataSet.drawCirclesEnabled = false//data points circle
            dataSet.setCircleColor(.white)//data points circle
            dataSet.highlightColor = .systemBlue//on graph click line color
            dataSet.drawHorizontalHighlightIndicatorEnabled = false//on graph click horizontal line
            dataSet.drawVerticalHighlightIndicatorEnabled = false//on graph click vertical line
            dataSet.fillFormatter = CubicLineSampleFillFormatter()
            
            if let color = graphValues[i].colorHexCode {
                dataSet.setColor(UIColor(hexString: color))
            } else {
                dataSet.setColor(UIColor.random)
            }
            
            return dataSet
        }
        
        let data = LineChartData(dataSets: dataSets)
        data.setValueFont(.systemFont(ofSize: 7, weight: .light))
        data.setDrawValues(false)
        lineChart.data = data
    }
    
    func getMarker(chartView: ChartViewBase) -> MarkerImage {
        let marker = BalloonMarker(color: UIColor(white: 180/255, alpha: 1),
                                   font: .systemFont(ofSize: 12),
                                   textColor: .white,
                                   insets: UIEdgeInsets(top: 8, left: 8, bottom: 20, right: 8))
        marker.chartView = chartView
        marker.minimumSize = CGSize(width: 80, height: 40)
        return marker
    }
}

private class CubicLineSampleFillFormatter: IFillFormatter {
    func getFillLinePosition(dataSet: ILineChartDataSet, dataProvider: LineChartDataProvider) -> CGFloat {
        return -10
    }
}
