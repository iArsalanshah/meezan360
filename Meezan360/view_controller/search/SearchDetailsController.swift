//
//  SearchDetailsController.swift
//  Meezan360
//
//  Created by Akber Sayani on 24/10/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit
import Peep
import Haptica
import Hero

protocol SearchDetailsDelegate {
    func didSelectItem(_ item: String, searchFieldType: SearchFieldType)
}

class SearchDetailsController: BaseController {
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.tableFooterView = UIView()
        }
    }
    
    var delegate: SearchDetailsDelegate?
    var searchFieldType: SearchFieldType = .branch

    var selectedIndexPath: IndexPath?
    var dataSource = [String]()
    var filterDataSource = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.filterDataSource = dataSource
        self.setupSubTitle()
        self.hero.isEnabled = true
        self.hero.modalAnimationType = .selectBy(presenting: .push(direction: .left), dismissing: .pull(direction: .right))
    }
    
    private func setupSubTitle() {
        self.subTitleLabel.text = String(format: "Select %@ to filter records", self.searchFieldType.rawValue)
    }
    
    private func searchTextDidChange(_ text: String) {
        self.filterDataSource = self.dataSource.filter({ name -> Bool in
            if text.isEmpty { return true }
            return name.lowercased().contains(text.lowercased())
        })
        
        self.tableView.reloadData()
    }
    
    // MARK: - IBAction Methods
    
    @IBAction func textClearButtonTapped(_ sender: UIButton) {
        Peep.play(sound: KeyPress.tap)
        Haptic.impact(.light).generate()
        self.searchTextField.text = ""
        self.searchTextDidChange("")
    }
    
    @IBAction func submitButtonTapped(_ sender: UIButton) {
        Peep.play(sound: KeyPress.tap)
        Haptic.impact(.light).generate()
        guard self.selectedIndexPath != nil else {
            AlertManager.showWarningBar(message: "No option selected")
            return
        }
        
        if self.delegate != nil {
            let item = self.filterDataSource[selectedIndexPath!.row]
            self.delegate?.didSelectItem(item, searchFieldType: searchFieldType)
        }        

        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SearchDetailsController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filterDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchDetailsCellIdentifier", for: indexPath) as! SearchDetailsViewCell
        cell.populateData(self.filterDataSource[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        // Do nothing
        let cell = cell as! SearchDetailsViewCell
        if let selectedIndexPath = self.selectedIndexPath, selectedIndexPath == indexPath {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.selectedIndexPath = indexPath
        
        tableView.reloadData()
    }
}

extension SearchDetailsController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            searchTextDidChange(updatedText)
        }
        return true
    }
}
