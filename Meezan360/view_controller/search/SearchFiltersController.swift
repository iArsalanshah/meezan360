//
//  SearchFiltersController.swift
//  Meezan360
//
//  Created by Akber Sayani on 24/10/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit
import Peep
import Haptica
import Hero
import ObjectMapper
import DropDown

enum SearchFieldType: String {
    case region = "region"
    case area = "area"
    case branch = "branch"
    case person = "person"
}

protocol SearchFiltersDelegate {
    func didApplySearchFilters()
}

class SearchFiltersController: BaseController {
    @IBOutlet weak var regionTextField: UITextField!
    @IBOutlet weak var areaTextField: UITextField!
    @IBOutlet weak var branchTextField: UITextField!
    @IBOutlet weak var personTextField: UITextField!
    @IBOutlet weak var dateTextField: UITextField!
    
    var delegate: SearchFiltersDelegate?
    
    var regionDropDown: DropDown!
    var areaDropDown: DropDown!
    var branchDropDown: DropDown!
    
    var dataSource = [SearchModel]()
    var selectedBranchCode: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.definesPresentationContext = true
        setupDropDown(withTextField: self.regionTextField)
        setupDropDown(withTextField: self.areaTextField)
        setupDropDown(withTextField: self.branchTextField)
        setupDatePicker(withTextField: self.dateTextField)
        
        self.getSearchFilters()
    }
    
    private func populateData() {
        if let data = AppStateManager.shared.getSearchFilterDetails() {
            self.regionTextField.text = data.regionName
            self.areaTextField.text = data.areaName
            self.branchTextField.text = data.branchName
            self.dateTextField.text = data.searchDate
        }
    }
    
    private func storeSearchFilterData() {
        let data = SearchModel()
        data.regionName = self.regionTextField.text
        data.areaName = self.areaTextField.text
        data.branchName = self.branchTextField.text
        data.searchDate = self.dateTextField.text
        
        AppStateManager.shared.saveSearchFilterDetails(data)
    }
        
    private func setupDropDown(withTextField textField: UITextField) {
        textField.delegate = self
        
        let dropdown = DropDown()
        dropdown.anchorView = textField
        dropdown.direction = .bottom
        dropdown.bottomOffset = CGPoint(x: 0, y: (dropdown.anchorView?.plainView.bounds.height)!)
        dropdown.selectionAction = { (index: Int, item: String) in
            textField.text = (index == 0) ? "" : item
            
            if textField == self.regionTextField {
                // update datasource
                var _dataSource = self.dataSource
                if let selectedRegion = self.regionTextField.text, !selectedRegion.isEmpty, index > 0 {
                    _dataSource = _dataSource.filter {$0.regionName == selectedRegion}
                }
                
                // set area datasource
                self.areaTextField.text = ""
                var areas = _dataSource.map { $0.areaName ?? "" }
                areas = Array(Set(areas)).sorted()
                areas.insert("-- All Areas --", at: 0)
                self.areaDropDown.dataSource = areas
                self.areaDropDown.reloadAllComponents()
                
                // set branch datasource
                self.branchTextField.text = ""
                var branches = _dataSource.map { $0.branchName ?? "" }
                branches = branches.sorted()
                branches.insert("-- All Branches --", at: 0)
                self.branchDropDown.dataSource = branches
                self.branchDropDown.reloadAllComponents()
            } else {
                if textField == self.areaTextField {
                    // update datasource
                    var _dataSource = self.dataSource
                    if let selectedArea = self.areaTextField.text, !selectedArea.isEmpty, index > 0 {
                        _dataSource = _dataSource.filter {$0.areaName == selectedArea}
                    }

                    // set branch datasource
                    self.branchTextField.text = ""
                    var branches = _dataSource.map { $0.branchName ?? "" }
                    branches = branches.sorted()
                    branches.insert("-- All Branches --", at: 0)
                    self.branchDropDown.dataSource = branches
                    self.branchDropDown.reloadAllComponents()
                }
            }
        }
        
        // Set drop down
        switch textField {
        case regionTextField:
            self.regionDropDown = dropdown
        case areaTextField:
            self.areaDropDown = dropdown
        case branchTextField:
            self.branchDropDown = dropdown
        default:
            print("do nothing")
        }
    }
    
    private func setupDatePicker(withTextField textField: UITextField) {
        textField.delegate = self
        
        let picker = UIDatePicker()
        picker.datePickerMode = .date
        picker.addTarget(self, action: #selector(handleDatePicker), for: .valueChanged)
        
        textField.inputView = picker
    }
    
    private func getBranchCode(fromBranchName branchName: String) -> String? {
        if let index = self.dataSource.firstIndex(where: { $0.branchName == branchName }) {
            return self.dataSource[index].branchCode
        }
        
        return nil
    }
    
    // MARK: - IBAction Methods
    
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        Peep.play(sound: KeyPress.tap)
        Haptic.impact(.light).generate()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func applyButtonTapped(_ sender: UIButton) {
        Peep.play(sound: KeyPress.tap)
        Haptic.impact(.light).generate()
        
        self.setSearchFilters()
    }
    
    @IBAction func clearButtonTapped(_ sender: UIButton) {
        Peep.play(sound: KeyPress.tap)
        Haptic.impact(.light).generate()
        
        self.regionTextField.text = ""
        self.areaTextField.text = ""
        self.branchTextField.text = ""
        self.personTextField.text = ""
        self.dateTextField.text = ""
        
        self.setSearchFilters()
    }
    
    @IBAction func backgroundViewTapped(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc private func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        self.dateTextField.text = dateFormatter.string(from: sender.date)
    }
    
    // MARK: - Web Service Methods
    
    private func getSearchFilters() {
        ActivityIndicatorManager.showLoader()
        let params = [
            "action": WebRoute.searchFilterAction.rawValue,
            "userid": AppStateManager.shared.getUserId() ?? ""
        ]
        APIManager.sharedInstance.user.postArrayRequestWith(params: params, success: { (responseArray) in
            ActivityIndicatorManager.hideLoader()
            self.dataSource = Mapper<SearchModel>().mapArray(JSONArray: responseArray as! [[String: Any]])

            // set region datasource
            var regions = self.dataSource.map { $0.regionName ?? "" }
            regions = Array(Set(regions)).sorted()
            regions.insert("-- All Regions --", at: 0)
            self.regionDropDown.dataSource = regions
            self.regionDropDown.reloadAllComponents()
            
            // set area datasource
            var areas = self.dataSource.map { $0.areaName ?? "" }
            areas = Array(Set(areas)).sorted()
            areas.insert("-- All Areas --", at: 0)
            self.areaDropDown.dataSource = areas
            self.areaDropDown.reloadAllComponents()

            // set branch datasource
            var branches = self.dataSource.map { $0.branchName ?? "" }
            branches = branches.sorted()
            branches.insert("-- All Branches --", at: 0)
            self.branchDropDown.dataSource = branches
            self.branchDropDown.reloadAllComponents()
            
            self.populateData()
            
        }) { (error) in
            ActivityIndicatorManager.hideLoader()
            if error.code == -1000 {
                self.showInvalidTokenAlert()
            } else if error.code == -2000 {
                self.showChangePasswordAlert()
            }  else {
                AlertManager.showErrorWith(message: error.localizedDescription)
            }
        }
    }
    
    private func setSearchFilters() {
        ActivityIndicatorManager.showLoader()
        let params = [
            "action": WebRoute.setSearchFilterAction.rawValue,
            "userid": AppStateManager.shared.getUserId() ?? "",
            "region_name": self.regionTextField.text ?? "",
            "area_name": self.areaTextField.text ?? "",
            "branch_code": self.getBranchCode(fromBranchName: self.branchTextField.text ?? "") ?? "",
            "search_date": self.dateTextField.text ?? ""
        ]
        APIManager.sharedInstance.user.postDictionaryRequestWith(params: params, success: { (responseObject) in
            ActivityIndicatorManager.hideLoader()
            self.storeSearchFilterData()
            self.dismiss(animated: true) {
                AlertManager.showSuccessMessage(message: "Search filter applied successfully")
                if self.delegate != nil {
                    self.delegate?.didApplySearchFilters()
                }
            }
        }) { (error) in
            ActivityIndicatorManager.hideLoader()
            if error.code == -1000 {
                self.showInvalidTokenAlert()
            } else if error.code == -2000 {
                self.showChangePasswordAlert()
            }  else {
                AlertManager.showErrorWith(message: error.localizedDescription)
            }
        }
    }
    
    // MARK: - Navigation
    
    private func showSearchDetails(_ searchFieldType: SearchFieldType) {
        self.performSegue(withIdentifier: "showSearchDetails", sender: searchFieldType)
    }
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showSearchDetails" {
            if let controller = segue.destination as? SearchDetailsController {
                if let searchFieldType = sender as? SearchFieldType {
                    controller.delegate = self
                    controller.searchFieldType = searchFieldType
                    switch searchFieldType {
                    case .region:
                        let regions = self.dataSource.map { $0.regionName ?? "" }
                        controller.dataSource = Array(Set(regions))
                        break
                    case .area:
                        var _dataSource = self.dataSource
                        if let selectedRegion = self.regionTextField.text, !selectedRegion.isEmpty {
                            _dataSource = _dataSource.filter {$0.regionName == selectedRegion}
                        }
                        let areas = _dataSource.map { $0.areaName ?? "" }
                        controller.dataSource = Array(Set(areas))
                        break
                    case .branch:
                        var _dataSource = self.dataSource
                        if let selectedRegion = self.regionTextField.text, !selectedRegion.isEmpty {
                            _dataSource = _dataSource.filter {$0.regionName == selectedRegion}
                        }
                        if let selectedArea = self.areaTextField.text, !selectedArea.isEmpty {
                            _dataSource = _dataSource.filter {$0.areaName == selectedArea}
                        }
                        let branches = _dataSource.map { String(format: "%@&&%@", $0.branchName ?? "", $0.branchCode ?? "") }
                        controller.dataSource = branches
                        break
                    default:
                        break
                    }
                }
            }
        }
    }
}

extension SearchFiltersController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        switch textField {
        case regionTextField:
            self.regionDropDown.show()
            return false
        case areaTextField:
            self.areaDropDown.show()
            return false
        case branchTextField:
            self.branchDropDown.show()
            return false
        case personTextField:
            return false
        default:
            return true
        }
    }
}

extension SearchFiltersController: SearchDetailsDelegate {
    func didSelectItem(_ item: String, searchFieldType: SearchFieldType) {
        switch searchFieldType {
        case .region:
            self.regionTextField.text = item
            break
        case .area:
            self.areaTextField.text = item
            break
        case .branch:
            self.selectedBranchCode = item.components(separatedBy: "&&").last
            self.branchTextField.text = item.components(separatedBy: "&&").first
            break
        default:
            break
        }
    }
}
