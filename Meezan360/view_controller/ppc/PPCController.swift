//
//  PPCController.swift
//  ChartViewTestProject
//
//  Created by Amr Abd Elwahab on 02/10/2019.
//  Copyright © 2019 Boxit4me. All rights reserved.
//

import UIKit
import Charts
import ObjectMapper

class PPCController: BaseController {
    @IBOutlet weak var totalCustomerTitleLabel: UILabel!
    @IBOutlet weak var totalCustomerValueLabel: UILabel!
    @IBOutlet weak var ratioTitleLabel: UILabel!
    @IBOutlet weak var ratioValueLabel: UILabel!
    
    @IBOutlet weak var key1Label: UILabel!
    @IBOutlet weak var val1Label: UILabel!
    @IBOutlet weak var key2Label: UILabel!
    @IBOutlet weak var val2Label: UILabel!
    @IBOutlet weak var key3Label: UILabel!
    @IBOutlet weak var val3Label: UILabel!
    @IBOutlet weak var key4Label: UILabel!
    @IBOutlet weak var val4Label: UILabel!

    @IBOutlet weak var pieChartView: PieChartView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var dataSource: PPCModel?
        
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupPieChart()
        
        // Call web service        
        self.getPPCDetails()
    }
    
    private func setupPieChart() {
        pieChartView.delegate = self
        pieChartView.isMultipleTouchEnabled = false
        pieChartView.rotationEnabled = false
        pieChartView.drawHoleEnabled = true
        pieChartView.chartDescription?.enabled = false
        
        // Set legend settings
        pieChartView.legend.enabled = true
        pieChartView.legend.form = .circle
        pieChartView.legend.orientation = .horizontal
        pieChartView.legend.verticalAlignment = .bottom
        pieChartView.legend.horizontalAlignment = .center
        pieChartView.drawEntryLabelsEnabled = false

        self.addShadow(to: pieChartView)
    }
    
    private func addShadow(to pie: PieChartView) {
        pie.layer.shadowColor = UIColor.gray.cgColor
        pie.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        pie.layer.shadowRadius = 12.0
        pie.layer.shadowOpacity = 0.7
    }
    
    private func populateData() {
        if let totalCustomer = self.dataSource?.headValues?.first {
            self.totalCustomerTitleLabel.text = totalCustomer.key
            self.totalCustomerValueLabel.text = totalCustomer.value1
        }
        
        if let ratio = self.dataSource?.headValues?.last {
            self.ratioTitleLabel.text = ratio.key
            self.ratioValueLabel.text = ratio.value1
        }
    }
    
    private func setChartBottomValues(_ data: PPCPieDetailsModel) {
        self.key1Label.text = data.key1
        self.val1Label.setHtmlText(data.val1)
        
        self.key2Label.text = data.key2
        self.val2Label.setHtmlText(data.val2)
        
        self.key3Label.text = data.key3
        self.val3Label.setHtmlText(data.val3)
        
        self.key4Label.text = data.key4
        self.val4Label.setHtmlText(data.val4)
    }
    
    private func reloadChartDataSet() {
        if let pieChartValues = self.dataSource?.pie {
            var dataEntries = [ChartDataEntry]()
            var colors = [UIColor]()
            for item in pieChartValues {
                let value = item.val?.replacingOccurrences(of: "%", with: "")
                let data = PieChartDataEntry(value: Double(value ?? "0")!, label: item.title)
                dataEntries.append(data)
                
                // Add pie chart color
                if let hexCode = item.color { colors.append(UIColor(hexString: hexCode)) }
                else { colors.append(UIColor.random) }
            }
            
            let dataSet = PieChartDataSet(entries: dataEntries, label: "")
            //pieChartView background colors
            dataSet.colors = colors
            dataSet.drawValuesEnabled = false
            dataSet.entryLabelFont = NSUIFont.systemFont(ofSize: 12.0)
            
            //pieChartView text colors
            dataSet.valueColors = [UIColor.black]
            dataSet.selectionShift = 8
                        
            let data = PieChartData(dataSet: dataSet)
            pieChartView.data = data
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                self.pieChartView.highlightValue(x: 0, dataSetIndex: 0, callDelegate: true)
            }
            
            //This must stay at end of function
            pieChartView.animate(xAxisDuration: 2, yAxisDuration: 2, easingOption: .easeOutSine)
        }
    }
    
    override func didApplySearchFilters() {
        self.getPPCDetails()
    }
    
    // MARK: - Web Service Methods
    
    private func getPPCDetails() {
        ActivityIndicatorManager.showLoader()
        let params = [
            "action": WebRoute.ppcAction.rawValue,
            "userid": AppStateManager.shared.getUserId() ?? ""
        ]
        APIManager.sharedInstance.user.postArrayRequestWith(params: params, success: { (responseArray) in
            ActivityIndicatorManager.hideLoader()
            let array = Mapper<PPCModel>().mapArray(JSONArray: responseArray as! [[String: Any]])
            self.dataSource = array.first
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.populateData()
                self.reloadChartDataSet()
            }
        }) { (error) in
            ActivityIndicatorManager.hideLoader()
            if error.code == -1000 {
                self.showInvalidTokenAlert()
            } else if error.code == -2000 {
                self.showChangePasswordAlert()
            }  else {
                AlertManager.showErrorWith(message: error.localizedDescription)
            }
        }
    }
}


extension PPCController: ChartViewDelegate {
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        if let data = chartView.data, data.isKind(of: PieChartData.self) {
            let index = Int(highlight.x)
            guard index < (self.dataSource?.pie?.count ?? 0) else { return }
            if let pieValues = self.dataSource?.pie?[index] {
                self.setChartBottomValues(pieValues)
            }
        }
    }
}
